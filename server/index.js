const express = require("express");
const app = express();
const cors = require("cors");

app.use(cors());
app.use(express.json());

//Route init
const routes = require("./src/routes/index");
routes(app);

app.listen(3001, () => {
  console.log("Server is running");
});
