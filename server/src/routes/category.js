var express = require("express");
var router = express.Router();
const categoryController = require("../controllers/CategoryController");

router.get("/:category", categoryController.category_getListCategory);
router.get("/:category/:products", categoryController.category_getListProducts);
router.get("/admin/manage-catagory/page/:number", categoryController.category_index);
router.post("/admin/manage-catagory/delete", categoryController.catagory_delete);
router.get("/admin/manage-catagory/search", categoryController.catagory_searchByName);
router.put("/admin/manage-catagory/edit-catagory/:id", categoryController.catagory_edit);
router.post("/admin/manage-catagory/add-new-catagory", categoryController.catagory_edit);
router.get("/admin/manage-catagory/edit-catagory/:id", categoryController.catagory_getCatagoryDetail);
router.get("/admin/manage-category/get-list-category", categoryController.category_getListDropDownCategory);

module.exports = router;