var express = require("express");
var router = express.Router();
const cartController = require("../controllers/CartController");

// router.post("/filter-products-type", homeController.home_filter);
router.get("/cart/:id", cartController.cart_getCart);
router.post("/cart/delete", cartController.cart_deleteCartItem);
router.post("/cart/:id", cartController.cart_add);
router.post("/cart/discount/:id", cartController.cart_getDiscount);


module.exports = router;