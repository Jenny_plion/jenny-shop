var express = require("express");
var router = express.Router();
const homeController = require("../controllers/HomeController");

router.post("/filter-products-type", homeController.home_filter);
router.get("/projects-type", homeController.home_productType);
router.post("/login", homeController.home_login);
router.post("/signup", homeController.home_signup);
router.get("/products/:id", homeController.home_productDetail);
router.post("/", homeController.hone_getProductCartUser);
router.get("/admin/manage-orders/page/:number", homeController.order_getListOrder);
router.delete("/admin/manage-orders/delete/:id", homeController.order_deleteOrder);
router.get("/admin/manage-orders/edit-orders/:id", homeController.order_getDetail);
router.post("/admin/manage-orders/edit-orders/:id", homeController.order_deleteOrderItem);
// router.put('/admin/manage-orders/edit-orders/:id', homeController.order_editOrderItem);
router.put('/admin/manage-orders/edit-orders/:id', homeController.order_editOrder);
router.post('/my-order', homeController.order_getMyOrder);
router.get("/admin/dashboard", homeController.order_statiticsOrders);
router.get("/admin/quantity-statistic", homeController.order_statiticsTotalQuantityEachMonth);
router.post("/search-same-product", homeController.products_searchByProductType);

module.exports = router;