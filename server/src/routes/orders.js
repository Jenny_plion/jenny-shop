var express = require("express");
var router = express.Router();
const ordersController = require("../controllers/OrdersController");

router.post("/checkout", ordersController.order_addNewOrder);
router.post("/admin/dashboard", ordersController.order_searchStatiticsOrders);
router.post("/admin/quantity-statistic", ordersController.order_searchStatiticsTotalQuantityEachMonth);

module.exports = router;