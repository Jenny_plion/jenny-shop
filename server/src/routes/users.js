var express = require("express");
var router = express.Router();
const multer = require("multer");
const path = require("path");
//! Use of Multer
var storage = multer.diskStorage({
  destination: (req, file, callBack) => {
    callBack(null, "../client/public/images"); // './public/images/' directory name where save the file
  },
  filename: (req, file, callBack) => {
    callBack(
      null,
      file.fieldname + "-" + Date.now() + path.extname(file.originalname)
    );
  },
});

var upload = multer({
  storage: storage,
});

const userController = require("../controllers/UserController");
router.get("/admin/manage-users/page/:number", userController.users_index);
router.delete("/admin/manage-users/delete/:id", userController.users_delete);
router.post(
  "/admin/setting-profile/:id",
  upload.single("myImage"),
  userController.users_profile
);
router.post("/admin/manage-users/add-new-users", userController.users_addnew);
router.get(
  "/admin/setting-profile/get-customer/:id",
  userController.users_getDetailProfile
);
router.get("/admin/manage-users/search", userController.users_searchByName);
router.get("/my-profile/:id", userController.users_getDetailProfile);
router.post("/my-profile/:id",upload.single("myImage"), userController.users_profile);
router.post("/change-password/:id", userController.users_changePassword);
module.exports = router;
