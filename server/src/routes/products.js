var express = require("express");
var router = express.Router();
const multer = require("multer");
const path = require('path')
//! Use of Multer
var storage = multer.diskStorage({
    destination: (req, file, callBack) => {
      callBack(null, "../client/public/images"); // './public/images/' directory name where save the file
    },
    filename: (req, file, callBack) => {
      callBack(
        null,
        file.fieldname + "-" + Date.now() + path.extname(file.originalname)
      );
    },
  });
  
  var upload = multer({
    storage: storage,
  });

const productsController = require("../controllers/ProductsController");
router.get('/admin/manage-products/page/:number', productsController.products_index);
router.delete('/admin/manage-products/delete/:id', productsController.products_delete);
router.put('/admin/manage-products/edit-products/:id',upload.single("myImage"), productsController.products_edit);
router.post("/admin/manage-products/add-new-products",upload.single("myImage"), productsController.products_addnew);
router.get("/admin/manage-products/edit-products/:id", productsController.products_detail);
router.get("/admin/manage-products/search", productsController.products_searchByName);
router.post("/upload", upload.single("myImage"), productsController.upload);
router.get("/admin/manage-products/search", productsController.products_searchByName);

module.exports = router;