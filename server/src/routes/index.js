const productsRouter = require("./products");
const homeRouter = require("./home");
const userRouter = require("./users");
const cartRouter = require("./cart");
const categoryRouter = require("./category");
const orderRouter = require("./orders");
const voucherRouter = require("./voucher");
const stockRouter = require("./stock");

function route(app){
    app.use(productsRouter);
    app.use(homeRouter);
    app.use(userRouter);
    app.use(cartRouter);
    app.use(categoryRouter);
    app.use(orderRouter);
    app.use(voucherRouter);
    app.use(stockRouter);
}

module.exports = route;