var express = require("express");
var router = express.Router();
const stockController = require("../controllers/StockController");

router.get("/admin/manage-stock/page/:number", stockController.stock_index);
router.post("/admin/manage-stock/delete", stockController.stock_delete);
router.get("/admin/manage-stock/search", stockController.stock_searchByName);
router.put("/admin/manage-stock/edit-stock/:id", stockController.stock_edit);
router.get("/admin/manage-stock/edit-stock/:id", stockController.stock_getVoucherDetail);
router.post("/admin/manage-stock/add-new-stock",  stockController.stock_edit);
router.get("/admin/manage-stock/get-list-products", stockController.products_getList);

module.exports = router;