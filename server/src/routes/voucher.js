var express = require("express");
var router = express.Router();
const voucherController = require("../controllers/VoucherController");

router.get("/admin/manage-voucher/page/:number", voucherController.voucher_index);
router.post("/admin/manage-voucher/delete", voucherController.voucher_delete);
router.get("/admin/manage-voucher/search", voucherController.voucher_searchByName);
router.put("/admin/manage-voucher/edit-voucher/:id", voucherController.voucher_edit);
router.get("/admin/manage-voucher/edit-voucher/edit", voucherController.voucher_getVoucherDetail);
router.post("/admin/manage-voucher/add-new-voucher", voucherController.voucher_edit);
router.get("/admin/manage-voucher/get-list-voucher", voucherController.voucher_getListVoucher);

module.exports = router;