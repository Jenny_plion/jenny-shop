const db = require("../configDB");

const home_login = (req, res) => {
  const email = req.body.email;
  const password = req.body.password;
  //new user logic
  db.query(
    "SELECT * FROM customer_account  WHERE email = ? AND password = ?",
    [email, password],
    (err, result) => {
      if (err) {
        res.send({ err: err });
      } else {
        if (result.length > 0) {
          res.send(result);
        } else res.send({ message: "Wrong username/password" });
      }
    }
  );
};

const home_signup = (req, res) => {
  const username = req.body.username;
  const email = req.body.email;
  const password = req.body.password;
  const role = "user";
  db.query(
    "SELECT username, password  FROM customer_account WHERE username = '" +
      username +
      "' && password = '" +
      password +
      "'",
    function (err, result) {
      if (result.length === 0) {
        //new user logic
        db.query(
          "INSERT INTO customer_account (username, email, password, role) VALUES (?,?,?,?)",
          [username, email, password, role],
          (err, result) => {
            if (err) {
              console.log(err);
            } else res.send("Values inserted");
          }
        );
      } else {
        //existing user, redirect to another page
        res.redirect("/create");
      }
    }
  );
};

const home_productType = (req, res) => {
  //get type of products
  db.query("SELECT * FROM product_type ", (err, result) => {
    if (err) {
      res.send({ err: err });
    } else res.send(result);
  });
};

const home_filter = (req, res) => {
  const productTypeId = req.body.product_type_id;
  //filter top products by product type
  db.query(
    "SELECT P.product_id, P.product_name, P.product_infomation, P.price, P.product_status, P.total_sold, P.product_type_id, P.product_image, TP.top_number, D.points FROM products AS P, top_product AS TP, discount AS D WHERE P.product_id = TP.product_id AND D.discount_id = P.discount_id AND product_type_id = ? ORDER BY TP.top_number asc ",
    [productTypeId],
    (err, result) => {
      if (err) {
        res.send({ err: err });
      } else res.send(result);
    }
  );
};

const home_productDetail = (req, res) => {
  db.query(
    " SELECT * FROM products AS P WHERE product_id = ? ",
    [req.params.id],
    (err, result) => {
      if (err) {
        res.send({ err: err });
      } else {
        res.send(result);
        // console.log(result);
      }
    }
  );
};

const hone_getProductCartUser = (req, res) => {
  if (typeof req.body.product_id === "undefined") {
    db.query(
      " SELECT COUNT(*) cartItem_id FROM cart_items WHERE cart_id = (SELECT cart_id FROM shopping_cart WHERE customer_id = ?);",
      [req.body.customer_id],
      (err, result) => {
        if (err) {
          res.send({ err: err });
        } else {
          res.send(result);
        }
      }
    );
  } else {
    const quantity = req.body.quantity;
    const product_id = req.body.product_id;
    const customer_id = req.body.customer_id;

    const checkExistCartItem =
      "SELECT * FROM cart_items WHERE product_id = ? AND cart_id IN (SELECT cart_id FROM shopping_cart WHERE customer_id = ?)";
    const cartAddQuery =
      "INSERT INTO cart_items ( quantity, product_id, cart_id ) SELECT ?, ?, C.cart_id FROM shopping_cart as C WHERE C.cart_id IN (SELECT C.cart_id FROM shopping_cart WHERE C.customer_id = ?) ";
    const cartUpdateQuery =
      "UPDATE cart_items SET quantity = quantity + 1 WHERE product_id = ? AND cart_id IN (SELECT cart_id FROM shopping_cart WHERE customer_id = ?)";

    db.query(checkExistCartItem, [product_id, customer_id], (err, rows) => {
      if (err) {
        res.send({ err: err });
      }
      if (!rows.length) {
        db.query(
          cartAddQuery,
          [quantity, product_id, customer_id],
          (err2, result) => {
            if (err2) {
              res.send({ err: err2 });
            } else {
              res.send(result);
            }
          }
        );
      } else {
        db.query(cartUpdateQuery, [product_id, customer_id], (err3, result) => {
          if (err3) {
            res.send({ err: err3 });
          } else {
            res.send(result);
          }
        });
      }
    });
  }
};

//Get list of Order
const order_getListOrder = (req, res) => {
  var page_size = 10;
  var page = req.params.number;
  var skip = parseInt(page) * page_size - page_size;
  var limit = skip + "," + page_size;

  db.query(
    "SELECT count(*) as numRows FROM jennyshop.order",
    function (err, rows) {
      if (err) {
        res.send({ err: err });
      } else {
        var numRows = rows[0].numRows;
        var numPages = Math.ceil(numRows / page_size);
        db.query(
          " SELECT * FROM jennyshop.order AS P limit " + limit,
          (err, rows) => {
            if (err) {
              res.send({ err: err });
            } else {
              var responsePayload = {
                totalPages: numPages,
                listProduct: rows,
              };
              res.json(responsePayload);
            }
          }
        );
      }
    }
  );
};

//Delete order
const order_deleteOrder = (req, res) => {
  const deleteOrdersQuery =
    "DELETE a.*, b.* FROM  jennyshop.order_items a LEFT JOIN jennyshop.order b ON b.order_id = a.id_order WHERE a.id_order = ?";
  db.query(deleteOrdersQuery, req.params.id, (err) => {
    if (err) {
      res.send({ err: err });
    } else {
      res.send("Delete order number " + req.params.id + " successfully!!!");
    }
  });
};

//Get detail order
const order_getDetail = (req, res) => {
  const getDetailOrderQuery =
    "SELECT JSON_ARRAYAGG(JSON_OBJECT('order_id', order_id," +
    " 'ordered_date', ordered_date," +
    " 'status', status," +
    " 'kh_id', kh_id, " +
    " 'list_order_items', " +
    " ( select json_arrayagg(json_object( " +
    " 'order_items_id',`oi`.`order_items_id`, 'id_product',`oi`.`id_product`,'quantity',`oi`.`quantity`)) " +
    "from jennyshop.order AS o, jennyshop.order_items AS oi" +
    " WHERE o.order_id = oi.id_order AND o.order_id = ?), " +
    " 'customer_information', " +
    " ( select json_arrayagg(json_object( " +
    " 'first_name_customer',`p`.`firstname`, 'last_name_customer',`p`.`lastname`,'phonenumber',`p`.`phonenumber`, " +
    " 'address', `p`.`address`)) " +
    " from jennyshop.customer_profile AS p " +
    " WHERE p.id_customer IN (SELECT kh_id FROM jennyshop.order WHERE order_id = ?)) " +
    " ))as json from jennyshop.order where order_id = ?;";
  db.query(
    getDetailOrderQuery,
    [req.params.id, req.params.id, req.params.id],
    (err, result) => {
      if (err) {
        res.send({ err: err });
      } else {
        res.setHeader("Content-Type", "application/json");
        res.send(JSON.stringify(result[0].json));
      }
    }
  );
};

//DELETE order item id
const order_deleteOrderItem = (req, res) => {
  console.log(req.body.order_item_id);
  db.query(
    "DELETE FROM jennyshop.order_items AS oi WHERE oi.order_items_id = ?",
    req.body.order_item_id,
    (err) => {
      if (err) {
        res.send({ err: err });
      } else {
        res.send(
          "Order item id" + req.body.order_item_id + "is deleted successfully"
        );
      }
    }
  );
};

//Edit order
const order_editOrder = (req, res) => {
  const order_id = req.body.order_id;
  console.log(order_id);
  if (typeof order_id !== "undefined") {
    const ordered_date = req.body.ordered_date;
    const status = req.body.status;
    const order_id = req.body.order_id;

    db.query(
      "UPDATE jennyshop.order AS o SET o.ordered_date = ?, o.status = ? WHERE o.order_id = ?",
      [ordered_date, status, order_id],
      (err) => {
        if (err) {
          res.send({ err: err });
        } else {
          res.send("Order item id is edited successfully");
        }
      }
    );
  } else if (typeof order_id === "undefined") {
    const order_items_id = req.body.order_items_id;
    const id_product = req.body.id_product;
    const quantity = req.body.quantity;
    console.log(quantity);
    db.query(
      "UPDATE jennyshop.order_items AS oi SET oi.order_items_id = ?, oi.id_product = ?, oi.quantity = ? WHERE oi.order_items_id = ?",
      [order_items_id, id_product, quantity, order_items_id],
      (err) => {
        if (err) {
          res.send({ err: err });
        } else {
          res.send("Order item id" + order_items_id + "is edited successfully");
        }
      }
    );
  }
};

//Get my order
const order_getMyOrder = (req, res) => {
  const kh_id = req.body.kh_id;
  if (typeof kh_id !== "undefined") {
    const myOrderQuery =
      " SELECT JSON_ARRAYAGG(JSON_OBJECT('order_id', order_id, " +
      " 'list_order_items', " +
      " ( select json_arrayagg(json_object( 'order_items_id',`oi`.`order_items_id`, " +
      " 'id_product',`oi`.`id_product`, " +
      " 'quantity',`oi`.`quantity`, " +
      " 'price',`p`.`price`,  " +
      " 'product_image',`p`.`product_image`, " +
      " 'product_name',`p`.`product_name`, " +
      " 'status',`o`.`status`, " +
      " 'ordered_date',`o`.`ordered_date`)) " +
      " from jennyshop.order AS o, jennyshop.order_items AS oi, jennyshop.products AS p " +
      " WHERE o.order_id = oi.id_order AND o.kh_id = ? AND oi.id_product = p.product_id) " +
      " ))as json from jennyshop.order where kh_id = ?; ";
    db.query(myOrderQuery, [kh_id, kh_id], (err, result) => {
      if (err) {
        res.send({ err: err });
      } else {
        res.setHeader("Content-Type", "application/json");
        res.send(JSON.stringify(result[0].json));
      }
    });
  } else order_deleteOrderItem(req, res);
};

const order_statiticsOrders = (req, res) => {
  const getStatiticsOrdersQuery =
    " SELECT id_product, COUNT(id_product) AS 'quantity' " +
    " FROM jennyshop.order_items WHERE ordered_date between ? and ?" +
    "GROUP BY id_product;";

  var date = new Date();
  var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
  var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
  db.query(getStatiticsOrdersQuery, [firstDay, lastDay], (err, result) => {
    if (err) {
      res.send({ err: err });
    } else {
      res.send(result);
    }
  });
};

const order_statiticsTotalQuantityEachMonth = (req, res) => {
  var date = new Date();
  var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
  var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
  const getStatiticTotalQuantityEachMonthQuery =
    " SELECT COUNT(id_product) AS 'quantity', MONTH(ordered_date) AS 'month' " +
    " FROM jennyshop.order_items  where ordered_date >= '" +
    firstDay.toISOString().slice(0, 10) +
    "' AND ordered_date <= '" +
    lastDay.toISOString().slice(0, 10) +
    "' GROUP BY MONTH(ordered_date) order by Year(ordered_date), month(ordered_date); ";
  db.query(getStatiticTotalQuantityEachMonthQuery, (err, result) => {
    if (err) {
      res.send({ err: err });
    } else {
      res.send(result);
    }
  });
};

const products_searchByProductType = (req, res) => {
  db.query(
    "SELECT * FROM products WHERE product_type_id LIKE '%" +
      req.body.productTypeId +
      "%'",
    (err, result) => {
      if (err) {
        res.send({ err: err });
      } else res.send(result);
    }
  );
};

module.exports = {
  home_login,
  home_signup,
  home_productType,
  home_filter,
  home_productDetail,
  hone_getProductCartUser,
  order_getListOrder,
  order_deleteOrder,
  order_getDetail,
  order_deleteOrderItem,
  order_editOrder,
  order_getMyOrder,
  order_statiticsOrders,
  order_statiticsTotalQuantityEachMonth,
  products_searchByProductType,
};
