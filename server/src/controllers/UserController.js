const db = require("../configDB");

const users_index = (req, res) => {
  var page_size = 10;
  var page = req.params.number;
  var skip = parseInt(page)*page_size - page_size;
  var limit = skip + ',' + page_size;

  db.query('SELECT count(*) as numRows FROM customer_account',function (err, rows) {
    if(err) {
      res.send({ err: err });
    }else{
        var numRows = rows[0].numRows;
        var numPages = Math.ceil(numRows / page_size);
        db.query(" SELECT * FROM customer_account AS P limit " + limit, (err, rows) => {
          if (err) {
            res.send({ err: err });
          } else {
            var responsePayload = {
              totalPages: numPages,
              listProduct: rows
            };
            res.json(responsePayload);
          }
        });          
    }
});
};

const users_delete = (req, res) => {
  db.query(
    " DELETE FROM customer_account WHERE id_customer = ? ",
    req.params.id,
    (err) => {
      if (err) {
        res.send({ err: err });
      } else res.send("Delete user successfully!!!");
    }
  );
};

const users_addnew = (req, res) => {
  const id_customer = req.body.id_customer;
  const username = req.body.username;
  const password = req.body.password;
  const email = req.body.email;
  const role = req.body.role;

  db.query(
    " SELECT * FROM customer_account AS P WHERE id_customer = ?",
    [id_customer],
    (err, rows) => {
      if (err) {
        res.send({ err: err });
      }

      if (!rows.length) {
        db.query(
          "INSERT INTO customer_account (id_customer, username, password, email, role) VALUES (?,?,?,?,?)",
          [id_customer, username, password, email, role],
          (err2, result) => {
            if (err2) {
              res.send({ err: err2 });
            } else res.send("Add new user successfully");
          }
        );
      } else {
        res.send("This user is existed!!!");
      }
    }
  );
};

const users_searchByName = (req, res) => {
  const username = req.query.username;
  db.query("select * from jennyshop.customer_account where username like '%" + username+ "%' ", username, (err, result) => {
    if(err){
      res.send({err: err});
    }else res.send(result);
  })
}

const users_profile = (req, res) => {
  const id_customer = req.body.id_customer;
  const firstname = req.body.firstname;
  const lastname = req.body.lastname;
  const phonenumber = req.body.phonenumber;
  const address = req.body.address;
  const user_image = req.file.filename;
  if (!user_image) {
    const error = new Error("Please upload a file");
    error.httpStatusCode = 400;
    return next(error);
  } else {
    db.query(
      " SELECT * FROM customer_profile WHERE id_customer = ?",
      [id_customer],
      (err, rows) => {
        if (err) {
          res.send({ err: err });
        }

        if (!rows.length) {
          res.send("Upload err");
        } else {
          db.query(
            "UPDATE customer_profile SET firstname = ? , lastname = ?, phonenumber = ?, address = ?, customer_avatar = ? WHERE id_customer = ?",
            [
              firstname,
              lastname,
              phonenumber,
              address,
              user_image,
              id_customer
            ],
            (err3, result) => {
              if (err3) {
                res.send({ err: err3 });
              } else res.send("Edit user successfully!!!");
            }
          );
        }
      }
    );}
  }
// }

const users_getDetailProfile = (req, res) => {
  db.query("SELECT * from customer_profile WHERE id_customer = ?",req.params.id, (err, result) => {
    if (err) {
      res.send({ err: err });
    } else res.send(result);
  });
}

const users_changePassword = (req, res) => {
  const old_password = req.body.oldPassword;
  const new_password = req.body.newPassword;
  const id_user = req.body.id_user;

  db.query(
    " SELECT * FROM customer_account AS P WHERE id_customer = ? AND password = ?",
    [id_user, old_password],
    (err, rows) => {
      if (err) {
        res.send("Có một vài lỗi đang xảy ra");
      }

      if (rows.length > 0) {
        db.query(
          "UPDATE customer_account SET password = ? WHERE id_customer = ?",
          [new_password, id_user],
          (err2, result) => {
            if (err2) {
              res.send({ err: err2 });
            } else res.send("Cập nhật mật khẩu thành công");
          }
        );
      } else {
        res.send("Mật khẩu hiện tại không chính xác");
      }
    }
  );

}

module.exports = {
  users_index,
  users_delete,
  users_addnew,
  users_searchByName,
  users_profile, 
  users_getDetailProfile,
  users_changePassword
};
