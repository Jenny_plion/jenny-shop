const db = require("../configDB");
const products_index = (req, res) => {
  var page_size = 10;
  var page = req.params.number;
  var skip = parseInt(page) * page_size - page_size;
  var limit = skip + "," + page_size;

  db.query("SELECT count(*) as numRows FROM products", function (err, rows) {
    if (err) {
      res.send({ err: err });
    } else {
      var numRows = rows[0].numRows;
      var numPages = Math.ceil(numRows / page_size);
      db.query(" SELECT * FROM products AS P limit " + limit, (err, rows) => {
        if (err) {
          res.send({ err: err });
        } else {
          var responsePayload = {
            totalPages: numPages,
            listProduct: rows,
          };
          res.json(responsePayload);
        }
      });
    }
  });
};

const products_detail = (req, res) => {
  db.query(
    " SELECT * FROM products AS P WHERE product_id = ? ",
    [req.params.id],
    (err, result) => {
      if (err) {
        res.send({ err: err });
      } else res.send(result);
    }
  );
};

const products_edit = (req, res, next) => {
  const product_id = req.body.product_id;
  const product_name = req.body.product_name;
  const product_information = req.body.product_information;
  const price = req.body.price;
  const product_status = req.body.product_status;
  const total_sold = req.body.total_sold;
  const product_type_id = req.body.product_type_id;
  const file = req.file;
  const discount_id = req.body.discount_id;

  db.query(
    " SELECT * FROM products WHERE product_id = ?",
    product_id,
    (err, rows) => {
      if (err) {
        res.send({ err: err });
      }

      if (rows.length > 0) {
        if (file) {
          var insertDataImage =
            "UPDATE products SET product_id = ?, product_name = ?, product_infomation = ?, price = ?, product_status = ?, total_sold = ?, product_type_id = ?, discount_id = ?, product_image = ? WHERE product_id = ? ";
          db.query(
            insertDataImage,
            [
              product_id,
              product_name,
              product_information,
              price,
              product_status,
              total_sold,
              product_type_id,
              discount_id,
              req.file.filename,
              product_id,
            ],
            (err2, result) => {
              if (err2) {
                throw err2;
              } else res.send("Edit products "+ product_id +" successfully!!!");
            }
          );
        } else {
          var insertData =
            "UPDATE products SET product_id = ?, product_name = ?, product_infomation = ?, price = ?, product_status = ?, total_sold = ?, product_type_id = ?, discount_id = ? WHERE product_id = ? ";
          db.query(
            insertData,
            [
              product_id,
              product_name,
              product_information,
              price,
              product_status,
              total_sold,
              product_type_id,
              discount_id,
              product_id,
            ],
            (err2, result) => {
              if (err2) {
                throw err2;
              } else res.send("Edit products "+ product_id +" successfully!!!");
            }
          );
        }
      } else {
        if (file) {
          db.query(
            "INSERT INTO products (product_id, product_name, product_infomation, price, product_status, total_sold, product_type_id, discount_id, product_image) VALUES (?,?,?,?,?,?,?,?,?)",
            [
              product_id,
              product_name,
              product_information,
              price,
              product_status,
              total_sold,
              product_type_id,
              discount_id,
              req.file.filename,
            ],
            (err3) => {
              if (err3) {
                res.send({ err: err3 });
              } else res.send("Add new successfully!!!");
            }
          );
        } else {
          db.query(
            "INSERT INTO products (product_id, product_name, product_infomation, price, product_status, total_sold, product_type_id, discount_id) VALUES (?,?,?,?,?,?,?,?)",
            [
              product_id,
              product_name,
              product_information,
              price,
              product_status,
              total_sold,
              product_type_id,
              discount_id,
            ],
            (err3) => {
              if (err3) {
                res.send({ err: err3 });
              } else res.send("Add new successfully!!!");
            }
          );
        }
      }
    }
  );
};

const products_addnew = (req, res) => {
  const product_id = req.body.product_id;
  const product_name = req.body.product_name;
  const product_information = req.body.product_information;
  const price = req.body.price;
  const product_status = req.body.product_status;
  const total_sold = req.body.total_sold;
  const product_type_id = req.body.product_type_id;
  const product_image = req.file.filename;
  const discount_id = req.body.discount_id;
  const category_id = req.body.category_id;

  if (!product_image) {
    const error = new Error("Please upload a file");
    error.httpStatusCode = 400;
    return next(error);
  } else {
    db.query(
      " SELECT * FROM products AS P WHERE product_id = ?",
      [product_id],
      (err, rows) => {
        if (err) {
          res.send({ err: err });
        }

        if (!rows.length) {
          db.query(
            "INSERT INTO products (product_id, product_name, product_infomation, price, product_status, total_sold, product_type_id, product_image, discount_id, category_id) VALUES (?,?,?,?,?,?,?,?,?,?)",
            [
              product_id,
              product_name,
              product_information,
              price,
              product_status,
              total_sold,
              product_type_id,
              product_image,
              discount_id,
              category_id
            ],
            (err2, result) => {
              if (err2) {
                res.send({ err: err2 });
                console.log(err2);
              } else res.send("Add new product successfully");
            }
          );
        } else {
          res.send("Products is existed!!!");
        }
      }
    );
  }
};

const products_delete = (req, res) => {
  db.query(
    " DELETE FROM products WHERE product_id = ? ",
    req.params.id,
    (err) => {
      if (err) {
        res.send({ err: err });
      } else res.send("Delete product "+ req.params.id + " successfully!!!");
    }
  );
};

// const products_index = (req, res) => {
//   // limit as 8 items per page
//   const limit = 8;
//   // page number
//   const page = req.query.page;
//   console.log(page);
//   // calculate offset
//   const offset = (page - 1) * limit
//   // query for fetching data with page number and offset
//   const prodsQuery = "select * from products limit "+limit+" OFFSET "+offset
//   db.query(prodsQuery, function (error, results, fields) {
//       // When done with the connection, release it.
//            if (error) throw error;
//       // create payload
//       var jsonResult = {
//         'products_page_count':results.length,
//         'page_number':page,
//         'products':results
//       }
//       // create response
//       var myJsonString = JSON.parse(JSON.stringify(jsonResult));
//       res.statusMessage = "Products for page "+page;
//       res.statusCode = 200;
//       res.json(myJsonString);
//       res.end();
//     })

// }

const products_searchByName = (req, res) => {
  db.query(
    "SELECT * FROM products WHERE product_name LIKE '%" +
      req.query.productName +
      "%'",
    (err, result) => {
      if (err) {
        res.send({ err: err });
      } else res.send(result);
    }
  );
};

const upload = (req, res, next) => {
  const file = req.file;
  console.log(file);
  if (!file) {
    const error = new Error("Please upload a file");
    error.httpStatusCode = 400;
    return next(error);
  } else {
    var insertData =
      "UPDATE products SET product_image = ? WHERE product_id = 'lv-table-12'";
    db.query(insertData, [req.file.filename], (err, result) => {
      if (err) {
        throw err;
      } else res.send(result);
    });
  }
};

module.exports = {
  products_index,
  products_addnew,
  products_delete,
  products_detail,
  products_edit,
  products_searchByName,
  upload,
};
