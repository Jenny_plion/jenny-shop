const db = require("../configDB");

const stock_index = (req, res) => {
    var page_size = 10;
  var page = req.params.number;
  var skip = parseInt(page)*page_size - page_size;
  var limit = skip + ',' + page_size;

  db.query('SELECT count(*) as numRows FROM stock',function (err, rows) {
    if(err) {
      res.send({ err: err });
    }else{
        var numRows = rows[0].numRows;
        var numPages = Math.ceil(numRows / page_size);
        db.query(" SELECT s.*, p.product_name FROM jennyshop.stock AS s, jennyshop.products AS p WHERE s.product_stock_id = p.product_id limit " + limit, (err, rows) => {
          if (err) {
            res.send({ err: err });
          } else {
            var responsePayload = {
              totalPages: numPages,
              listVoucher: rows
            };
            res.json(responsePayload);
          }
        });          
    }
});
}

const stock_delete = (req, res) => {
    db.query(
      " DELETE FROM stock WHERE stock_id = ? ",
      req.body.id,
      (err) => {
        if (err) {
          res.send({ err: err });
        } else res.send("Xoá thành công");
      }
    );
  };

  const stock_searchByName = (req, res) => {
    const stock = req.query.stock;
    db.query("Select s.*, p.product_name FROM jennyshop.stock AS s, jennyshop.products AS p WHERE s.product_stock_id = p.product_id AND s.product_stock_id like '%" + stock + "%' ", stock , (err, result) => {
      if(err){
        res.send({err: err});
      }else res.send(result);
    })
  }

  const stock_edit = (req, res) => {
    const product_id = req.body.product_stock_id;
    const nhap_kho = req.body.nhap_kho;
    const ton_kho = req.body.ton_kho;
    const xuat_kho = req.body.xuat_kho;
  
    db.query(
      " SELECT * FROM stock AS P WHERE product_stock_id = ?",
      [product_id],
      (err, rows) => {
        if (err) {
          res.send({ err: err });
        }
  
        if (!rows.length) {
          db.query(
            "INSERT INTO stock (product_stock_id, so_luong_nhap_kho, so_luong_ton_kho, so_luong_xuat_kho) VALUES (?,?,?,?)",
            [product_id, nhap_kho, ton_kho, xuat_kho],
            (err2, result) => {
              if (err2) {
                res.send({ err: err2 });
              } else res.send("Thêm mới stock product thành công ");
            }
          );
        } else {
            db.query(
                "UPDATE stock SET  so_luong_nhap_kho = ?, so_luong_ton_kho = ?, so_luong_xuat_kho = ? WHERE product_stock_id = ?",
                [
                    nhap_kho,
                    ton_kho,
                    xuat_kho,
                    product_id,
                ],
                (err3, result) => {
                  if (err3) {
                    res.send({ err: err3 });
                  } else res.send("Cập nhật stock product thành công");
                }
              );
        }
      }
    );
  };

  const stock_getVoucherDetail = (req, res) => {
    db.query("SELECT * from stock WHERE stock_id = ?",req.params.id, (err, result) => {
        if (err) {
          res.send({ err: err });
        } else res.send(result);
      });
  }

  
const products_getList = (req, res) => {
  db.query(
    " SELECT JSON_ARRAYAGG(JSON_OBJECT('value', product_id, "+
    " 'label', product_name "+  
    " ))as json from jennyshop.products ",
    (err, result) => {
      if (err) {
        res.send({ err: err });
      } else res.send(result);
    }
  );
}

module.exports = {
    stock_index,
    stock_delete,
    stock_searchByName,
    stock_edit,
    stock_getVoucherDetail,
    products_getList
  };