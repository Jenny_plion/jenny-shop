const db = require("../configDB");

const cart_getCart = (req, res) => {
    const query = "SELECT P.product_id, P.product_name, P.product_infomation, P.price,CI.quantity, P.product_image FROM jennyshop.products AS P, jennyshop.cart_items AS CI WHERE P.product_id IN(SELECT CI.product_id FROM jennyshop.shopping_cart AS C, jennyshop.cart_items AS CI WHERE C.customer_id = ? AND C.cart_id = CI.cart_id) AND CI.product_id = P.product_id";
    db.query(query,
        [req.params.id],
        (err, result) => {
          if (err) {
            res.send({ err: err });
          } else {
            res.send(result);
          }
        }
      );
}

const cart_deleteCartItem = (req, res) => {
      const customer_id = req.body.id_customer;
      const product_id = req.body.cartItems_id;
      const deleteQuery = "DELETE FROM cart_items WHERE product_id = ? AND cart_id IN(SELECT cart_id FROM shopping_cart WHERE customer_id = ?)"
      db.query(deleteQuery,
        [product_id, customer_id],
        (err, result) => {
          if (err) {
            res.send({ err: err });
          } else {
            res.send(result);
          }
        }
      );
}

const cart_getDiscount = (req, res) => {
  const discount_id = req.body.discountInput;
  const voucher_id = req.body.voucherInput;

  const getDiscountIDQuery = "SELECT points FROM discount WHERE discount_id = ?";
  const getVoucherIDQuery = "SELECT points FROM voucher WHERE voucher_id = ?";
  if(discount_id){
    db.query(getDiscountIDQuery, discount_id, (err, result) => {
      if (err) {
        res.send({ err: err });
      } else {
        res.send(result);
      }
    })
  }else if(voucher_id){
    db.query(getVoucherIDQuery, voucher_id, (err, result) => {
      if (err) {
        res.send({ err: err });
      } else {
        res.send(result);
      }
    })
  }
  
}

const cart_add = (req, res) => {
  const quantity = req.body.quantity;
  const product_id = req.body.product_id;
  const customer_id = req.body.customer_id;

  const checkExistCartItem = "SELECT * FROM cart_items WHERE product_id = ? AND cart_id IN (SELECT cart_id FROM shopping_cart WHERE customer_id = ?)"
  const cartAddQuery = "INSERT INTO cart_items ( quantity, product_id, cart_id ) SELECT ?, ?, C.cart_id FROM shopping_cart as C WHERE C.cart_id IN (SELECT C.cart_id FROM shopping_cart WHERE C.customer_id = ?) ";
  const cartUpdateQuery = "UPDATE cart_items SET quantity = quantity + 1 WHERE product_id = ? AND cart_id IN (SELECT cart_id FROM shopping_cart WHERE customer_id = ?)";
  
  db.query(checkExistCartItem,[product_id, customer_id], (err, rows) => {
    if (err) {
      res.send({ err: err });
    } 
    if (!rows.length) {
      db.query(cartAddQuery,[quantity, product_id, customer_id], (err2, result) => {
        if (err2) {
          res.send({ err: err2 });
        } else {
          res.send(result);
        }
      })
    }else{
      db.query(cartUpdateQuery,[product_id, customer_id], (err3, result) => {
        if (err3) {
          res.send({ err: err3 });
        } else {
          res.send(result);
        }
      })
    }
  })
  
  
}

module.exports = {
    cart_getCart,
    cart_deleteCartItem,
    cart_getDiscount,
    cart_add
  };