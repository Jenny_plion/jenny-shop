const db = require("../configDB");

const category_getListCategory = (req, res) => {
    db.query(
        " SELECT category_name FROM category WHERE category_type = ? ",
        [req.params.category],
        (err, result) => {
          if (err) {
            res.send({ err: err });
          } else {
            res.send(result);
            // console.log(result);
          }
        }
      );
}

const category_getListProducts = (req, res) => {
    db.query(
        " SELECT P.*, D.points FROM jennyshop.products AS P, jennyshop.discount AS D  WHERE category_id IN (SELECT category_id FROM jennyshop.category AS C WHERE category_name = ?) AND D.discount_id = P.discount_id",
        [req.params.products],
        (err, result) => {
          if (err) {
            res.send({ err: err });
          } else {
            res.send(result);
            // console.log(result);
          }
        }
      );
}

const category_index = (req, res) => {
  var page_size = 10;
  var page = req.params.number;
  var skip = parseInt(page)*page_size - page_size;
  var limit = skip + ',' + page_size;

  db.query('SELECT count(*) as numRows FROM category',function (err, rows) {
    if(err) {
      res.send({ err: err });
    }else{
        var numRows = rows[0].numRows;
        var numPages = Math.ceil(numRows / page_size);
        db.query(" SELECT * FROM category limit " + limit, (err, rows) => {
          if (err) {
            res.send({ err: err });
          } else {
            var responsePayload = {
              totalPages: numPages,
              listVoucher: rows
            };
            res.json(responsePayload);
          }
        });          
    }
});
}

const catagory_delete = (req, res) => {
  db.query(
    " DELETE FROM category WHERE category_id = ? ",
    req.body.id,
    (err) => {
      if (err) {
        res.send({ err: err });
      } else res.send("Xoá thành công");
    }
  );
};

const catagory_searchByName = (req, res) => {
  const category = req.query.catagory;
  db.query("select * from jennyshop.category where category_name like '%" + category + "%' ", category , (err, result) => {
    if(err){
      res.send({err: err});
    }else res.send(result);
  })
}

const catagory_edit = (req, res) => {
  const category_id = req.body.category_id;
  const category_name = req.body.category_name;
  const category_type = req.body.category_type;

  db.query(
    " SELECT * FROM category AS P WHERE category_id = ?",
    [category_id],
    (err, rows) => {
      if (err) {
        res.send({ err: err });
      }

      if (!rows.length) {
        db.query(
          "INSERT INTO category (category_name, category_type) VALUES (?,?)",
          [category_name, category_type],
          (err2, result) => {
            if (err2) {
              res.send({ err: err2 });
            } else res.send("Thêm mới catagory thành công ");
          }
        );
      } else {
          db.query(
              "UPDATE category SET  category_name = ?, category_type = ? WHERE category_id = ?",
              [
                  category_name,
                  category_type,
                  category_id,
              ],
              (err3, result) => {
                if (err3) {
                  res.send({ err: err3 });
                } else res.send("Cập nhật catagory thành công");
              }
            );
      }
    }
  );
};

const catagory_getCatagoryDetail = (req, res) => {
  console.log(req.params.id);
  db.query("SELECT * from category WHERE category_id = ?",req.params.id, (err, result) => {
      if (err) {
        res.send({ err: err });
      } else res.send(result);
    });
}

const category_getListDropDownCategory = (req, res) => {
  db.query(
    " SELECT JSON_ARRAYAGG(JSON_OBJECT('value', category_id, "+
    " 'label', category_name "+  
    " ))as json from jennyshop.category ",
    (err, result) => {
      if (err) {
        res.send({ err: err });
      } else res.send(result);
    }
  );
}

module.exports = {
    category_getListCategory,
    category_getListProducts,
    category_index,
    catagory_delete,
    catagory_searchByName,
    catagory_edit,
    catagory_getCatagoryDetail,
    category_getListDropDownCategory
  };
  