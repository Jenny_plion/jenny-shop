const db = require("../configDB");

const voucher_index = (req, res) => {
    var page_size = 10;
  var page = req.params.number;
  var skip = parseInt(page)*page_size - page_size;
  var limit = skip + ',' + page_size;

  db.query('SELECT count(*) as numRows FROM discount',function (err, rows) {
    if(err) {
      res.send({ err: err });
    }else{
        var numRows = rows[0].numRows;
        var numPages = Math.ceil(numRows / page_size);
        db.query(" SELECT * FROM discount limit " + limit, (err, rows) => {
          if (err) {
            res.send({ err: err });
          } else {
            var responsePayload = {
              totalPages: numPages,
              listVoucher: rows
            };
            res.json(responsePayload);
          }
        });          
    }
});
}

const voucher_delete = (req, res) => {
    db.query(
      " DELETE FROM jennyshop.discount WHERE discount_id = ? ",
      req.body.id,
      (err) => {
        if (err) {
          res.send({err: "Không thể xóa mã khuyến mãi này, chỉ có thể sửa"});
        } else res.send("Xoá thành công");
      }
    );
  };

  const voucher_searchByName = (req, res) => {
    const voucher = req.query.voucher;
    db.query("select * from jennyshop.discount where discount_id like '%" + voucher + "%' ", voucher , (err, result) => {
      if(err){
        res.send({err: err});
      }else res.send(result);
    })
  }

  const voucher_edit = (req, res) => {
    const discount_id = req.body.discount_id;
    const points = req.body.points;
    const time = req.body.time;
    var datetime = new Date();
  
    db.query(
      " SELECT * FROM discount AS P WHERE discount_id = ?",
      [discount_id],
      (err, rows) => {
        if (err) {
          res.send({ err: err });
        }
  
        if (!rows.length) {
          db.query(
            "INSERT INTO discount (discount_id, create_date, points, time) VALUES (?,?,?,?)",
            [discount_id,datetime.toISOString().slice(0,10), points, time],
            (err2, result) => {
              if (err2) {
                res.send({ err: err2 });
              } else res.send("Thêm mới voucher thành công ");
            }
          );
        } else {
            db.query(
                "UPDATE discount SET  create_date = ?, points = ?, time = ? WHERE discount_id = ?",
                [
                    datetime.toISOString().slice(0,10),
                    points,
                    time,
                    discount_id,
                ],
                (err3, result) => {
                  if (err3) {
                    res.send({ err: err3 });
                  } else res.send("Cập nhật voucher thành công");
                }
              );
        }
      }
    );
  };

  const voucher_getVoucherDetail = (req, res) => {
    db.query("SELECT * from discount WHERE discount_id = ?",req.query.voucher, (err, result) => {
        if (err) {
          res.send({ err: err });
        } else res.send(result);
      });
  }

  const voucher_getListVoucher = (req, res) => {
    db.query(
      " SELECT JSON_ARRAYAGG(JSON_OBJECT('value', discount_id, "+
      " 'label', discount_id "+  
      " ))as json from jennyshop.discount ",
      (err, result) => {
        if (err) {
          res.send({ err: err });
        } else res.send(result);
      }
    );
  }

module.exports = {
    voucher_index,
    voucher_delete,
    voucher_searchByName,
    voucher_edit,
    voucher_getVoucherDetail,
    voucher_getListVoucher
  };