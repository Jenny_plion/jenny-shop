const db = require("../configDB");


const order_addNewOrder = (req, res) => {
  let listOrderRequest = req.body.order_list;
  var datetime = new Date();
  const insertIntoOrderQuery = "INSERT INTO jennyshop.order (ordered_date, status, kh_id) VALUES (?, ?, ?);";
  if(listOrderRequest.length > 0){
  db.query(
    insertIntoOrderQuery,[datetime.toISOString().slice(0,10), "Comfirmed by shop owner", req.body.kh_id],
      (err, result) => {
        if (err) {
          res.send({ err: err });
        } else {
           let values=listOrderRequest.reduce((o,a)=>{
                 let ini=[];
                 ini.push(result.insertId);
                 ini.push(a.product_id);
                 ini.push(a.quantity);
                 ini.push(datetime.toISOString().slice(0,10));
                 o.push(ini);
                 return o
           },[])
           if(values.length > 0){
            db.query(
              "INSERT INTO jennyshop.order_items (id_order, id_product, quantity, ordered_date) VALUES ?",
              [values],
              (err2) => {
                if (err2) {
                  res.send({ err: err2 });
                } else res.send("Place order successfully");
              }
            );
           }
        }
      }
    );
  }
}

const order_searchStatiticsOrders = (req, res) => {
  const startDate = req.body.startDate;
  const endDate = req.body.endDate;

  const getStatiticsOrdersQuery =
    " SELECT id_product, COUNT(id_product) AS 'quantity' " +
    " FROM jennyshop.order_items WHERE ordered_date between ? and ?" +
    "GROUP BY id_product;";

  db.query(getStatiticsOrdersQuery, [startDate, endDate], (err, result) => {
    if (err) {
      res.send({ err: err });
    } else {
      res.send(result);
    }
  });
};

const order_searchStatiticsTotalQuantityEachMonth = (req, res) => {
  const startDate = req.body.startDate;
  const endDate = req.body.endDate;

  const getStatiticTotalQuantityEachMonthQuery =
    " SELECT COUNT(id_product) AS 'quantity', MONTH(ordered_date) AS 'month' " +
    " FROM jennyshop.order_items  where ordered_date >= ? AND ordered_date <= ? " +
    " GROUP BY MONTH(ordered_date) order by Year(ordered_date), month(ordered_date) ; ";

  db.query(getStatiticTotalQuantityEachMonthQuery,[startDate, endDate], (err, result) => {
    if (err) {
      res.send({ err: err });
    } else {
      res.send(result);
    }
  });
};

module.exports = {
  order_addNewOrder,
  order_searchStatiticsOrders,
  order_searchStatiticsTotalQuantityEachMonth
  };