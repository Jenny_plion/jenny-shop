import * as React from "react";
import PropTypes from "prop-types";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import "../css/Furniture.css";
import { Container, Row, Col, Card, Offcanvas, Button } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import Select from "react-select";
import { useState, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import { Loyalty } from "@mui/icons-material";
import { RiHeart3Fill } from "react-icons/ri";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography component={"span"}>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    "aria-controls": `vertical-tabpanel-${index}`,
  };
}

export default function VerticalTabs({ match }) {
  const [toggleHeart, setToggleHeart] = useState(false);
  const [isHeart, setIsHeart] = useState(false);
  const [value, setValue] = React.useState(0);
  const [listProducts, setListPorducts] = useState([]);
  const [furnitureCategory, setFurnitureCategory] = useState([]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
    axios
      .get(
        `http://localhost:3001/${match.params.product}/${furnitureCategory[newValue].category_name}`
      )
      .then((response) => {
        if (response.data.err) {
          alert(response.data.err);
        } else {
          setListPorducts(response.data);
          // console.log(response.data);
        }
      });
  };

  const optionsSort = [
    { value: "lowtohigh", label: "Price: Low to High" },
    { value: "hightolow", label: "Price: High to Low" },
    { value: "newest", label: "Newest" },
    { value: "name", label: "Name" },
    { value: "popular", label: "Most popular" },
  ];

  const optionsPrice = [
    { value: "1", label: "0 - 1,000,000 VND" },
    { value: "2", label: "1,000,000 - 3,000,000 VND" },
    { value: "3", label: "3,000,000 - 5,000,000 VND" },
    { value: "4", label: "5,000,000 - 10,000,000 VND" },
    { value: "5", label: "10,000,000 - 50,000,000 VND" },
    { value: "6", label: "more 50,000,000 VND" },
  ];

  const optionsColors = [
    { value: "black", label: "black" },
    { value: "red", label: "red" },
    { value: "white", label: "white" },
    { value: "pasel", label: "pasel" },
    { value: "blue", label: "blue" },
    { value: "green", label: "green" },
  ];

  const optionsMaterial = [
    { value: "wood", label: "wood" },
    { value: "wool", label: "wool" },
    { value: "cotton", label: "cotton" },
  ];

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  useEffect(() => {
    setValue(0);
    axios
      .get(`http://localhost:3001/${match.params.product}`)
      .then((response) => {
        if (response.data.err) {
          alert(response.data.err);
        } else {
          setFurnitureCategory(response.data);
          if (response.data.length > 0) {
            axios
              .get(
                `http://localhost:3001/${match.params.product}/${response.data[0].category_name}`
              )
              .then((response) => {
                if (response.data.err) {
                  alert(response.data.err);
                } else {
                  setListPorducts(response.data);
                  // console.log(response.data);
                }
              });
          }
        }
      });
  }, [match]);

  const changeColor = (i) => {
    setToggleHeart(!toggleHeart);
    setIsHeart(i);
  };

  return (
    <Box
      className="tab-products"
      sx={{
        flexGrow: 1,
        bgcolor: "background.paper",
        display: "flex",
        height: 224,
      }}
    >
      <Tabs
        orientation="vertical"
        variant="scrollable"
        value={value}
        onChange={handleChange}
        aria-label="Vertical tabs example"
        sx={{ borderRight: 1, borderColor: "divider" }}
      >
        {furnitureCategory.length > 0 &&
          furnitureCategory.map((item, i) => (
            <Tab
              label={item.category_name}
              {...a11yProps(`${i + 1}`)}
              key={i}
            />
          ))}
      </Tabs>

      {furnitureCategory.length > 0 &&
        furnitureCategory.map((item, i) => (
          <TabPanel value={value} index={i} key={i}>
            <Button variant="primary" onClick={handleShow}>
              All sort
            </Button>
            <Container className="product-lists">
              <Row>
                {listProducts &&
                  listProducts.map((product, i) => (
                    <Col lg={3} md={4} sm={6} key={i} className="mb-5">
                      <Card className="product-card h-100">
                        <RiHeart3Fill
                          className={
                            toggleHeart && isHeart === i
                              ? "hearts ml-auto active"
                              : "hearts ml-auto"
                          }
                          onClick={() => changeColor(i)}
                        />
                        <Card.Img
                          variant="top"
                          src={`images/${product.product_image}`}
                        />
                        <Card.Body>
                          <Card.Title>
                            <Link to={`products/${product.product_id}`}>
                              {product.product_name}
                            </Link>
                          </Card.Title>
                          <Card.Text>
                            {product.points ? (
                              <del>${product.price} </del>
                            ) : (
                              `${product.price} `
                            )}
                            <span className="monney-unit">VNĐ</span>
                            {product.points ? (
                              <>
                                <Loyalty className="sale-icon" />
                                <span className="total-sold-products">
                                  {product.points}
                                  {"%"}
                                  {/* <span className="monney-unit">sold</span> */}
                                </span>
                              </>
                            ) : (
                              ""
                            )}
                          </Card.Text>
                          <Card.Text>
                            {product.points > 0 ? (
                              <>
                                {" "}
                                {(parseFloat(product.price) *
                                  parseInt(product.points)) /
                                  100}{" "}
                                <span className="monney-unit">VNĐ</span>{" "}
                              </>
                            ) : (
                              ""
                            )}
                          </Card.Text>
                        </Card.Body>
                      </Card>
                    </Col>
                  ))}
              </Row>
            </Container>
          </TabPanel>
        ))}
      <Offcanvas show={show} onHide={handleClose} placement="end" name="end">
        <Offcanvas.Header closeButton>
          <Offcanvas.Title>Sort and Filter</Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
          <label className="my-3" style={{ fontWeight: "500" }}>
            Sort type
          </label>
          <Select options={optionsSort} />
          <label className="my-3" style={{ fontWeight: "500" }}>
            Price
          </label>
          <Select options={optionsPrice} />
          <label className="my-3" style={{ fontWeight: "500" }}>
            Colors
          </label>
          <Select options={optionsColors} />
          <label className="my-3" style={{ fontWeight: "500" }}>
            Material
          </label>
          <Select options={optionsMaterial} />
        </Offcanvas.Body>
      </Offcanvas>
    </Box>
  );
}
