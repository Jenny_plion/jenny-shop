import { useState, useEffect } from "react";
import axios from 'axios';
const useForm = (callback,validate) => {
    const [values, setValues] = useState({
        username: '',
        email: '',
        password: '',
        password2: ''
    })
    const [errors, setErrors] = useState({})
    const [isSubmitting, setIsSubmitting] = useState(false);
    const handleChange = e => {
        const {name, value} = e.target;
        setValues({
            ...values,
            [name]: value
        })
    }

    const handleSubmitSignUp = e => {
        e.preventDefault();
        setErrors(validate(values));
        setIsSubmitting(true);
    }

    useEffect(() => {
        if(Object.keys(errors).length === 0 && isSubmitting){
            addUser();
            callback();
        }
    })

    const addUser = () => {
        axios.post('http://localhost:3001/signup', {username: values.username.trim(), email: values.email.trim(), password: values.password.trim()}).then(()=>{
            console.log('Successfully!!!');
        })
    }

    return {handleChange, values, handleSubmitSignUp, errors}
}
export default useForm;
