export default function validateInfo(values){
    let errors = {}
    if(!values.username.trim()){
        errors.username = "Username is required"
    }

    if(!values.email.trim()){
        errors.email = "Email is required"
    }else if (!/^[A-Z8-9._%+-]+@[A-Z0-9,-]+\.[A-Z]{2,}$/i.test(values.email)){
        errors.email = "Invalid email"
    }

    if(!values.password.trim()){
        errors.password = "Password is required"
    }else if (values.password.lengh < 6){
        errors.password = 'Password needs to be 6 characters or more';
    }

    if(!values.password2.trim()){
        errors.password2= "Password is required"
    }else if (values.password2 !== values.password){
        errors.password = 'Password doesnt match';
    }

    return errors;
}