import React from 'react'


const FormSuccess = () => {
    return (
        <div className="form-content-right">
            <div>
                <h1 className='form-success'>You create an account successfully!</h1>
                <img src="images/img-3.svg" alt="" className="success-img mx-auto"/>
            </div>
            <a className="sign-after-register-link" href="/login">Sign in</a>
        </div>
    )
}

export default FormSuccess
