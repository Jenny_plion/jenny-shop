import React from "react";
import { Row, Container, Col} from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import '../../css/Footer.css';
import {Instagram, Facebook, Twitter, Linkedin} from "react-bootstrap-icons";
import Chatbot from "../chatbot/Chatbot";

function Footer() {
  return (
    <div className="bg-dark">
      <Container className="bg-dark footer">
        <Row>
          <Col>
            <p>INFORMATION</p>
            <ul className="text-left">
                <li>
                    <a href="https://www.instagram.com/">Delivery Information</a>
                </li>
                <li>
                    <a href="https://www.instagram.com/">Privacy Policy</a>
                </li>
                <li>
                    <a href="https://www.instagram.com/">Terms and Conditions</a>
                </li>
                <li>
                    <a href="https://www.instagram.com/">Contact</a>
                </li>
            </ul>
          </Col>
          <Col>
          <p>MY ACCOUNT</p>
            <ul>
                <li>
                    <a href="https://www.instagram.com/">My account</a>
                </li>
                <li>
                    <a href="https://www.instagram.com/">Order history</a>
                </li>
                <li>
                    <a href="https://www.instagram.com/">Shipping</a>
                </li>
                <li>
                    <a href="https://www.instagram.com/">Privacy Policy</a>
                </li>
                <li>
                    <a href="https://www.instagram.com/">Help</a>
                </li>
            </ul>
          </Col>
          <Col>
          <p>CONNECT WITH US</p>
            <ul className="social-media-icons d-flex">
                <li>
                    <a href="https://www.instagram.com/"><Instagram/></a>
                </li>
                <li>
                    <a href="https://www.instagram.com/"><Facebook/></a>
                </li>
                <li>
                    <a href="https://www.instagram.com/"><Twitter/></a>
                </li>
                <li>
                    <a href="https://www.instagram.com/"><Linkedin/></a>
                </li>
            </ul>
          </Col>
          <Col>
          <p>PAYMENT METHODS</p>
            <ul>
                <li>
                    <img src="images/payment.png" alt="payment method"/>
                </li>
            </ul>
          </Col>
        </Row>
        <Row className="copyright">
            <p className="text-white text-center">Copyright 2021 © Jenny-shop. All right reserved.</p>
        </Row>
        <Row><Chatbot/></Row>
      </Container>
    </div>
  );
}

export default Footer;
