import { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
import { Navbar, Nav, Container, Offcanvas, ListGroup } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faUserAlt,
  faShoppingCart,
  faSignOutAlt,
  faIdBadge,
  faShippingFast,
  faKey,
} from "@fortawesome/free-solid-svg-icons";
import "../../css/Navbar.css";
import { Search } from "react-bootstrap-icons";
import { useSelector } from "react-redux";

import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import Button from "@mui/material/Button";
import List from "@mui/material/List";
import Divider from "@mui/material/Divider";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import * as React from "react";
import axios from "axios";
import { useDispatch } from "react-redux";
import { withRouter } from "react-router-dom";

function Navbarmenu({history}) {
  const state = useSelector((state) => state);
  const dispatch = useDispatch();
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [isLoginSuccess, setIsLoginSuccess] = useState(false);
  useEffect(() => {
    if (state.LoginReducer.responseData.username.length > 0) {
      setIsLoginSuccess(true);
      axios
        .post("http://localhost:3001/", {
          customer_id: state.LoginReducer.responseData.id_account,
        })
        .then((response) => {
          console.log(response.data[0].cartItem_id);
          dispatch({
            type: "current-login",
            payload: { count: response.data[0].cartItem_id },
          });
        });
    }
  }, [state.LoginReducer]);

  // useEffect(() => {
  //   if (state.LoginReducer.responseData.username.length > 0){

  //   })
  // }}, []);

  //Start handle product sidebar
  const [stateSideBar, setState] = React.useState({
    products: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...stateSideBar, [anchor]: open });
  };

  const list = (anchor) => (
    <Box
      sx={{ width: anchor === "top" || anchor === "bottom" ? "auto" : 250 }}
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <h5>Jenny shop</h5>
      <List>
        {[
          "Furniture",
          "Kitchen-Appliances",
          "Beds",
          "Storage-Organisation",
        ].map((text, index) => (
          <ListItem button key={text}>
            <Nav.Link className="list-products" as={NavLink} to={`/${text}`}>
              <ListItemText primary={text} />
            </Nav.Link>
          </ListItem>
        ))}
      </List>
      <Divider />
      <List>
        {[
          "Working-from-home",
          "Decoration",
          "Bathroom-products",
          "Outdoor-products",
        ].map((text, index) => (
          <ListItem button key={text}>
            <Nav.Link className="list-products" as={NavLink} to={`/${text}`}>
              <ListItemText primary={text} />
            </Nav.Link>
          </ListItem>
        ))}
      </List>
      <Divider />
      <List>
        {["Lighting", "Rugs-mats-flooring", "Baby-children", "Textiles"].map(
          (text, index) => (
            <ListItem button key={text}>
              <Nav.Link className="list-products" as={NavLink} to={`/${text}`}>
                <ListItemText primary={text} />
              </Nav.Link>
            </ListItem>
          )
        )}
      </List>
    </Box>
  );
  //End handle product sidebar
  const handleLogOut = () => {
    dispatch({ type: "log_out" });
    setIsLoginSuccess(false);
    handleClose();
  };

  const handleKeyDown = (event) => {
    if (event.key === "Enter"){
      history.push(`/search?productName=${event.target.value}`)
    }
  }

  return (
    <div>
      <Navbar fixed="top" collapseOnSelect expand="lg" bg="dark" variant="dark">
        <Container>
          <Navbar.Brand as={NavLink} to={"/"}>
            HOME
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link as={NavLink} to={"/aboutus"}>
                ABOUT US
              </Nav.Link>
              {["products"].map((anchor) => (
                <React.Fragment key={anchor}>
                  <Button style={{color: "rgba(255,255,255,.55)", fontWeight: '700'}} onClick={toggleDrawer(anchor, true)}>{anchor}</Button>
                  <Drawer
                    anchor={anchor}
                    open={stateSideBar[anchor]}
                    onClose={toggleDrawer(anchor, false)}
                  >
                    {list(anchor)}
                  </Drawer>
                </React.Fragment>
              ))}
            </Nav>
            <Nav>
              <div className="search-navbar">
                <span className="search-navbar__icon text-white">
                  <Search />
                </span>
                <input
                  type="text"
                  className="search-navbar__input"
                  placeholder="type something"
                  onKeyDown={handleKeyDown}
                />
              </div>
              {isLoginSuccess === true ? (
                ""
              ) : (
                <Nav.Link as={NavLink} to={"/signup"}>
                  SIGN UP
                </Nav.Link>
              )}
              {isLoginSuccess === true ? (
                ""
              ) : (
                <Nav.Link as={NavLink} to={"/login"}>
                  LOG IN
                </Nav.Link>
              )}
              {isLoginSuccess === true ? (
                <Nav.Link
                  as={NavLink}
                  to={`/cart/${state.LoginReducer.responseData.id_account}`}
                  className="notification"
                >
                  <FontAwesomeIcon icon={faShoppingCart}></FontAwesomeIcon>
                  <span className="badge">
                    {state.QuantityBadgeReducer.count}
                  </span>
                </Nav.Link>
              ) : (
                ""
              )}
              <Nav.Link as={NavLink} to={"/"}>
                {isLoginSuccess === true ? (
                  <>
                    <FontAwesomeIcon
                      icon={faUserAlt}
                      onClick={handleShow}
                    ></FontAwesomeIcon>
                    <span className="userName">
                      {state.LoginReducer.responseData.username}
                    </span>
                  </>
                ) : (
                  <FontAwesomeIcon icon={faUserAlt}></FontAwesomeIcon>
                )}
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <div>
        <Offcanvas show={show} onHide={handleClose} placement="end" name="end">
          <Offcanvas.Header closeButton>
            <Offcanvas.Title>
              {state.LoginReducer.responseData.username}
            </Offcanvas.Title>
          </Offcanvas.Header>
          <Offcanvas.Body>
            <ListGroup variant="flush">
              <ListGroup.Item action>
                <Nav.Link as={NavLink} to={`/my-profile/${state.LoginReducer.responseData.id_account}`}>
                  <FontAwesomeIcon icon={faIdBadge}></FontAwesomeIcon> My
                  profile
                </Nav.Link>
              </ListGroup.Item>
              <ListGroup.Item action>
                <Nav.Link as={NavLink} to={"/my-order"}>
                  <FontAwesomeIcon icon={faShippingFast}></FontAwesomeIcon> My
                  order
                </Nav.Link>
              </ListGroup.Item>
              <ListGroup.Item action>
                <Nav.Link  as={NavLink} to={`/change-password/${state.LoginReducer.responseData.id_account}`} >
                  <FontAwesomeIcon icon={faKey}></FontAwesomeIcon> Change password
                </Nav.Link>
              </ListGroup.Item>
              <ListGroup.Item action onClick={handleLogOut}>
              <Nav.Link  as={NavLink} to={`/`} >
                <FontAwesomeIcon icon={faSignOutAlt}></FontAwesomeIcon> Log out
                </Nav.Link>
              </ListGroup.Item>
            </ListGroup>
          </Offcanvas.Body>
        </Offcanvas>
      </div>
    </div>
  );
}

export default withRouter(Navbarmenu);
