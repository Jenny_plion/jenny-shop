import * as React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Paper from '@mui/material/Paper';
import Stepper from '@mui/material/Stepper';
import Step from '@mui/material/Step';
import StepLabel from '@mui/material/StepLabel';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import AddressForm from './AddressForm';
import PaymentForm from './PaymentForm';
import Review from './Review.js';
import {useDispatch, useSelector} from 'react-redux';
import axios from "axios";

const steps = ['Shipping address', 'Payment details', 'Review your order'];

function getStepContent(step, formValues = null, changeFormValue = null) {
  switch (step) {
    case 0:
      return <AddressForm addressValues={formValues} changeAddressValue={changeFormValue}/>;
    case 1:
      return <PaymentForm paymentFormValues={formValues} changePaymentFormValue={changeFormValue} />;
    case 2:
      return <Review/>;
    default:
      throw new Error('Unknown step');
  }
}

const theme = createTheme();

export default function Checkout() {
  const [activeStep, setActiveStep] = React.useState(0);
  const [addressFormValues, setAddressFormValues] = React.useState({});
  const [paymentFormValues, setPaymentFormValues] = React.useState({});
  const dispatch = useDispatch();
  const state = useSelector((state) => state);
  const [isPlacedOrder, setIsPlacedOrder] = React.useState("");
  const handleNext = () => {
    setActiveStep(activeStep + 1);
    if(activeStep === 2){
      axios.post("http://localhost:3001/checkout",{kh_id: state.LoginReducer.responseData.id_account, order_list: 
      state.OrderItemsReducer.responseData}).then((res) => {
        if(res.data.err){
          alert("Something went wrong!!!")
        }else{
          setIsPlacedOrder(res.data);
        } 
      })
    }
  };

  const handleBack = () => {
    setActiveStep(activeStep - 1);
  };

  const changeAddressFormValue = (key, value) => {
    let values = { ...addressFormValues };
    values[key] = value;
    setAddressFormValues(values);
  };

  const changePaymentFormValue = (key, value) => {
    let values = { ...paymentFormValues };
    values[key] = value;
    setPaymentFormValues(values);
  };

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Container component="main" maxWidth="sm" sx={{ mb: 4 }}>
        <Paper variant="outlined" sx={{ my: { xs: 3, md: 6 }, p: { xs: 2, md: 3 } }}>
          <Typography component="h1" variant="h4" align="center">
            Checkout
          </Typography>
          <Stepper activeStep={activeStep} sx={{ pt: 3, pb: 5 }}>
            {steps.map((label) => (
              <Step key={label}>
                <StepLabel>{label}</StepLabel>
              </Step>
            ))}
          </Stepper>
          <React.Fragment>
            {activeStep === steps.length ? (
              <React.Fragment>
                <Typography variant="subtitle1">
                  {isPlacedOrder}
                </Typography>
              </React.Fragment>
            ) : (
              <React.Fragment>
                {/* {getStepContent(activeStep)} */}

                {
                  activeStep === 0 ? 
                    getStepContent(activeStep, addressFormValues, changeAddressFormValue)
                    : activeStep === 1 ?  getStepContent(activeStep, paymentFormValues, changePaymentFormValue)
                      : getStepContent(activeStep)
                }

                {activeStep === 0 ? <Button onClick={() => {
                  // All the address values will be availabe in addressFormValues object for further processes
                  console.log(addressFormValues.firstname);
                  dispatch({type: 'shipping_address', payload: {firstname: addressFormValues.firstname, lastname: addressFormValues.lastname, address1: addressFormValues.address1, address2: addressFormValues.address2, city: addressFormValues.city, state: addressFormValues.state, zipCode: addressFormValues.zipCode, country: addressFormValues.country}});
                }} >Submit Address</Button> : null}

                {activeStep === 1 ? <Button onClick={() => {
                  // All the address values will be availabe in paymentFormValues object for further processes
                  console.log(paymentFormValues);
                  dispatch({type: 'payment', payload: {nameOnCard: paymentFormValues.cardname, cardNumber: paymentFormValues.cardnumber, expireDate: paymentFormValues.expDate, cvv: paymentFormValues.cvv}});
                }} >Submit payment</Button> : null}

                <Box sx={{ display: 'flex', justifyContent: 'flex-end' }}>
                  {activeStep !== 0 && (
                    <Button onClick={handleBack} sx={{ mt: 3, ml: 1 }}>
                      Back
                    </Button>
                  )}

                  <Button
                    variant="contained"
                    onClick={handleNext}
                    sx={{ mt: 3, ml: 1 }}
                  >
                    {activeStep === steps.length - 1 ? 'Place order' : 'Next'}
                  </Button>
                </Box>
              </React.Fragment>
            )}
          </React.Fragment>
        </Paper>
      </Container>
    </ThemeProvider>
  );
}