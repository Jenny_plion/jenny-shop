import * as React from "react";
import Typography from "@mui/material/Typography";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import Grid from "@mui/material/Grid";
import { useSelector } from "react-redux";

export default function Review() {
  const state = useSelector((state) => state);
  var sum = state.OrderItemsReducer.responseData.reduce((a,v) => a + parseInt(v.quantity) * parseInt(v.price), 0);
  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Order summary
      </Typography>
      <List disablePadding>
        {state.OrderItemsReducer.responseData.map((product, i) => (
          <ListItem key={i} sx={{ py: 1, px: 0 }}>
            <ListItemText primary={product.product_name} />
            <Typography variant="body2">{product.quantity} x {product.price}</Typography>
          </ListItem>
        ))}

        <ListItem sx={{ py: 1, px: 0 }}>
          <ListItemText primary="Total" />
          <Typography variant="subtitle1" sx={{ fontWeight: 700 }}>
            {sum}
          </Typography>
        </ListItem>
      </List>
      <Grid container spacing={2}>
        <Grid item xs={12} sm={6}>
          <Typography variant="h6" gutterBottom sx={{ mt: 2 }}>
            Shipping
          </Typography>
          {state.CheckoutReducer.shippingAddressData && (
            <>
              <Typography gutterBottom>
                {state.CheckoutReducer.shippingAddressData.firstname +
                  " " +
                  state.CheckoutReducer.shippingAddressData.lastname}
              </Typography>
              <Typography gutterBottom>
                {state.CheckoutReducer.shippingAddressData.address1}
              </Typography>
            </>
          )}
        </Grid>
        <Grid item container direction="column" xs={12} sm={6}>
          <Typography variant="h6" gutterBottom sx={{ mt: 2 }}>
            Payment details
          </Typography>
          <Grid container>
            <React.Fragment>
              {state.PaymentReducer.paymentData && (
                <>
                  <Grid item xs={6}>
                    <Typography gutterBottom>Card type</Typography>
                    <Typography gutterBottom>Card holder</Typography>
                    <Typography gutterBottom>Card number</Typography>
                    <Typography gutterBottom>Expire date</Typography>
                  </Grid>
                  <Grid item xs={6}>
                  <Typography gutterBottom>
                      VISA
                    </Typography>
                    <Typography gutterBottom>
                      {state.PaymentReducer.paymentData.nameOnCard}
                    </Typography>
                    <Typography gutterBottom>
                      {state.PaymentReducer.paymentData.cardNumber}
                    </Typography>
                    <Typography gutterBottom>
                      {state.PaymentReducer.paymentData.expireDate}
                    </Typography>
                  </Grid>
                </>
              )}
            </React.Fragment>
          </Grid>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
