import { React, useEffect, useState } from "react";
import { Row, Container, Col, Image, Button, ButtonGroup, Card } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "../css/ProductDetail.css";
import axios from "axios";
import {useDispatch, useSelector} from 'react-redux';
import { withRouter } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Divider from '@mui/material/Divider';
import { Loyalty } from "@mui/icons-material";
import { Link } from "react-router-dom";
import LazyLoad from 'react-lazyload'

function ProductDetail({ match, history }) {
  const dispatch = useDispatch();
  const state = useSelector((state) => state);
  const [detailProducts, setDetailProducts] = useState([]);
  const id_account = state.LoginReducer.responseData.id_account;
  const [listProducts, setListPorducts] = useState([]);
  const [productType, setProductType] = useState('');

  useEffect(() => {
    //get list of product type
    axios
      .get(`http://localhost:3001/products/${match.params.id}`)
      .then((response) => {
        setDetailProducts(response.data);
        setProductType(response?.data?.[0]?.product_type_id);
      });
  }, [match]);

  useEffect(() => {
    axios
    .post(`http://localhost:3001/search-same-product`, {productTypeId: productType})
    .then((response) => {
      setListPorducts(response.data);
      console.log('data', response.data);
    });
  }, [productType])

  const handleAddToCatagory = (product_id) => {
    if (state.LoginReducer.responseData.username === "") {
      history.push("/login");
    } else {
      dispatch({ type: "add-to-cart-quantity", payload: { count: state.CountReducer.count} });

      axios
        .post(`http://localhost:3001/`, {
          customer_id: id_account,
          quantity: state.CountReducer.count,
          product_id: product_id,
        })
        .then((response) => {
          if (response.data.err) {
            toast("Something went wrong!!!");
          } else toast("Add 1 item to cart successfully!");
        });
    }
  };

  const handleBuyNow = (product_id) => {
    if (state.LoginReducer.responseData.username === "") {
      history.push("/login");
    } else {
      dispatch({ type: "add-to-cart-quantity", payload: { count: state.CountReducer.count} });
      history.push(`/cart/${id_account}`);
      axios
        .post(`http://localhost:3001/cart/${id_account}`, {
          customer_id: id_account,
          quantity: state.CountReducer.count,
          product_id: product_id,
        })
        .then((response) => {
          if (response.data.err) {
            alert("Something went wrong!!!");
          }
        });
    }
  };

  const Loading = () => (
    <div className="post loading">
      <h5>Loading...</h5>
    </div>
  )

  return (
    <div>
      <Container>
        <Row className="product-detail-row">
          <Col sm={6} className="text-center">
            {detailProducts.length > 0 && (detailProducts[0].product_image.includes("https:") ? 
              <Image style={{width: '500px', height: 'auto'}} src={`${detailProducts[0].product_image}`} rounded /> :
              <Image style={{width: '500px', height: 'auto'}} src={`../images/${detailProducts[0].product_image}`} rounded />)
            }
          </Col>
          <Col sm={6}>
            {detailProducts.length > 0 && (
              <Row className="product-detail">
                <h5>{detailProducts[0].product_name}</h5>
                <p>
                  Status:{" "}
                  <span className="status-of-product">
                    {detailProducts[0].product_status}
                  </span>
                </p>
                <h5>
                  {detailProducts[0].price}
                  <span className="unit-of-money"> VNĐ</span>
                </h5>
                <p>
                  Total sold: <span>{detailProducts[0].total_sold}</span>
                </p>

                <div className="divider"></div>

                <p className="detail-of-product">
                  {detailProducts[0].product_infomation}
                </p>
                <div className="divider"></div>
                <p>
                  Quantity:
                  <ButtonGroup
                    aria-label="Basic example"
                    className="quantity-group"
                  >
                    <Button variant="secondary" onClick = {() => {dispatch({type: 'decrease'})}}>-</Button>
                    <Button variant="outline-secondary">{state.CountReducer.count}</Button>
                    <Button variant="secondary" onClick = {() => {dispatch({type: 'increase'})}}>+</Button>
                  </ButtonGroup>
                </p>
                <Button variant="outline-dark" className="add-to-cart-btn" onClick={() =>
                            handleAddToCatagory(detailProducts[0].product_id)
                          }>
                  ADD TO CART
                </Button>
                <Button variant="outline-danger" className="add-to-cart-btn" onClick={() => handleBuyNow( detailProducts[0].product_id)}>
                  BUY NOW
                </Button>
              </Row>
            )}
          </Col>
        </Row>
        <h5>More products</h5>
        <Divider />
        <Row className="same-product">
                {listProducts &&
                  listProducts.map((product, i) => (
                    <LazyLoad once={true} offset={[-100, 100]}
                    placeholder={<Loading />}>
                    
                    <Col  key={i} className="mb-5">
                      <Card className="product-card h-100">
                        <Card.Img
                          variant="top"
                          src={`../images/${product.product_image}`}
                        />
                        <Card.Body>
                          <Card.Title>
                            <Link to={`/products/${product.product_id}`}>
                              {product.product_name}
                            </Link>
                          </Card.Title>
                          <Card.Text>
                            {product.points ? (
                              <del>${product.price} </del>
                            ) : (
                              `${product.price} `
                            )}
                            <span className="monney-unit">VNĐ</span>
                            {product.points ? (
                              <>
                                <Loyalty className="sale-icon" />
                                <span className="total-sold-products">
                                  {product.points}
                                  {"%"}
                                  {/* <span className="monney-unit">sold</span> */}
                                </span>
                              </>
                            ) : (
                              ""
                            )}
                          </Card.Text>
                          <Card.Text>
                            {product.points > 0 ? (
                              <>
                                {" "}
                                {(parseFloat(product.price) *
                                  parseInt(product.points)) /
                                  100}{" "}
                                <span className="monney-unit">VNĐ</span>{" "}
                              </>
                            ) : (
                              ""
                            )}
                          </Card.Text>
                        </Card.Body>
                      </Card>
                    </Col>
                 
                    </LazyLoad>
                  ))} 
              </Row>
      </Container>
      <ToastContainer />
    </div>
  );
}

export default withRouter(ProductDetail);
