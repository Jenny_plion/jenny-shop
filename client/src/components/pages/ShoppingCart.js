import React, { useState, useEffect } from "react";
import {
  Row,
  Container,
  Table,
  Image,
  Col,
  Button,
  Modal,
} from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../css/ShoppingCart.css";
import { Trash } from "react-bootstrap-icons";
import axios from "axios";
import { useForm } from "react-hook-form";
import { withRouter } from "react-router-dom";
import { useDispatch } from "react-redux";
import Checkbox from "@mui/material/Checkbox";
import { toast } from "react-toastify";

function ShoppingCart({ match, history }) {
  const dispatch = useDispatch();
  const [cartItems, setCartItems] = useState([]);
  useEffect(() => {
    //get list of product type
    axios
      .get(`http://localhost:3001/cart/${match.params.id}`)
      .then((response) => {
        if (response.data.err) {
          alert("Something went wrong!!!");
        } else {
          setCartItems(response.data);
        }
      });
  }, []);

  const handleInputQuantity = (e, i) => {
    let newCartItems = [...cartItems]; // copying the old datas array
    newCartItems[i].quantity = e.target.value; // replace e.target.value with whatever you want to change it to
    setCartItems(newCartItems);
  };

  const [show, setShow] = useState(false);
  //Handle show or close modal confirm delete products
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [isDeleteCartItem, setIsDeleteCartItem] = useState({
    confirmDelete: false,
    id: null,
    quan: null,
  });

  const handleDeleteCartItems = (i, quantity) => {
    handleShow();
    setIsDeleteCartItem({ confirmDelete: true, id: i, quan: quantity });
  };

  const confirmDelete = () => {
    if (isDeleteCartItem.confirmDelete) {
      axios
        .post(`http://localhost:3001/cart/delete`, {
          id_customer: match.params.id,
          cartItems_id: isDeleteCartItem.id,
        })
        .then((response) => {
          if (response.data.err) {
            alert(response.data.err);
          } else {
            dispatch({
              type: "delete-cart",
              payload: { count: isDeleteCartItem.quan },
            });
            handleClose();
            setCartItems(
              cartItems.filter(
                (item) => item.product_id !== isDeleteCartItem.id
              )
            );
          }
        });
    }
  };

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = (data) => {
    // console.log(data);
  };
  const [discountInput, setDiscountInput] = useState("");
  const [voucherInput, setVoucherInput] = useState("");
  // const [shippingOptionInput, setShippingOptionInput] = useState("");
  const handleDiscount = (e) => {
    axios
      .post(`http://localhost:3001/cart/discount/${match.params.id}`, {
        discountInput: e.target.value,
      })
      .then((response) => {
        if (response.data.err) {
          alert(response.data.err);
        } else {
          if (response.data.length > 0) {
            // console.log(response.data[0].points);
            setDiscountInput(response.data[0].points);
          }
        }
      });
  };

  const handleVoucher = (e) => {
    axios
      .post(`http://localhost:3001/cart/${match.params.id}`, {
        voucherInput: e.target.value,
      })
      .then((response) => {
        if (response.data.err) {
          alert(response.data.err);
        } else {
          if (response.data.length > 0) {
            setVoucherInput(response.data[0].points);
          }
        }
      });
  };

  const [listOrderItems, setListOrderItems] = useState([]);
  const handleCheckbox = (e) => {
    const productId = e.target.value;
    let newArray = [...listOrderItems, productId];
    if (listOrderItems.includes(productId)) {
      newArray = newArray.filter((p) => p !== productId);
    }
    setListOrderItems(newArray);
  };
  // console.log(listOrderItems);
  // console.log(cartItems);

  var sum = listOrderItems.reduce(
    (a, v) =>
      listOrderItems.length > 0 && cartItems[v]
        ? a + parseInt(cartItems[v].quantity) * parseInt(cartItems[v].price)
        : 0,
    0
  );
  let list = listOrderItems.map((item) => cartItems[item]); //List of checked order items
  //Handle click checkout button
  const handleClickCheckout = () => {
    if (list.length > 0) {
      history.push("/checkout");
      dispatch({ type: "check-out", payload: list });
    } else toast("Choose an order item above!!!");
  };
  return (
    <div>
      <Container>
        <Row>
          <h5 className="shopping-cart-title">Shopping cart</h5>
          <Table responsive className="shopping-cart-table">
            <thead>
              <tr>
                <th>#</th>
                <th></th>
                <th>Product Image</th>
                <th>Product Name</th>
                <th>Product Infomation</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Total price</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {cartItems &&
                cartItems.map((item, i) => (
                  <tr key={i}>
                    <td>{i + 1}</td>
                    <td>
                      <Checkbox value={i} onChange={(e) => handleCheckbox(e)} />
                    </td>
                    <td>
                      <Image src={`/images/${item.product_image}`} />
                    </td>
                    <td>{item.product_name}</td>
                    <td>{item.product_name}</td>
                    <td>
                      <input
                        className="text-center"
                        type="number"
                        id="quantity"
                        defaultValue={item.quantity}
                        onChange={(e) => handleInputQuantity(e, i)}
                        name="quantity"
                        min="1"
                        max="5"
                      />
                    </td>
                    <td>{item.price}</td>
                    <td>{parseInt(item.price) * parseInt(item.quantity)}</td>
                    <td className="remove-product-from-cart">
                      <Trash
                        onClick={() =>
                          handleDeleteCartItems(item.product_id, item.quantity)
                        }
                      />
                    </td>
                  </tr>
                ))}
            </tbody>
          </Table>
        </Row>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Row className="shopping-cart-checkout d-flex justify-content-end">
            <Col sm={4}>
              <p>Do you have a discount code?</p>
              <input
                type="text"
                placeholder="Enter your discount code"
                {...register("Discount", { maxLength: 100 })}
                onChange={(e) => handleDiscount(e)}
              />
              <p className="mt-2">Do you have a gift voucher?</p>
              <input
                type="text"
                placeholder="Enter your gift voucher code"
                {...register("Voucher", { maxLength: 100 })}
                onChange={(e) => handleVoucher(e)}
              />
            </Col>
            {/* <Col sm={5}>
              <p>Shipping option</p>
              <Form.Select
                aria-label="Default select example"
                {...register("ShippingOption", { required: true ,onChange: (e) => {setShippingOptionInput(e.target.value)}})}
                
              >
                <option value="0">Open this select menu</option>
                <option value="20000">GHTK</option>
                <option value="30000">Viettel Post</option>
                <option value="40000">JT</option>
              </Form.Select>
              {errors.ShippingOption && (
                <p className="error">Shipping option is required</p>
              )}
              <p className="mt-2">City</p>
              <Form.Select
                aria-label="Default select example"
                {...register("City", { required: true })}
              >
                <option value=" ">Open this select menu</option>
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
              </Form.Select>
              {errors.City && <p className="error">City is required</p>}
              <p className="mt-2">Your shipping destination</p>
              <Form.Select
                aria-label="Default select example"
                {...register("Destination", { required: true })}
              >
                <option value=" ">Open this select menu</option>
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
              </Form.Select>
              {errors.Destination && (
                <p className="error">Destination is required</p>
              )}
            </Col> */}
            <Col sm={4}>
              <p>Summary</p>
              <Table striped>
                <tbody>
                  <tr>
                    <td>Subtotal</td>
                    <td>
                      {sum} <span>VNĐ</span>
                    </td>
                  </tr>
                  <tr>
                    <td>Shipping</td>
                    <td>FREE</td>
                  </tr>
                  <tr>
                    <td>Discount</td>
                    <td>
                      {discountInput} <span>%</span>
                    </td>
                  </tr>
                  <tr>
                    <td>Gift voucher</td>
                    <td>
                      {voucherInput} <span>%</span>
                    </td>
                  </tr>
                  <tr className="total-pay-title">
                    <td className="text-left">TOTAL PAY</td>
                    <td>
                      {sum !== 0
                        ? sum -
                          (discountInput / 100) * sum -
                          (voucherInput / 100) * sum
                        : 0}{" "}
                      <span>VNĐ</span>
                    </td>
                  </tr>
                </tbody>
              </Table>
            </Col>
            <Row>
              <Button
                className="ml-auto shopping-cart-checkout__btn"
                variant="dark"
                type="submit"
                onClick={handleClickCheckout}
              >
                CHECKOUT
              </Button>
            </Row>
          </Row>
        </form>
      </Container>
      <Modal style={{ zIndex: "9999" }} show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Confirm delete form</Modal.Title>
        </Modal.Header>
        <Modal.Body>Are you sure to delete this cart item</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={confirmDelete}>
            Delete
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}

export default withRouter(ShoppingCart);
