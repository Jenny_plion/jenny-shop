import { React, useState } from "react";
import { Row, Container, Col, Image } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../css/Aboutus.css";

function Aboutus() {
  const stories = [
    {id: "s1", title: "John", body: "param 1" },
    {id: "s2", title: "John", body: "param 2" },
    {id: "s3", title: "John", body: "param 3" },
    {id: "s4", title: "John", body: "param 4" },
    {id: "s5", title: "John", body: "param 5" },
  ];

  const [isActive, setIsActive] = useState(false);
  // const [storyId, setStoryId] = useState();
  const handleAccordion = (i) => {
    if(isActive === i){
      return setIsActive(false);
    }
    setIsActive(i);
  }

  return (
    <div className="about-us">
      <h1>About us</h1>
      <Container>
        <Row className="row" gap={5}>
          <Col className="param">
            <h5 className="our-vision">Our vision</h5>
            <p>
              Our vision is to create a better everyday life for the many
              people. On this site you can discover exactly what this means and
              find out who we are, what we do and what we stand for.
            </p>
            <p>
              One brand, many companies, and many, many people – that’s us in a
              nutshell. Spread all over the world, we have a passion for home
              furnishing and an inspiring shared vision: to create a better
              everyday life for the many people. This, together with our
              straightforward business idea, shared values, and a culture based
              on the spirit of togetherness, guides us in everything we do.
            </p>
          </Col>
          <Col xs={6} md={{ span: 6, offset: 1 }}>
            <Image src="images/our-vision.jpeg" alt="our vision" fluid />
          </Col>
        </Row>
        <Row className="row">
          <Col xs={6} md={6}>
            <Image src="images/our-vision.jpeg" alt="our approach" fluid />
          </Col>
          <Col className="param" md={{ offset: 1 }}>
            <h5 className="our-approach">Our approach</h5>
            <p>
              To offer a wide range of well-designed, functional home furnishing
              products at prices so low, that as many people as possible will be
              able to afford them.
            </p>
            <p>
              To meet the needs of our customers we have a unique business model
              and value chain. It includes product development, design, supply,
              manufacture and sales. And of course, it starts and ends with our
              customers.
            </p>
          </Col>
        </Row>
        <Row className="row">
          <Col className="param">
            <h5 className="our-process">Our process</h5>
            <p>
              IKEA is not the work of one person alone, it is the result of many
              minds and many souls working together through many years of joy
              and hard work.
            </p>
            <p>
              Finding the smartest, most innovative, cost-efficient way of doing
              things is part of our DNA. We’ve always gone our own way, knowing
              that you can’t blaze a trail by walking the same path as everyone
              else.
            </p>
          </Col>
          <Col xs={6} md={{ span: 6, offset: 1 }}>
            <Image src="images/our-vision.jpeg" alt="our process" fluid />
          </Col>
        </Row>
        {/* <Row className=" faq-section justify-content-md-center">
          {stories.map((u, i) => (
            <>
              <button
                className={`accordion ${isActive === i ? 'active' : ''}`}
                onClick={() => handleAccordion(i)}
                key={i}
              >
                {u.title} <span className="plus-icon">{isActive === i ? '-' : `+`}</span>
              </button>
              <div className={`panel ${isActive === i ? "show" : ""}`}  key={u.id}>
                <p>{u.body}</p>
              </div>
            </>
          ))}
        </Row> */}
      </Container>
    </div>
  );
}

export default Aboutus;
