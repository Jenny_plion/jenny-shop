import React, { useState, useEffect } from "react";
import LazyLoad from "react-lazyload";
import { Row, Container, Col, Card } from "react-bootstrap";
import { Link } from "react-router-dom";
import { withRouter } from "react-router-dom";
import { Loyalty } from "@mui/icons-material";
import axios from "axios";
import "../../css/ProductDetail.css";
import Divider from "@mui/material/Divider";

function SearchProduct() {
  const [listProducts, setListPorducts] = useState([]);
  let productName = new URLSearchParams(window.location.search).get(
    "productName"
  );
  console.log("city", productName);
  useEffect(() => {
    axios
      .get(
        `http://localhost:3001/admin/manage-products/search?productName=${productName}`
      )
      .then((response) => {
        if (!response.data.err) setListPorducts(response.data);
      });
  }, [productName]);
  const Loading = () => (
    <div className="post loading">
      <h5>Loading...</h5>
    </div>
  );
  return (
    <div>
      <Container>
           {listProducts?.length === 0 ? <h5>No results</h5> : (
              <>
               <h5>This is results...</h5>
                <Divider />
              </>
          )}
        <Row className="product-search">
          {listProducts?.length > 0 &&
            listProducts.map((product, i) => (
                <LazyLoad
                  once={true}
                  offset={[-100, 100]}
                  placeholder={<Loading />}
                >
                  <Col key={i} className="mb-5">
                    <Card className="product-card h-100">
                      <Card.Img
                        variant="top"
                        src={`../images/${product.product_image}`}
                      />
                      <Card.Body>
                        <Card.Title>
                          <Link to={`products/${product.product_id}`}>
                            {product.product_name}
                          </Link>
                        </Card.Title>
                        <Card.Text>
                          {product.points ? (
                            <del>${product.price} </del>
                          ) : (
                            `${product.price} `
                          )}
                          <span className="monney-unit">VNĐ</span>
                          {product.points ? (
                            <>
                              <Loyalty className="sale-icon" />
                              <span className="total-sold-products">
                                {product.points}
                                {"%"}
                                {/* <span className="monney-unit">sold</span> */}
                              </span>
                            </>
                          ) : (
                            ""
                          )}
                        </Card.Text>
                        <Card.Text>
                          {product.points > 0 ? (
                            <>
                              {" "}
                              {(parseFloat(product.price) *
                                parseInt(product.points)) /
                                100}{" "}
                              <span className="monney-unit">VNĐ</span>{" "}
                            </>
                          ) : (
                            ""
                          )}
                        </Card.Text>
                      </Card.Body>
                    </Card>
                  </Col>
                </LazyLoad>
            ))}
        </Row>
      </Container>
    </div>
  );
}

export default withRouter(SearchProduct);
