import React from "react";
import { withRouter } from "react-router-dom";
import { Container, Row, Col, Button } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { useForm } from "react-hook-form";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import axios from 'axios';

function ChangePassword({ match, history }) {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = (data) => {
    if (data.NewPassword === data.ConfirmNewPassword) {
      axios
        .post(`http://localhost:3001/change-password/${match.params.id}`, {
          oldPassword: data.OldPassword,
          newPassword: data.NewPassword,
          id_user: match.params.id
        })
        .then((response) => {
          console.log(response);
          if (response.data.err) {
            toast(response.data, { autoClose: 5000 });
          } else toast(response.data, {
            autoClose: 5000 });
        });
    } else toast("Nhập mật khẩu mới chính xác với ô confirm password");
  };
  return (
    <div>
      <Container>
        <Row>
          <Col>
            <form onSubmit={handleSubmit(onSubmit)}>
              <Row className="justify-content-center">
                <Col sm={12} md={5}>
                  <h5 className="mb-3">Change password</h5>
                  <div className="d-flex flex-column">
                    <label
                      style={{ fontWeight: 700 }}
                      className="mb-2"
                      htmlFor="Old password"
                    >
                      Old password <span style={{ color: "red" }}>*</span>
                    </label>
                    <input
                      className="py-2"
                      type="password"
                      defaultValue={""}
                      placeholder="Enter old password"
                      {...register("OldPassword", {
                        required: true,
                        maxLength: 80,
                      })}
                    />
                  </div>
                  {errors.OldPassword && (
                    <p className="error">Old password is required</p>
                  )}

                  <div className="d-flex flex-column">
                    <label
                      style={{ fontWeight: 700 }}
                      className="mb-2 mt-4"
                      htmlFor="New password"
                    >
                      New password <span style={{ color: "red" }}>*</span>
                    </label>
                    <input
                      className="py-2"
                      type="password"
                      defaultValue={""}
                      placeholder="Enter new password"
                      {...register("NewPassword", {
                        required: true,
                        maxLength: 80,
                      })}
                    />
                  </div>
                  {errors.NewPassword && (
                    <p className="error">New password is required</p>
                  )}

                  <div className="d-flex flex-column">
                    <label
                      style={{ fontWeight: 700 }}
                      className="mb-2 mt-4"
                      htmlFor="Confirmpassword"
                    >
                      Confirm new password{" "}
                      <span style={{ color: "red" }}>*</span>
                    </label>
                    <input
                      className="py-2"
                      type="password"
                      defaultValue={""}
                      placeholder="Comfirm new password"
                      {...register("ConfirmNewPassword", {
                        required: true,
                        maxLength: 80,
                      })}
                    />
                  </div>
                  {errors.ConfirmNewPassword && (
                    <p className="error">Confirm new password is required</p>
                  )}
                  <div className="d-flex justify-content-end mb-5 mt-2">
                    <Button
                      id="submit-edit-products-btn"
                      type="submit"
                      variant="primary"
                    >
                      Submit
                    </Button>
                  </div>
                </Col>
              </Row>
            </form>
          </Col>
        </Row>
      </Container>
      <ToastContainer />
    </div>
  );
}

export default withRouter(ChangePassword);
