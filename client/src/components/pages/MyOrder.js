import React, { useState, useEffect } from "react";
import {
  Row,
  Container,
  Table,
  Image,
  Button,
  Modal,
} from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../css/ShoppingCart.css";
import axios from "axios";
import {withRouter} from "react-router-dom";
import { useSelector} from 'react-redux';

function MyOrder({ match, history }) {
  const [myOrder, setMyOrder] = useState([]);
  const state = useSelector((state) => state);
  const id_account = state.LoginReducer.responseData.id_account;
  console.log(id_account)
  useEffect(() => {
    //get list of product type
    axios
      .post(`http://localhost:3001/my-order`, {kh_id: id_account})
      .then((response) => {
        if (response.data.err) {
          alert("Something went wrong!!!");
        } else {
          setMyOrder(JSON.parse(response.data)[0].list_order_items)
        }
      });
  }, []);

  const [show, setShow] = useState(false);
  //Handle show or close modal confirm delete products
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [isDeleteCartItem, setIsDeleteCartItem] = useState({
    confirmDelete: false,
    id: null,
  });

  const handleDeleteCartItems = (i, status) => {
      if(status === "Comfirmed by shop owner"){
        handleShow();
        setIsDeleteCartItem({ confirmDelete: true, id: i });
      }
   
  };
  console.log(isDeleteCartItem)

  const confirmDelete = () => {
    if (isDeleteCartItem.confirmDelete) {
      axios
        .post(`http://localhost:3001/my-order`, {
          order_item_id: isDeleteCartItem.id,
        })
        .then((response) => {
          if (response.data.err) {
            alert(response.data.err);
          } else {
            handleClose();
            setMyOrder(
              myOrder.filter(
                (item) => item.order_items_id !== isDeleteCartItem.id
              )
            );
          }
        });
    }
  };

  return (
    <div>
      <Container>
        <Row style={{marginTop: '-100px'}}>
          <h5 className="shopping-cart-title">My order</h5>
          <Table responsive className="shopping-cart-table">
            <thead>
              <tr>
                <th>#</th>
                <th>Product Image</th>
                <th>Product Name</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Status</th>
                <th>Ordered date</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {myOrder && myOrder.length > 0 &&
                myOrder.map((item, i) => (
                  <tr key={i}>
                    <td>{i + 1}</td>
                    <td>
                      <Image src={`/images/${item.product_image}`} />
                    </td>
                    <td>{item.product_name}</td>
                    
                    <td>
                      <input
                        className="text-center"
                        type="number"
                        id="quantity"
                        value={item.quantity}
                        readOnly
                        name="quantity"
                        min="1"
                        max="5"
                      />
                    </td>
                    <td>{parseInt(item.price) * parseInt(item.quantity)}</td>
                    <td>{item.status}</td>
                    <td>{item.ordered_date}</td>
                    <td className="remove-product-from-cart">
                      <Button variant="danger" onClick={() => handleDeleteCartItems(item.order_items_id, item.status)}>Cancel</Button>{' '}
                      <Button variant="success">View tracking</Button>
                    </td>
                  </tr>
                ))}
            </tbody>
          </Table>
        </Row>
      </Container>
      <Modal style={{ zIndex: "9999" }} show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Confirm delete form</Modal.Title>
        </Modal.Header>
        <Modal.Body>Are you sure to delete this cart item</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={confirmDelete}>
            Delete
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}

export default withRouter(MyOrder);
