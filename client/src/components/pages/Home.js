import { React, useEffect, useState } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { Row, Container, Image, Card, Col } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../css/Home.css";
import axios from "axios";
import { Loyalty } from "@mui/icons-material";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { withRouter } from "react-router-dom";
import { useDispatch } from "react-redux";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Banner from "../Banner";

// import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
// import { faChevronCircleLeft, faChevronCircleRight } from '@fortawesome/free-solid-svg-icons';
// import ButtonBase from "@material-ui/core/ButtonBase";

function Home({ history }) {
  const [productTypeList, SetProductTypeList] = useState([]);
  const [topProductByFilter, setTopProductByFilter] = useState([]);
  const [isClicked, setIsClicked] = useState(false);
  const state = useSelector((state) => state);
  const id_account = state.LoginReducer.responseData.id_account;
  const dispatch = useDispatch();

  useEffect(() => {
    //get list of product type
    axios.get("http://localhost:3001/projects-type").then((response) => {
      SetProductTypeList(response.data);
    });

    axios
      .post("http://localhost:3001/filter-products-type", {
        product_type_id: "js-02",
      })
      .then((response) => {
        setTopProductByFilter(response.data);
      });
  }, []);

  const filterTopProducts = (productTypeId) => {
    setIsClicked(productTypeId);
    axios
      .post("http://localhost:3001/filter-products-type", {
        product_type_id: productTypeId,
      })
      .then((response) => {
        setTopProductByFilter(response.data);
      });
  };

  var settings = {
    dots: false,
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    pauseOnHover: true,
    arrows: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: true,
          arrows: false,
        },
      },
    ],
  };

  const handleAddToCatagory = (i, product_id) => {
    if (state.LoginReducer.responseData.username === "") {
      history.push("/login");
    } else {
      dispatch({ type: "add-to-cart" });

      axios
        .post(`http://localhost:3001/`, {
          customer_id: id_account,
          quantity: 1,
          product_id: product_id,
        })
        .then((response) => {
          if (response.data.err) {
            toast("Something went wrong!!!");
          } else toast("Add 1 item to cart successfully!");
        });
    }
  };

  const handleBuyNow = (i, product_id) => {
    if (state.LoginReducer.responseData.username === "") {
      history.push("/login");
    } else {
      dispatch({ type: "add-to-cart" });
      history.push(`/cart/${id_account}`);
      axios
        .post(`http://localhost:3001/cart/${id_account}`, {
          customer_id: id_account,
          quantity: 1,
          product_id: product_id,
        })
        .then((response) => {
          if (response.data.err) {
            alert("Something went wrong!!!");
          }
        });
    }
  };
  return (
    <div>
      <Container fluid style={{ padding: 0 }}>
        <Banner />
      </Container>
      <Container>
        <Row>
          <div className="banner-homepage"></div>
          <div className="get-inspired">
            <h5>Get inspired</h5>
            <div>
              <Slider {...settings}>
                <div>
                  <Image
                    className="get-inspired-images"
                    src="images/kitchen-table.jpg"
                  />
                </div>
                <div>
                  <Image
                    className="get-inspired-images"
                    src="images/hehe.webp"
                  />
                </div>
                <div>
                  <Image
                    className="get-inspired-images"
                    src="images/flower-pot.webp"
                  />
                </div>
                <div>
                  <Image
                    className="get-inspired-images"
                    src="images/decor-christmas.jpg"
                  />
                </div>
                <div>
                  <Image
                    className="get-inspired-images"
                    src="images/table-kitchen-2.webp"
                  />
                </div>
                <div>
                  <Image
                    className="get-inspired-images"
                    src="images/kitchen.webp"
                  />
                </div>
              </Slider>
            </div>
          </div>
        </Row>
        <Row>
          <h5>Top products</h5>
          <ul className="top-products-catagory row  text-center">
            {productTypeList.map((item, i) => (
              <li
                className="col-3 col-md"
                style={
                  isClicked === item.product_type_id
                    ? {
                        color: "gray",
                        borderBottom: "3px solid #000",
                        padding: "5px 0",
                      }
                    : {}
                }
                onClick={() => filterTopProducts(item.product_type_id)}
                key={i}
              >
                {item.product_type_name}
              </li>
            ))}
          </ul>
          <div className="list-top-products-catagory">
            <Row>
              {topProductByFilter.map((product, i) => (
                <Col
                  sm={12}
                  lg={3}
                  md={4}
                  key={i}
                  className="d-flex justify-content-center"
                >
                  <Card
                    style={{ width: "18rem" }}
                    className="product-card h-100"
                  >
                    <span className="top-product">
                      Top {product.top_number}
                    </span>
                    {product.product_image?.includes("https://www.ikea.com") ?
                    <Card.Img variant="top" src={`${product.product_image}`} />
                    :
                    <Card.Img variant="top" src={`images/${product.product_image}`} />}
                    <Card.Body>
                      <Card.Title>
                        <Link to={`products/${product.product_id}`}>
                          {product.product_name}
                        </Link>
                      </Card.Title>
                      <Card.Text>
                        {product.points ? (
                          <del>${product.price} </del>
                        ) : (
                          `${product.price} `
                        )}
                        <span className="monney-unit">VNĐ</span>
                        {product.points ? (
                          <>
                            <Loyalty className="sale-icon" />
                            <span className="total-sold-products">
                              {product.points}
                              {"%"}
                              {/* <span className="monney-unit">sold</span> */}
                            </span>
                          </>
                        ) : (
                          ""
                        )}
                      </Card.Text>
                      <Card.Text>
                        {product.points > 0 ? (
                          <>
                            {" "}
                            {(parseFloat(product.price) *
                              parseInt(product.points)) /
                              100}{" "}
                            <span className="monney-unit">VNĐ</span>{" "}
                          </>
                        ) : (
                          ""
                        )}
                      </Card.Text>
                    </Card.Body>
                    <Card.Body>
                      <Card.Link href="#">
                        <button
                          type="button"
                          className="btn btn-outline-danger"
                          onClick={() => handleBuyNow(i, product.product_id)}
                        >
                          Buy now
                        </button>
                      </Card.Link>
                      <Card.Link href="#">
                        <button
                          type="button"
                          className="btn btn-outline-dark"
                          onClick={() =>
                            handleAddToCatagory(i, product.product_id)
                          }
                        >
                          Go to catagory
                        </button>
                      </Card.Link>
                    </Card.Body>
                  </Card>
                </Col>
              ))}
            </Row>
          </div>
        </Row>
      </Container>
      <Container fluid>
        <Row>
          <Col>
            <Image
              className="decoration-image img-responsive"
              width="100%"
              src="images/smLiving.webp"
            />
          </Col>
          <Col>
            <Row>
              <h1>What’s new in the IKEA range</h1>
              <p>
                As we start a new season, why not choose products that are
                friendlier on the environment? Browse our latest products that
                use pure wool and recycled fabrics for a better future.
              </p>
            </Row>
            {/* <Row>
        <Col md={6}>
          <Image className="decoration-image img-responsive" width = "100%" src="images/table1.webp"/>
        </Col>
        <Col md={6}>
          <Image className="decoration-image img-responsive" width = "100%" src="images/cussion.webp"/>
        </Col>
          </Row> */}
            <Row>
              <Image
                className="decoration-image img-responsive"
                width="100%"
                height="40%"
                src="images/sofa.webp"
              />
            </Row>
          </Col>
        </Row>
      </Container>
      <ToastContainer />
    </div>
  );
}

export default withRouter(Home);
