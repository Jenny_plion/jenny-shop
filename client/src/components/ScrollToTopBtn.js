import { React, useState, useEffect } from "react";
import "../css/ScrollToTopBtn.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronUp } from "@fortawesome/free-solid-svg-icons";

function ScrollToTopBtn() {
    // The back-to-top button is hidden at the beginning
    const [isDisplayScrollBtn, setIsDisplayScrollBtn] = useState(false);
    useEffect(() => {
        window.addEventListener("scroll", () => {
            if (window.pageYOffset > 300) {
                setIsDisplayScrollBtn(true);
            } else {
                setIsDisplayScrollBtn(false);
            }
        });
    }, []);

    const topFunction = () => {
        window.scrollTo({
            top: 0,
            behavior: "smooth",
        });
    };

    return (
        <div>
            {isDisplayScrollBtn && (
                <button
                    onClick={topFunction}
                    className="scroll-btn"
                    id="myBtn"
                    title="Go to top"
                >
                    <FontAwesomeIcon
                        className="arrow-up"
                        icon={faChevronUp}
                    ></FontAwesomeIcon>
                </button>
            )}
        </div>
    );
}

export default ScrollToTopBtn;
