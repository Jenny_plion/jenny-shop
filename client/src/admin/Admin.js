
import NavbarAdmin from "./components-admin/pagesAdmin/NavbarAdmin";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "./css-admin/admin.css";
import { React, useState, useEffect } from "react";

function Admin() {
  const [location, setLocation] = useState("http://localhost:3000/");
  useEffect(() => {
    setLocation(window.location.href);
  }, [window.location.href])
  return (
    <div className="App">
        <NavbarAdmin />
        {location  === 'http://localhost:3000/' ? (
          <div className="dashboard">
          <h5 style={{marginLeft: '100px'}}>Welcome to dashboard admin hehe!!!</h5>
          <img className="dashboard-bg" src="/images/dashboard-q16.jpg"></img>
        </div>
        ) : null }
        <ToastContainer />
    </div>
  );
}

export default Admin;
