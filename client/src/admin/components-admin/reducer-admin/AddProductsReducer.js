
var initialState = {};

function AddProductsReducer(state = initialState, action) {
    const { type} = action;
    switch(type){
        case 'addnew':
            return state;
        case 'edit':
            return state;
        default:
            return state;
    }
}

export default AddProductsReducer;