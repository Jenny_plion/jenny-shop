import { React, useEffect, useState } from "react";
import {
  Container,
  Row,
  Button,
  ButtonToolbar,
  ButtonGroup,
} from "react-bootstrap";
import axios from "axios";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import TextField from "@mui/material/TextField";
import DatePicker from "@mui/lab/DatePicker";
import "../../css-admin/admin.css";
import { Line } from "react-chartjs-2";
import { border } from "@mui/system";

function QuantityStatistics() {
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const d = new Date();
  const [month, setMonth] = useState(d.getMonth());
  const [year, setYear] = useState(d.getFullYear());
  const [totalQuantity, setTotalQuantity] = useState(null);
  const [totalOrderedProducts, setTotalOrderedProducts] = useState(0);
  const backgroundColor = [
    "rgba(255, 99, 132, 0.2)",
    "rgba(54, 162, 235, 0.2)",
    "rgba(255, 206, 86, 0.2)",
    "rgba(75, 192, 192, 0.2)",
    "rgba(153, 102, 255, 0.2)",
    "rgba(153, 102, 255, 0.2)",
    "rgba(255, 159, 64, 0.2)",
    "rgba(153, 100, 275, 0.2)",
    "rgba(153, 150, 255, 0.2)",
    "rgba(255, 105, 64, 0.2)",
    "rgba(153, 102, 255, 0.2)",
    "rgba(255, 159, 64, 0.2)",
  ]

  const borderColor = [
    "rgba(255, 99, 132, 1)",
    "rgba(54, 162, 235, 1)",
    "rgba(255, 206, 86, 1)",
    "rgba(75, 192, 192, 1)",
    "rgba(153, 102, 255, 1)",
    "rgba(153, 102, 255, 0.2)",
    "rgba(255, 159, 64, 1)",
    "rgba(255, 99, 132, 1)",
    "rgba(54, 162, 235, 1)",
    "rgba(255, 206, 86, 1)",
    "rgba(153, 102, 205, 0.2)",
    "rgba(255, 120, 64, 0.2)",
  ]

  useEffect(() => {
    axios
      .get("http://localhost:3001/admin/quantity-statistic")
      .then((response) => {
        if (response.data.err) {
          alert("something went wrong");
        } else {
          if(response.data.length > 0){
            setTotalOrderedProducts(
              response.data.map((itm) => itm.quantity).reduce((a, v) => a + v),
              0
            );
          }else setTotalOrderedProducts(0);
        
          setTotalQuantity({
            labels: response.data.map((itm) => {
              switch (itm.month) {
                case 1:
                  return "January"
                case 2:
                  return  "February";
                case 3:
                  return "March";
                case 4:
                  return "April";
                case 5:
                  return "May";
                case 6:
                  return "June";
                case 7:
                  return "July";
                case 8:
                  return "August";
                case 9:
                  return "September";
                case 10:
                  return "October";
                case 11:
                  return "November";
                case 12:
                  return "December";
                default:
                  break;
              }
            }),
            datasets: [
              {
                label: "total ordered items",
                data: response.data.map((itm) => itm.quantity),
                backgroundColor: backgroundColor,
                borderColor: borderColor,
                borderWidth: 1,
              },
            ],
          });
        }
      });
  }, []);

  const handleSubmitSearch = () => {
    axios
    .post("http://localhost:3001/admin/quantity-statistic", {startDate: startDate, endDate: endDate})
    .then((response) => {
      if (response.data.err) {
        alert("something went wrong");
      } else {
        setTotalOrderedProducts(
          response.data.map((itm) => itm.quantity).reduce((a, v) => a + v),
          0
        );
        setTotalQuantity({
          labels: response.data.map((itm) => {
            switch (itm.month) {
              case 1:
                return "January"
              case 2:
                return  "February";
              case 3:
                return "March";
              case 4:
                return "April";
              case 5:
                return "May";
              case 6:
                return "June";
              case 7:
                return "July";
              case 8:
                return "August";
              case 9:
                return "September";
              case 10:
                return "October";
              case 11:
                return "November";
              case 12:
                return "December";
              default:
                break;
            }
          }),
          datasets: [
            {
              label: "total ordered items",
              data: response.data.map((itm) => itm.quantity),
              backgroundColor: backgroundColor,
              borderColor: borderColor,
              borderWidth: 1,
            },
          ],
        });
      }
    });
  }

  return (
    <div>
      <p style={{fontSize: '20px', fontWeight: 700}}>
        Statitics total number of sold products in each month in {year}
      </p>
      <Container>
        <Row className="dashboard-toolbar d-flex flex-row-reverse">
          <ButtonToolbar aria-label="Toolbar with button groups">
            <ButtonGroup className="me-2" aria-label="First group">
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DatePicker
                  label="Date start"
                  value={startDate}
                  onChange={(newValue) => {
                    setStartDate(newValue);
                  }}
                  renderInput={(params) => <TextField {...params} />}
                />
              </LocalizationProvider>
            </ButtonGroup>
            <ButtonGroup className="me-2" aria-label="Second group">
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DatePicker
                  minDate={startDate}
                  label="Date end"
                  value={endDate}
                  onChange={(newValue) => {
                    setEndDate(newValue);
                  }}
                  renderInput={(params) => <TextField {...params} />}
                />
              </LocalizationProvider>
            </ButtonGroup>
            <ButtonGroup aria-label="Third group">
              <Button type="submit" variant="primary" onClick={handleSubmitSearch}>
                Search
              </Button>
            </ButtonGroup>
          </ButtonToolbar>
        </Row>
        <Row>
          {totalQuantity !== null && (
            <Line
              data={totalQuantity}
              options={{
                legend: {
                  display: true,
                  position: "right",
                },
              }}
            />
          )}
          <p>Total ordered products are {totalOrderedProducts} (products)</p>
        </Row>
      </Container>
    </div>
  );
}

export default QuantityStatistics;
