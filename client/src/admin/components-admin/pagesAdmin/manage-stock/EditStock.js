
import { React, useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import "../../../css-admin/edit-products-admin-form.css";
import "bootstrap/dist/css/bootstrap.min.css";
import axios from "axios";
import { withRouter } from "react-router-dom";
import {  Button } from "react-bootstrap";
import { toast } from "react-toastify";
import Breadscrumb from "../Breadscrumb";

function EditStock({  match, history }) {
  const [voucherDetail, setVoucherDetail] = useState([]);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  useEffect(() => {
    //get list of product type
    axios.get(`http://localhost:3001/admin/manage-stock/edit-stock/${match.params.id}`).then((response) => {
        if(response.data.err){
            toast("Something went wrong")
        }else setVoucherDetail(response.data);
    });
  }, []);

  //handle submit to send updated data
  const onSubmit = (data) => {
    axios
      .put(
        `http://localhost:3001/admin/manage-stock/edit-stock/${match.params.id}`,
        {product_stock_id: data.ProductId, nhap_kho: data.NhapKho, ton_kho: data.TonKho, xuat_kho: data.XuatKho}
      )
      .then((res) => {
        history.push("/admin/manage-stock");
        toast("Edit stock successfully")
      })
      .catch((err) => {
        toast("something went wrong")
      });
  };

  

  //When user click cancel button then redirect to manage products page
  const handleCancelEditProducts = () => {
    history.push("/admin/manage-stock");
  };

  return (
    <div>
       <Breadscrumb link2="Manage stock" link3="Edit stock"/>
      {voucherDetail.length > 0 && (
        <>
          <form id="form-edit-orders" onSubmit={handleSubmit(onSubmit)}>
            <div className="d-flex edit-products">
              <label htmlFor="productId">Product Id *</label>
              <input
                readOnly
                defaultValue={voucherDetail[0].product_stock_id}
                type="text"
                placeholder="Product id"
                {...register("ProductId")}
              />
            </div>

            <div className="d-flex edit-products">
              <label htmlFor="productName">Số lượng nhập kho *</label>
              <input
                type="text"
                defaultValue={voucherDetail[0].so_luong_nhap_kho}
                placeholder="So luong nhap kho"
                {...register("NhapKho", { required: true, maxLength: 100 })}
              />
            </div>
            {errors.NhapKho && <p className="error">So luong nhap kho is required</p>}

            <div className="d-flex edit-products">
              <label htmlFor="productName">Số lượng tồn kho *</label>
              <input
                type="text"
                defaultValue={voucherDetail[0].so_luong_ton_kho}
                placeholder="So luong ton kho"
                {...register("TonKho", { required: true, maxLength: 100 })}
              />
            </div>
            {errors.TonKho && <p className="error">So luong ton kho is required</p>}

            <div className="d-flex edit-products">
              <label htmlFor="productName">Số lượng xuất kho *</label>
              <input
                type="text"
                defaultValue={voucherDetail[0].so_luong_xuat_kho}
                placeholder="So luong xuat kho"
                {...register("XuatKho", { required: true, maxLength: 100 })}
              />
            </div>
            {errors.XuatKho && <p className="error">So luong xuat kho is required</p>}

            <div className="d-flex justify-content-center">
              <Button
                onClick={handleCancelEditProducts}
                id="cancel-edit-products-btn"
                variant="secondary"
              >
                Cancel
              </Button>
              <Button
                id="submit-edit-products-btn"
                type="submit"
                variant="primary"
              >
                Submit
              </Button>
            </div>
          </form>
        </>
      )}
    </div>
  );
}

export default withRouter(EditStock) ;
