import { React, useState, useEffect } from "react";
import { Table, Button, Modal } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import axios from "axios";
import "../../../css-admin/admin.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash, faPen } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";
import SearchIcon from "@mui/icons-material/Search";
import { styled, alpha } from "@mui/material/styles";
import InputBase from "@mui/material/InputBase";
import Pagination from "@mui/material/Pagination";
import Stack from "@mui/material/Stack";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Breadscrumb from "../Breadscrumb";

function ManageStock() {
  const Search = styled("div")(({ theme }) => ({
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginBottom: theme.spacing(1),
    marginRight: theme.spacing(2),
    border: "1px solid #dee2e6",
    width: "20%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(1),
      width: "auto",
    },
  }));

  const SearchIconWrapper = styled("div")(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  }));

  const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: "inherit",
    "& .MuiInputBase-input": {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)})`,
      transition: theme.transitions.create("width"),
      width: "100%",
      [theme.breakpoints.up("sm")]: {
        width: "12ch",
        "&:focus": {
          width: "20ch",
        },
      },
    },
  }));

  //Get list of users account
  const [vouchers, setVouchers] = useState([]);
  const [totalPages, setTotalPages] = useState(1);
  useEffect(() => {
    //get list of product type
    axios
      .get(`http://localhost:3001/admin/manage-stock/page/1`)
      .then((response) => {
        setVouchers(response.data.listVoucher);
        setTotalPages(response.data.totalPages);
      });
  }, []);

  const [page, setPage] = useState(1);
  //Handle change page
  const handleChangePage = (event, value) => {
    axios
      .get(`http://localhost:3001/admin/manage-stock/page/${value}`)
      .then((response) => {
        setVouchers(response.data.listVoucher);
        setTotalPages(response.data.totalPages);
        setPage(value);
      });
  };

  const [show, setShow] = useState(false);
  //Handle show or close modal confirm delete products
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [isDeleteUser, setIsDeleteUser] = useState({
    confirmDelete: false,
    id: null,
  });

  const handleDelete = (i) => {
    handleShow();
    setIsDeleteUser({ confirmDelete: true, id: i });
  };

  const confirmDelete = () => {
    if (isDeleteUser.confirmDelete) {
      axios
        .post(`http://localhost:3001/admin/manage-stock/delete`, {
          id: isDeleteUser.id,
        })
        .then((response) => {
          if (response.data.err) {
            toast(response.data.err, { autoClose: 5000 });
          } else {
            handleClose();
            setVouchers(
              vouchers.filter((item) => item.discount_id !== isDeleteUser.id)
            );
            toast(response.data, { autoClose: 5000 });
          }
        });
    }
  };

  //Handle search by name
  const handleKeyDown = (event) => {
    if (event.key === "Enter") {
      axios
        .get(
          `http://localhost:3001/admin/manage-stock/search?stock=${event.target.value}`
        )
        .then((response) => {
          if (response.data.err) {
            toast("Something went wrong!!!");
          } else setVouchers(response.data);
        });
    }
  };

  return (
    <div>
       <Breadscrumb link2="Manage stock" />
      <div className="d-flex flex-row-reverse">
        <Button variant="info" className="add-new-products mb-2">
          <Link to="/admin/manage-stock/add-new-stock">Add new</Link>
        </Button>
        <Search>
          <SearchIconWrapper>
            <SearchIcon />
          </SearchIconWrapper>
          <StyledInputBase
            placeholder="Search…"
            inputProps={{ "aria-label": "search" }}
            onKeyDown={handleKeyDown}
          />
        </Search>
      </div>

      <Table striped bordered hover size="sm" className="text-center">
        <thead>
          <tr>
            <th>#</th>
            <th>Product id</th>
            <th>Product name</th>
            <th>Số lượng nhập kho</th>
            <th>Số lượng tồn kho</th>
            <th>Số lượng xuất kho</th>
            <th></th>
          </tr>
        </thead>
        <tbody className="view-products_table-body ">
          {vouchers.map((p, i) => (
            <tr key={i} className="text-">
              <td>{(page-1) * 10 + i + 1}</td>
              <td>{p.product_stock_id}</td>
              <td>{p.product_name}</td>
              <td>{p.so_luong_nhap_kho}</td>
              <td>{p.so_luong_ton_kho}</td>
              <td>{p.so_luong_xuat_kho}</td>
              <td className="d-flex justify-content-center">
                <div className="d-flex">
                  <Link className="action-edit-btn"
                    to={`/admin/manage-stock/edit-stock/${p.stock_id}`}
                  >
                    <FontAwesomeIcon
                      className="edit-product_btn mx-2"
                      icon={faPen}
                    ></FontAwesomeIcon>
                  </Link>
                  <Link className="action-delete-btn">
                  <FontAwesomeIcon
                    className="delete-user_btn mx-2"
                    icon={faTrash}
                    onClick={() => handleDelete(p.stock_id)}
                  ></FontAwesomeIcon>
                  </Link>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>

      {totalPages > 1 ? (
        <Stack spacing={2} className="ml-auto">
          <Pagination
            count={totalPages}
            color="primary"
            onChange={handleChangePage}
          />
        </Stack>
      ) : null}

      <Modal style={{ zIndex: "9999" }} show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Confirm delete form</Modal.Title>
        </Modal.Header>
        <Modal.Body>Are you sure to delete this voucher</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={confirmDelete}>
            Delete
          </Button>
        </Modal.Footer>
      </Modal>
      <ToastContainer />
    </div>
  );
}

export default ManageStock;
