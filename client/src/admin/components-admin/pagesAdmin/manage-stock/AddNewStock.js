import { React, useEffect, useState } from "react";
import { useForm, Controller } from "react-hook-form";
import "../../../css-admin/edit-products-admin-form.css";
import "bootstrap/dist/css/bootstrap.min.css";
import axios from "axios";
import { withRouter } from "react-router-dom";
import { Button } from "react-bootstrap";
import { toast } from "react-toastify";
import Select from "react-select";
import Breadscrumb from "../Breadscrumb";

function AddNewStock({ history }) {
  const [options, setOptions] = useState([]);
  useEffect(() => {
    axios.get("http://localhost:3001/admin/manage-stock/get-list-products").then((res)=>{
      if(res.data.err){
        toast(res.data.res);
      }else {
        const result = JSON.parse(res.data[0].json);
          setOptions(result);
      }
    })
  }, [])

  const {
    register,
    handleSubmit,
    formState: { errors },
    control,
  } = useForm();

  //handle submit to send updated data
  const onSubmit = (data) => {
    if(data.ProductId === ""){toast("Chọn tên sản phẩm muốn thêm");}
    else axios
      .post(`http://localhost:3001/admin/manage-stock/add-new-stock`, {
        product_stock_id: data.ProductId,
        nhap_kho: data.NhapKho,
        ton_kho: data.TonKho,
        xuat_kho: data.XuatKho,
      })
      .then((res) => {
        history.push("/admin/manage-stock");
        toast(res.data);
      })
      .catch((err) => {
        toast(err);
      });
  };

  //When user click cancel button then redirect to manage products page
  const handleCancelEditProducts = () => {
    history.push("/admin/manage-stock");
  };

  return (
    <div>
      <>
        <Breadscrumb link2="Manage stock" link3="Add new stock"/>
        <form id="form-edit-orders" onSubmit={handleSubmit(onSubmit)}>
          <div className="d-flex edit-products">
            <label htmlFor="productId">
              Product Id <span style={{ color: "red" }}>*</span>
            </label>
            {/* <input
                type="text"
                placeholder="Product id"
                {...register("ProductId")}
              /> */}
            <Controller
              control={control}
              name="ProductId"
              render={({ field: { onChange, value, ref } }) => (
                <>
                  <Select
                    inputRef={ref}
                    value={options.find(c => c.value === value)}
                    options={options}
                    onChange={val => onChange(val.value)}
                  />
                </>
              )}
            />
          </div>

          <div className="d-flex edit-products">
            <label htmlFor="productName">
              Số lượng nhập kho <span style={{ color: "red" }}>*</span>
            </label>
            <input
              type="text"
              placeholder="So luong nhap kho"
              {...register("NhapKho", { required: true, maxLength: 100 })}
            />
          </div>
          {errors.NhapKho && (
            <p className="error">So luong nhap kho is required</p>
          )}

          <div className="d-flex edit-products">
            <label htmlFor="productName">
              Số lượng tồn kho <span style={{ color: "red" }}>*</span>
            </label>
            <input
              type="text"
              placeholder="So luong ton kho"
              {...register("TonKho", { required: true, maxLength: 100 })}
            />
          </div>
          {errors.TonKho && (
            <p className="error">So luong ton kho is required</p>
          )}

          <div className="d-flex edit-products">
            <label htmlFor="productName">
              Số lượng xuất kho <span style={{ color: "red" }}>*</span>
            </label>
            <input
              type="text"
              placeholder="So luong xuat kho"
              {...register("XuatKho", { required: true, maxLength: 100 })}
            />
          </div>
          {errors.XuatKho && (
            <p className="error">So luong xuat kho is required</p>
          )}

          <div className="d-flex justify-content-center">
            <Button
              onClick={handleCancelEditProducts}
              id="cancel-edit-products-btn"
              variant="secondary"
            >
              Cancel
            </Button>
            <Button
              id="submit-edit-products-btn"
              type="submit"
              variant="primary"
            >
              Submit
            </Button>
          </div>
        </form>
      </>
    </div>
  );
}

export default withRouter(AddNewStock);
