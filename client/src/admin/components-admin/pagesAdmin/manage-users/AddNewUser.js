import { React} from "react";
import { useForm } from "react-hook-form";
import "../../../css-admin/edit-products-admin-form.css";
import { Button } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import axios from "axios";
import { withRouter } from "react-router-dom";
import Breadscrumb from "../Breadscrumb";
import {toast} from "react-toastify";

function AddNewUser({ history }) {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const onSubmit = (data) => {
    axios
      .post("http://localhost:3001/admin/manage-users/add-new-users", {
        id_customer: data.CustomerID,
        username: data.UserName,
        email: data.Email,
        password: data.Password,
        role: data.Role,
      })
      .then((response) => {
        if (response.data.err) {
          alert(response.data.err);
        } else {
          history.push("/admin/manage-users");
          toast(response.data)
        }
      });
  };
  //Cancel edit product
  const handleCancelAddNewUsers = () => {
    history.push("/admin/manage-users");
  };
  return (
    <div>
       <Breadscrumb link2="Manage users" link3="Add new user"/>
       
      <form id="form-edit-products" onSubmit={handleSubmit(onSubmit)}>
      <h5 className="mb-5">Add new user</h5>
        <div className="d-flex edit-products">
          <label htmlFor="customerId">Customer Id <span style={{color: 'red'}}>*</span></label>
          <input
            type="text"
            placeholder="Customer id"
            {...register("CustomerID", { required: true, maxLength: 80 })}
          />
        </div>
        {errors.CustomerID && <p className="error">Customer id is required</p>}

        <div className="d-flex edit-products">
          <label htmlFor="userName">Username <span style={{color: 'red'}}>*</span></label>
          <input
            type="text"
            placeholder="Username"
            {...register("UserName", { required: true, maxLength: 100 })}
          />
        </div>
        {errors.UserName && <p className="error">Username is required</p>}

        <div className="d-flex edit-products">
          <label htmlFor="email">Email <span style={{color: 'red'}}>*</span></label>
          <input
            type="text"
            placeholder="Email"
            {...register("Email", { required: true, maxLength: 100 })}
          />
        </div>
        {errors.Email && <p className="error">Email is required</p>}

        <div className="d-flex edit-products">
          <label htmlFor="Password">Password <span style={{color: 'red'}}>*</span></label>
          <input
            type="password"
            placeholder="Password"
            {...register("Password", { required: true })}
          />
        </div>
        {errors.Password && <p className="error">Password is required</p>}

        <div className="d-flex edit-products">
          <label htmlFor="Role">Role <span style={{color: 'red'}}>*</span></label>
          <select {...register("Role", { required: true })}>
            <option value=" ">Select...</option>
            <option value="user">User</option>
            <option value="admin">Admin</option>
          </select>
        </div>
        {errors.Role && <p className="error">Role is required</p>}

        <div className="d-flex justify-content-center">
          <Button
            onClick={handleCancelAddNewUsers}
            id="cancel-edit-products-btn"
            variant="secondary"
          >
            Cancel
          </Button>
          <Button id="submit-edit-products-btn" type="submit" variant="primary">
            Submit
          </Button>
        </div>
      </form>
    </div>
  );
}

export default withRouter(AddNewUser);
