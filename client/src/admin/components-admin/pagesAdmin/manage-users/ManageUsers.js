import { React, useState, useEffect } from "react";
import { Table, Button, Modal } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import axios from "axios";
import "../../../css-admin/admin.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";
import SearchIcon from "@mui/icons-material/Search";
import { styled, alpha } from "@mui/material/styles";
import InputBase from "@mui/material/InputBase";
import Pagination from "@mui/material/Pagination";
import Stack from "@mui/material/Stack";
import Breadscrumb from "../Breadscrumb";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function ManageUsers() {
  const Search = styled("div")(({ theme }) => ({
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginBottom: theme.spacing(1),
    marginRight: theme.spacing(2),
    border: "1px solid #dee2e6",
    width: "20%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(1),
      width: "auto",
    },
  }));

  const SearchIconWrapper = styled("div")(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  }));

  const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: "inherit",
    "& .MuiInputBase-input": {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)})`,
      transition: theme.transitions.create("width"),
      width: "100%",
      [theme.breakpoints.up("sm")]: {
        width: "12ch",
        "&:focus": {
          width: "20ch",
        },
      },
    },
  }));

  //Get list of users account
  const [users, setUsers] = useState([]);
  const [totalPages, setTotalPages] = useState(1);
  useEffect(() => {
    //get list of product type
    axios.get(`http://localhost:3001/admin/manage-users/page/1`).then((response) => {
      setUsers(response.data.listProduct);
      setTotalPages(response.data.totalPages);
      console.log(response)
    });
  }, []);

  const [page, setPage] = useState(1);
  //Handle change page
  const handleChangePage = (event, value) => {
    axios
      .get(`http://localhost:3001/admin/manage-users/page/${value}`)
      .then((response) => {
        setUsers(response.data.listProduct);
        setTotalPages(response.data.totalPages);
        console.log(response.data);
        setPage(value);
      });
  }

  const [show, setShow] = useState(false);
  //Handle show or close modal confirm delete products
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [isDeleteUser, setIsDeleteUser] = useState({
    confirmDelete: false,
    id: null,
  });

  const handleDelete = (i) => {
    handleShow();
    setIsDeleteUser({ confirmDelete: true, id: i });
  };

  const confirmDelete = () => {
    if (isDeleteUser.confirmDelete) {
      axios
        .delete(
          `http://localhost:3001/admin/manage-users/delete/${isDeleteUser.id}`
        )
        .then((response) => {
          if (response.data.err) {
            toast(response.data.err);
          } else {
            handleClose();
            setUsers(
              users.filter((item) => item.id_customer !== isDeleteUser.id)
            );
            toast(response.data);
          }
        });
    }
  };

  //Handle search by name
  const handleKeyDown = (event) => {
    if (event.key === "Enter") {
        axios
        .get(`http://localhost:3001/admin/manage-users/search?username=${event.target.value}`)
        .then((response) => {
          setUsers(response.data);
        });
    }
  };

  return (
    <div>
      <Breadscrumb link2="Manage users"/>
      <div className="d-flex flex-row-reverse">
        <Button variant="info" className="add-new-products mb-2">
          <Link to="/admin/manage-users/add-new-users">Add new</Link>
        </Button>
        <Search>
          <SearchIconWrapper>
            <SearchIcon />
          </SearchIconWrapper>
          <StyledInputBase
            placeholder="Search…"
            inputProps={{ "aria-label": "search" }}
            onKeyDown={handleKeyDown}
          />
        </Search>
        
      </div>

      <Table striped bordered hover size="sm" className="text-center">
        <thead>
          <tr>
            <th>#</th>
            <th>Customer id</th>
            <th>Username</th>
            <th>Password</th>
            <th>Email</th>
            <th>Role</th>
            <th></th>
          </tr>
        </thead>
        <tbody className="view-products_table-body ">
          {users.map((p, i) => (
            <tr key={i} className="text-">
              <td>{(page-1) * 10 + i + 1}</td>
              <td>{p.id_customer}</td>
              <td>{p.username}</td>
              <td>{p.password}</td>
              <td>{p.email}</td>
              <td>{p.role}</td>
              <td className="d-flex justify-content-center">
                <div className="d-flex">
                  <Link className="action-delete-btn">
                  <FontAwesomeIcon
                    className="delete-user_btn"
                    icon={faTrash}
                    onClick={() => handleDelete(p.id_customer)}
                  ></FontAwesomeIcon>
                  </Link>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>

      
      {totalPages > 1 ? <Stack spacing={2} className="ml-auto">
        <Pagination count={totalPages} color="primary" onChange={handleChangePage}/>
      </Stack> : null}

      <Modal style={{ zIndex: "9999" }} show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Confirm delete form</Modal.Title>
        </Modal.Header>
        <Modal.Body>Are you sure to delete this user</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={confirmDelete}>
            Delete
          </Button>
        </Modal.Footer>
      </Modal>
      <ToastContainer />
    </div>
  );
}

export default ManageUsers;
