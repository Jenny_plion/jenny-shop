import React, { useState, useEffect } from "react";
import { Container, Row, Col, Button, Image } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { useForm } from "react-hook-form";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCamera } from "@fortawesome/free-solid-svg-icons";
import axios from "axios";
import { withRouter } from "react-router-dom";
import Breadscrumb from "../Breadscrumb";
import {toast} from "react-toastify";

function ProfileSetting({ match, history }) {
  const [image_file, setImageFile] = useState(null);
  const [image_preview, setImagePreview] = useState("");
  const [customer, setCustomer] = useState("");
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = (data) => {
    console.log(data);
    if (image_file !== null) {
      console.log(image_file);
      let formData = new FormData();
      formData.append("myImage", image_file);
      formData.append("id_customer", data.CustomerId);
      formData.append("firstname", data.Firstname);
      formData.append("lastname", data.Lastname);
      formData.append("phonenumber", data.Phonenumber);
      formData.append("address", data.Address);

      axios
        .post(
          `http://localhost:3001/admin/setting-profile/${match.params.id}`,
          formData
        )
        .then((response) => {
          console.log(response);
          if (response.data.err) {
            toast(response.data.err);
          } else {
            history.push("/admin/dashboard");
            toast(response.data);
          }
        });
    }
  };

  //When user click cancel button then redirect to manage products page
  const handleCancelEditProducts = () => {
    history.push("/admin/dashboard")
  };

  // Image Preview Handler
  const handleImagePreview = (e) => {
    let image_as_base64 = URL.createObjectURL(e.target.files[0]);
    let image_as_files = e.target.files[0];
    setImageFile(image_as_files);
    setImagePreview(image_as_base64);
  };

  //get profile of customer
  useEffect(() => {
    axios
      .get(
        `http://localhost:3001/admin/setting-profile/get-customer/${match.params.id}`
      )
      .then((response) => {
        if (response.data.err) {
          alert(response.data.err);
        } else {
          setCustomer(response.data);
          console.log(response.data);
        }
      });
  }, [match]);
  return (
    <div>
      <Container>
        <Row>
        <Breadscrumb link2="Manage users" link3="Edit user"/>
          <Col sm={12} md={5}>
            <div className="d-flex flex-column justify-content-center align-items-start image-preview">
              {image_preview ? (
                <Image
                  src={image_preview}
                  alt="image preview"
                  roundedCircle
                />
              ) : (customer ? (
                <Image
                  src={`/images/${customer[0].customer_avatar}`}
                  alt="image preview"
                  roundedCircle
                />
              ) : (
                <Image
                  src="https://raw.githubusercontent.com/OlgaKoplik/CodePen/master/profile.jpg"
                  alt="image preview"
                  roundedCircle
                />
              ))}

              {/* image input field */}
              <div  className="avatar">
              <label htmlFor="file-upload" className="custom-file-upload">
                <FontAwesomeIcon icon={faCamera}></FontAwesomeIcon>
              </label>
              <input
                type="file"
                id="file-upload"
                onChange={handleImagePreview}
                name="myImage"
              />
              </div>
            </div>
          </Col>
          <Col sm={12} md={7}>
          <h5>User profile</h5>
            {customer && (
              <form onSubmit={handleSubmit(onSubmit)}>
                <div className="d-flex flex-column">
                  <label style={{fontWeight: 700}} className="mb-2" htmlFor="CustomerId">
                    Customer id *
                  </label>
                  <input
                    className="py-2"
                    value={match.params.id}
                    type="text"
                    placeholder="CustomerId"
                    {...register("CustomerId", {
                      required: true,
                      maxLength: 80,
                    })}
                  />
                </div>
                {errors.Username && (
                  <p className="error">Username is required</p>
                )}

                <div className="d-flex flex-column">
                  <label style={{fontWeight: 700}} className="mb-2 mt-4" htmlFor="Firstname">
                    Firstname *
                  </label>
                  <input
                    className="py-2"
                    type="text"
                    defaultValue={
                      customer[0].length === 0 ? "" : customer[0].firstname
                    }
                    placeholder="Firstname"
                    {...register("Firstname", {
                      required: true,
                      maxLength: 100,
                    })}
                  />
                </div>
                {errors.Firstname && (
                  <p className="error">Firstname is required</p>
                )}

                <div className="d-flex flex-column">
                  <label style={{fontWeight: 700}} className="mb-2 mt-4" htmlFor="Lastname">
                    Lastname *
                  </label>
                  <input
                    className="py-2"
                    type="text"
                    placeholder="Lastname"
                    {...register("Lastname", {
                      required: true,
                      maxLength: 100,
                    })}
                    defaultValue={
                      customer[0].length === 0 ? "" : customer[0].lastname
                    }
                  />
                </div>
                {errors.Lastname && (
                  <p className="error">Lastname is required</p>
                )}

                <div className="d-flex flex-column">
                  <label style={{fontWeight: 700}} className="mb-2 mt-4" htmlFor="Phonenumber">
                    Phone number *
                  </label>
                  <input
                    className="py-2"
                    type="text"
                    defaultValue={
                      customer[0].length === 0 ? "" : customer[0].phonenumber
                    }
                    placeholder="Phonenumber"
                    {...register("Phonenumber", { required: true })}
                  />
                </div>
                {errors.Phonenumber && (
                  <p className="error">Phonenumber is required</p>
                )}

                <div className="d-flex flex-column">
                  <label style={{fontWeight: 700}} className="mb-2 mt-4" htmlFor="Address">
                    Address *
                  </label>
                  <input
                    className="py-2"
                    type="text"
                    defaultValue={
                      customer[0].length === 0 ? "" : customer[0].address
                    }
                    placeholder="Address"
                    {...register("Address", { required: true })}
                  />
                </div>
                {errors.Address && <p className="error">Address is required</p>}

                <div className="d-flex justify-content-center">
                  <Button
                    onClick={handleCancelEditProducts}
                    id="cancel-edit-products-btn"
                    variant="secondary"
                  >
                    Cancel
                  </Button>
                  <Button
                    id="submit-edit-products-btn"
                    type="submit"
                    variant="primary"
                  >
                    Submit
                  </Button>
                </div>
              </form>
            )}
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default withRouter(ProfileSetting);
