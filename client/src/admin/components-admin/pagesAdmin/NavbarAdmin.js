import * as React from "react";
import { styled, useTheme } from "@mui/material/styles";
import Box from "@mui/material/Box";
import MuiDrawer from "@mui/material/Drawer";
import MuiAppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import CssBaseline from "@mui/material/CssBaseline";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import DashboardIcon from "@mui/icons-material/Dashboard";
import ManageAccountsIcon from "@mui/icons-material/ManageAccounts";
import { Link } from "react-router-dom";
import CategoryIcon from '@mui/icons-material/Category';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import ManageProducts from "./manage-product/ManageProducts";
import ManageUsers from "./manage-users/ManageUsers";
import Dashboard from "./Dashboard";
import EditProducts from "./manage-product/EditProducts";
import AddNewProduct from "./manage-product/AddNewProduct";
import AddNewUser from "./manage-users/AddNewUser";
import MenuItem from "@mui/material/MenuItem";
import AccountCircle from "@mui/icons-material/AccountCircle";
import NotificationsIcon from "@mui/icons-material/Notifications";
import Badge from "@mui/material/Badge";
import Menu from "@mui/material/Menu";
import ProfileSetting from "./manage-users/ProfileSetting";
import { useSelector } from "react-redux";
import ManageOrders from "./manage-order/ManageOrders";
import EditOrders from "./manage-order/EditOrders";
import {useDispatch} from 'react-redux';
import QuantityStatistics from "./QuantityStatistic";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChartBar, faBoxOpen, faCog, faTags, faBoxes } from "@fortawesome/free-solid-svg-icons";
import ManageVoucher from "./manage-voucher/ManageVoucher";
import EditVoucher from "./manage-voucher/EditVoucher";
import AddNewVoucher from "./manage-voucher/AddNewVoucher";
import ManageCatagory from "./manage-catagory/ManageCatagory";
import EditCatagory from "./manage-catagory/EditCatagory";
import AddNewCatagory from "./manage-catagory/AddNewCatagory";
import ManageStock from "./manage-stock/ManageStock";
import EditStock from "./manage-stock/EditStock";
import AddNewStock from "./manage-stock/AddNewStock";
import AddNewOrders from "./manage-order/AddNewOrders";

const drawerWidth = 240;

const openedMixin = (theme) => ({
  width: drawerWidth,
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen,
  }),
  overflowX: "hidden",
});

const closedMixin = (theme) => ({
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  overflowX: "hidden",
  width: `calc(${theme.spacing(7)} + 1px)`,
  [theme.breakpoints.up("sm")]: {
    width: `calc(${theme.spacing(9)} + 1px)`,
  },
});

const DrawerHeader = styled("div")(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  justifyContent: "flex-end",
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
}));

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(["width", "margin"], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  width: drawerWidth,
  flexShrink: 0,
  whiteSpace: "nowrap",
  boxSizing: "border-box",
  ...(open && {
    ...openedMixin(theme),
    "& .MuiDrawer-paper": openedMixin(theme),
  }),
  ...(!open && {
    ...closedMixin(theme),
    "& .MuiDrawer-paper": closedMixin(theme),
  }),
}));

const userstyle = {
  right: "40px",
  position: "absolute",
};

const notifystyle = {
  right: "100px",
  position: "absolute",
};

export default function MiniDrawer() {
  const state = useSelector((state) => state);
  const dispatch = useDispatch();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const isMenuOpen = Boolean(anchorEl);
  const menuId = "primary-search-account-menu";
  const handleProfileMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const handleLogout = () => {
    dispatch({type: 'log_out'});
  }

  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      id={menuId}
      keepMounted
      transformOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={handleMenuClose}>User: {state.LoginReducer.responseData.username}</MenuItem>
      <MenuItem onClick={handleMenuClose}>
        {" "}
        <Link className="text-dark" to={`/admin/setting-profile/get-customer/${state.LoginReducer.responseData.id_account}`}>Profile</Link>
      </MenuItem>
      <MenuItem onClick={handleLogout}><Link className="text-dark" to={`/`}>Log out</Link></MenuItem>
    </Menu>
  );

  return (
    <Router>
      <Switch>
        <Box sx={{ display: "flex" }}>
          <CssBaseline />
          <AppBar position="fixed" open={open}>
            <Toolbar>
              <IconButton
                color="inherit"
                aria-label="open drawer"
                onClick={handleDrawerOpen}
                edge="start"
                sx={{
                  marginRight: "36px",
                  ...(open && { display: "none" }),
                }}
              >
                <MenuIcon />
              </IconButton>
              <Typography variant="h6" noWrap component="div">
                Dashboard
              </Typography>
              <MenuItem style={notifystyle}>
                <IconButton
                  size="large"
                  aria-label="show 17 new notifications"
                  color="inherit"
                >
                  <Badge badgeContent={17} color="error">
                    <NotificationsIcon />
                  </Badge>
                </IconButton>
              </MenuItem>
              <MenuItem onClick={handleProfileMenuOpen} style={userstyle}>
                <IconButton
                  size="large"
                  aria-label="account of current user"
                  aria-controls="primary-search-account-menu"
                  aria-haspopup="true"
                  color="inherit"
                >
                  <AccountCircle />
                </IconButton>
              </MenuItem>
            </Toolbar>
            {renderMenu}
          </AppBar>
          <Drawer variant="permanent" open={open}>
            <DrawerHeader>
              <IconButton onClick={handleDrawerClose}>
                {theme.direction === "rtl" ? (
                  <ChevronRightIcon />
                ) : (
                  <ChevronLeftIcon />
                )}
              </IconButton>
            </DrawerHeader>
            <Divider />
            <List>
              <ListItem button>
                <ListItemIcon>
                  <Link to="/admin/dashboard">
                    <DashboardIcon />
                  </Link>
                </ListItemIcon>
                <ListItemText primary="Dashboard" />
              </ListItem>
              <ListItem button>
                <ListItemIcon>
                  <Link to="/admin/manage-users">
                    <ManageAccountsIcon />
                  </Link>
                </ListItemIcon>
                <ListItemText primary="Manage Users" />
              </ListItem>
              <ListItem button>
                <ListItemIcon>
                  <Link to="/admin/manage-products">
                  <FontAwesomeIcon icon={faCog} style={{fontSize: "20px"}}></FontAwesomeIcon>
                  </Link>
                </ListItemIcon>
                <ListItemText primary="Manage Products" />
              </ListItem>
              <ListItem button>
                <ListItemIcon>
                  <Link to="/admin/manage-orders">
                  <FontAwesomeIcon icon={faBoxOpen} style={{fontSize: "20px"}}></FontAwesomeIcon>
                  </Link>
                </ListItemIcon>
                <ListItemText primary="Manage Orders" />
              </ListItem>
              <ListItem button>
                <ListItemIcon>
                  <Link to="/admin/quantity-statistic">
                  <FontAwesomeIcon icon={faChartBar} style={{fontSize: "20px"}}></FontAwesomeIcon>
                  </Link>
                </ListItemIcon>
                <ListItemText primary="Quantity statistics" />
              </ListItem>
              <ListItem button>
                <ListItemIcon>
                  <Link to="/admin/manage-voucher">
                  <FontAwesomeIcon icon={faTags} style={{fontSize: "20px"}}></FontAwesomeIcon>
                  </Link>
                </ListItemIcon>
                <ListItemText primary="Manage voucher" />
              </ListItem>
              <ListItem button>
                <ListItemIcon>
                  <Link to="/admin/manage-catagory">
                  <CategoryIcon style={{fontSize: "28px"}}/>
                  </Link>
                </ListItemIcon>
                <ListItemText primary="Manage catagory" />
              </ListItem>
              <ListItem button>
                <ListItemIcon>
                  <Link to="/admin/manage-stock">
                  <FontAwesomeIcon icon={faBoxes} style={{fontSize: "20px"}}></FontAwesomeIcon>
                  </Link>
                </ListItemIcon>
                <ListItemText primary="Manage stock" />
              </ListItem>
            </List>
          </Drawer>
          <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
            <DrawerHeader />
            <Route path="/admin/dashboard" component={Dashboard}></Route>
            <Route
              path="/admin/manage-users"
              exact
              component={ManageUsers}
            ></Route>
            <Route
              path="/admin/manage-users/add-new-users"
              component={AddNewUser}
            ></Route>
            <Route
              path="/admin/manage-products"
              exact
              component={ManageProducts}
            ></Route>
            <Route
              path="/admin/manage-products/edit-products/:id"
              component={EditProducts}
            ></Route>
            <Route
              path="/admin/manage-products/add-new-products"
              component={AddNewProduct}
            ></Route>
            <Route
              path="/admin/setting-profile/get-customer/:id"
              component={ProfileSetting}
            ></Route>
            <Route
              path="/admin/manage-orders"
              exact
              component={ManageOrders}
            ></Route>
            <Route
              path="/admin/manage-orders/edit-orders/:id"
              component={EditOrders}
            ></Route>
             <Route
              path="/admin/manage-orders/add-new-order"
              component={AddNewOrders}
            ></Route>
            <Route
              path="/admin/quantity-statistic"
              component={QuantityStatistics}
            ></Route>
            <Route
              path="/admin/manage-voucher"
              exact
              component={ManageVoucher}
            ></Route>
            <Route
              path="/admin/manage-voucher/edit-voucher/:id"
              component={EditVoucher}
            ></Route>
            <Route
              path="/admin/manage-voucher/add-new-voucher"
              component={AddNewVoucher}
            ></Route>
            <Route
              path="/admin/manage-catagory"
              exact
              component={ManageCatagory}
            ></Route>
            <Route
              path="/admin/manage-catagory/edit-catagory/:id"
              component={EditCatagory}
            ></Route>
            <Route
              path="/admin/manage-catagory/add-new-catagory"
              component={AddNewCatagory}
            ></Route>
            <Route
              path="/admin/manage-stock"
              exact
              component={ManageStock}
            ></Route>
             <Route
              path="/admin/manage-stock/edit-stock/:id"
              component={EditStock}
            ></Route>
            <Route
              path="/admin/manage-stock/add-new-stock"
              component={AddNewStock}
            ></Route>
          </Box>
        </Box>
      </Switch>
    </Router>
  );
}
