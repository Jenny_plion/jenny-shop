import { React, useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import "../../../css-admin/edit-products-admin-form.css";
import "bootstrap/dist/css/bootstrap.min.css";
import axios from "axios";
import { withRouter } from "react-router-dom";
import { Table, Button, Modal } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPen, faTrash } from "@fortawesome/free-solid-svg-icons";
import Breadscrumb from "../Breadscrumb";
import { toast } from "react-toastify";

function EditOrders({ match, history }) {
  const [productDetail, setProductDetail] = useState([]);
  const [orderItemList, setOrderItemList] = useState([]);
  const [customerInformation, setCustomerInformation] = useState([]);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  //handle submit to send updated data
  const onSubmit = (data) => {
    axios
      .put(
        `http://localhost:3001/admin/manage-orders/edit-orders/${match.params.id}`,
        { ordered_date: data.Date, status: data.Status, order_id: data.OrderID }
      )
      .then((res) => {
        history.push("/admin/manage-orders");
        toast(res.data);
      })
      .catch((err) => {
        // console.log(err);
      });
  };

  //get list of product type
  useEffect(() => {
    axios
      .get(
        `http://localhost:3001/admin/manage-orders/edit-orders/${match.params.id}`
      )
      .then((response) => {
        if (response.data.err) {
          alert(response.data.err);
        } else {
          setProductDetail(JSON.parse(response.data));
          setOrderItemList(JSON.parse(response.data)[0].list_order_items);
          setCustomerInformation(
            JSON.parse(response.data)[0].customer_information
          );
        }
      });
  }, [match]);

  //When user click cancel button then redirect to manage products page
  const handleCancelEditProducts = () => {
    history.push("/admin/manage-orders");
  };
  // console.log(productDetail );
  //When user click delete a product in list of order items
  const [show, setShow] = useState(false);
  const [showEdit, setShowEdit] = useState(false);
  //Handle show or close modal confirm delete products
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const handleCloseEdit = () => setShowEdit(false);
  const [isDeleteOrderItem, setIsDeleteOrderItem] = useState({
    confirmDelete: false,
    id: null,
  });

  const handleDeleteProduct = (i) => {
    handleShow();
    setIsDeleteOrderItem({ confirmDelete: true, id: i });
  };

  const confirmDelete = () => {
    // console.log(isDeleteOrderItem.id);
    if (isDeleteOrderItem.confirmDelete) {
      axios
        .post(
          `http://localhost:3001/admin/manage-orders/edit-orders/${match.params.id}`,
          { order_item_id: isDeleteOrderItem.id }
        )
        .then((response) => {
          // console.log(response)
          if (response.data.err) {
            toast(response.data.err);
          } else {
            handleClose();
            setOrderItemList(
              orderItemList.filter(
                (item) => item.order_items_id !== isDeleteOrderItem.id
              )
            );
            toast(response.data);
          }
        });
    }
  };
  //Handle show or hide edit popup
  const [orderItem, setOrderItem] = useState(null);
  const handleCloseEditPopup = () => setShowEdit(false);
  const handleShowEditPopup = () => setShowEdit(true);
  const [isEditOrderItem, setIsEditOrderItem] = useState({
    confirmEdit: false,
    id: null,
  });
  const handleEditProduct = (i, pId, s, index) => {
    // console.log(i+pId+s);
    handleShowEditPopup();
    setIsEditOrderItem({ confirmEdit: true, id: i, stt: index });
    setOrderItem({
      orderItemId: i,
      productId: pId,
      quantity: s,
      stt: index,
    });
  };

  //handle submit to send updated data
  const onSubmitEdit = (data) => {
    if (isEditOrderItem.confirmEdit) {
      axios
        .put(
          `http://localhost:3001/admin/manage-orders/edit-orders/${match.params.id}`,
          {
            order_items_id: data.OrderItemID,
            id_product: data.ProductID,
            quantity: data.Quantity,
          }
        )
        .then((res) => {
          // console.log(`Success` + res.data);
          handleCloseEdit();
          orderItemList[0].id_product = data.ProductID;
          orderItemList[0].quantity = data.Quantity;
         
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  return (
    <div>
      <Breadscrumb link2="Manage orders" link3="Edit orders" />
      {productDetail.length > 0 && (
        <>
          <form id="form-edit-orders" onSubmit={handleSubmit(onSubmit)}>
            <div className="d-flex edit-products edit-orders">
              <label htmlFor="productId">Order Id *</label>
              <input
                value={productDetail[0].order_id}
                readOnly
                type="text"
                placeholder="Order id"
                {...register("OrderID", { required: true, maxLength: 80 })}
              />
            </div>
            {errors.ProductID && <p className="error">Order id is required</p>}

            <div className="d-flex edit-products">
              <label htmlFor="productName">Customer id *</label>
              <input
                type="text"
                value={productDetail[0].kh_id}
                readOnly
                placeholder="Customer id"
                {...register("CustomerID", { required: true, maxLength: 100 })}
              />
            </div>
            {errors.CustomerID && (
              <p className="error">Customer id is required</p>
            )}

            <div className="d-flex edit-products">
              <label htmlFor="productName">Customer fullname</label>
              <input
                type="text"
                readOnly
                value={
                  customerInformation !== null && customerInformation.length > 0
                    ? customerInformation[0].first_name_customer +
                      " " +
                      customerInformation[0].last_name_customer
                    : ""
                }
              />
            </div>

            <div className="d-flex edit-products">
              <label htmlFor="productName">Customer address</label>
              <input
                type="text"
                readOnly
                value={
                  customerInformation !== null && customerInformation.length > 0
                    ? customerInformation[0].address
                    : ""
                }
              />
            </div>

            <div className="d-flex edit-products">
              <label htmlFor="productName">Customer phonenumber</label>
              <input
                type="text"
                readOnly
                value={
                  customerInformation !== null && customerInformation.length > 0
                    ? customerInformation[0].phonenumber
                    : ""
                }
              />
            </div>

            <div className="d-flex edit-products">
              <label htmlFor="productPrice">Place order date *</label>
              <input
                type="text"
                defaultValue={productDetail[0].ordered_date}
                placeholder="Place order date "
                {...register("Date", { required: true })}
              />
            </div>
            {errors.Date && <p className="error">Date is required</p>}

            <div className="d-flex edit-products">
              <label htmlFor="productType">Status *</label>
              <select
                defaultValue={productDetail[0].status}
                {...register("Status", { required: true })}
              >
                <option value=" ">Select...</option>
                <option value="Comfirmed by shop owner">
                  Comfirmed by shop owner
                </option>
                <option value="Shop owner transfer to poster">
                  Shop owner transfer to poster
                </option>
                <option value="Ypur products to contanier">
                  Ypur products to contanier
                </option>
                <option value="Shipper is shipping">Shipper is shipping</option>
                <option value="Customer received sucessfully">
                  Customer received sucessfully
                </option>
                <option value="Canceled">Canceled</option>
              </select>
            </div>
            {errors.Status && <p className="error">Status is required</p>}
            <Table striped bordered hover size="sm" className="text-center">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Order items id</th>
                  <th>Product id</th>
                  <th>quantity</th>
                  <th></th>
                </tr>
              </thead>
              <tbody className="view-products_table-body ">
                {orderItemList.length > 0 &&
                  orderItemList.map((p, i) => (
                    <tr key={i}>
                      <td>{i + 1}</td>
                      <td>{p.order_items_id}</td>
                      <td>{p.id_product}</td>
                      <td>{p.quantity}</td>
                      <td>
                        <div className="d-flex justify-content-center">
                          <div className="action-edit-btn">
                            <FontAwesomeIcon
                              className="edit-product_btn"
                              icon={faPen}
                              onClick={() =>
                                handleEditProduct(
                                  p.order_items_id,
                                  p.id_product,
                                  p.quantity,
                                  i
                                )
                              }
                            ></FontAwesomeIcon>
                          </div>

                          <div className="action-delete-btn">
                            <FontAwesomeIcon
                              className="delete-user_btn"
                              icon={faTrash}
                              onClick={() =>
                                handleDeleteProduct(p.order_items_id)
                              }
                            ></FontAwesomeIcon>
                          </div>
                        </div>
                      </td>
                    </tr>
                  ))}
              </tbody>
            </Table>
            <div className="d-flex justify-content-center">
              <Button
                onClick={handleCancelEditProducts}
                id="cancel-edit-products-btn"
                variant="secondary"
              >
                Cancel
              </Button>
              <Button
                id="submit-edit-products-btn"
                type="submit"
                variant="primary"
              >
                Submit
              </Button>
            </div>
          </form>
          <Modal style={{ zIndex: "9999" }} show={show} onHide={handleClose}>
            <Modal.Header closeButton>
              <Modal.Title>Confirm delete form</Modal.Title>
            </Modal.Header>
            <Modal.Body>Are you sure to delete this order</Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Close
              </Button>
              <Button variant="primary" onClick={confirmDelete}>
                Delete
              </Button>
            </Modal.Footer>
          </Modal>
          <Modal
            style={{ zIndex: "9999" }}
            show={showEdit}
            onHide={handleCloseEditPopup}
          >
            {orderItem && (
              <form
                id="form-edit-orders-items"
                onSubmit={handleSubmit(onSubmitEdit)}
              >
                <Modal.Header closeButton>
                  <Modal.Title>
                    Edit order item {orderItem.orderItemId}
                  </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <div className="d-flex edit-products">
                    <label htmlFor="index">#</label>
                    <input
                      value={orderItem.stt}
                      type="text"
                      readOnly
                      {...register("Stt")}
                    />
                  </div>
                  <div className="d-flex edit-products">
                    <label htmlFor="orderItemId">Order items id *</label>
                    <input
                      value={orderItem.orderItemId}
                      type="text"
                      readOnly
                      {...register("OrderItemID")}
                    />
                  </div>
                  <div className="d-flex edit-products">
                    <label htmlFor="productId">Product id *</label>
                    <input
                      type="text"
                      defaultValue={orderItem.productId}
                      placeholder="Product id"
                      {...register("ProductID", {
                        required: true,
                        maxLength: 200,
                      })}
                    />
                  </div>
                  {errors.ProductID && (
                    <p className="error">Product id is required</p>
                  )}

                  <div className="d-flex edit-products">
                    <label htmlFor="quantity">Quantity *</label>
                    <input
                      type="text"
                      defaultValue={orderItem.quantity}
                      placeholder="Quantity "
                      {...register("Quantity", { required: true })}
                    />
                  </div>

                  {errors.Quantity && (
                    <p className="error">Quantity is required</p>
                  )}
                </Modal.Body>
                <Modal.Footer>
                  <Button variant="secondary" onClick={handleCloseEdit}>
                    Close
                  </Button>
                  <Button variant="primary" type="submit">
                    Save
                  </Button>
                </Modal.Footer>
              </form>
            )}
          </Modal>
        </>
      )}
    </div>
  );
}

export default withRouter(EditOrders);
