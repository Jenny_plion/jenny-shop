import React from 'react'
import Breadscrumb from "../Breadscrumb";

function AddNewOrders() {
    return (
        <div>
            <Breadscrumb link2="Manage orders" link3="Add new order"/>
        </div>
    )
}

export default AddNewOrders
