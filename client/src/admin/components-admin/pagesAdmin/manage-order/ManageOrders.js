import { React, useState, useEffect } from "react";
import { Table, Button, Modal } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import axios from "axios";
import "../../../css-admin/admin.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPen, faTrash } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";
import SearchIcon from "@mui/icons-material/Search";
import { styled, alpha } from "@mui/material/styles";
import InputBase from "@mui/material/InputBase";
import Pagination from "@mui/material/Pagination";
import Stack from "@mui/material/Stack";
import Breadscrumb from "../Breadscrumb";
import {toast} from "react-toastify";

function ManageOrders() {
  const Search = styled("div")(({ theme }) => ({
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginBottom: theme.spacing(1),
    marginRight: theme.spacing(2),
    border: "1px solid #dee2e6",
    width: "20%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(1),
      width: "auto",
    },
  }));

  const SearchIconWrapper = styled("div")(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  }));

  const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: "inherit",
    "& .MuiInputBase-input": {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)})`,
      transition: theme.transitions.create("width"),
      width: "100%",
      [theme.breakpoints.up("sm")]: {
        width: "12ch",
        "&:focus": {
          width: "20ch",
        },
      },
    },
  }));

  //Get list of users account
  const [orders, setOrders] = useState([]);
  const [totalPages, setTotalPages] = useState(1);
  useEffect(() => {
    //get list of product type
    axios.get(`http://localhost:3001/admin/manage-orders/page/1`).then((response) => {
      setOrders(response.data.listProduct);
      setTotalPages(response.data.totalPages);
      // console.log(response)
    });
  }, []);
  const [page, setPage] = useState(1);
  //Handle change page
  const handleChangePage = (event, value) => {
    axios
      .get(`http://localhost:3001/admin/manage-orders/page/${value}`)
      .then((response) => {
        setOrders(response.data.listProduct);
        setTotalPages(response.data.totalPages);
        setPage(value);
      });
  }

  const [show, setShow] = useState(false);
  //Handle show or close modal confirm delete products
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [isDeleteOrder, setIsDeleteOrder] = useState({
    confirmDelete: false,
    id: null,
  });

  const handleDelete = (i) => {
    handleShow();
    setIsDeleteOrder({ confirmDelete: true, id: i });
  };

  const confirmDelete = () => {
    if (isDeleteOrder.confirmDelete) {
      axios
        .delete(
          `http://localhost:3001/admin/manage-orders/delete/${isDeleteOrder.id}`
        )
        .then((response) => {
          // console.log(response)
          if (response.data.err) {
            toast(response.data.err);
          } else {
            handleClose();
            setOrders(
              orders.filter((item) => item.order_id !== isDeleteOrder.id)
            );
            toast(response.data);
          }
        });
    }
  };

  //Handle search by name
  const handleKeyDown = (event) => {
    if (event.key === "Enter") {
        axios
        .get(`http://localhost:3001/admin/manage-users/search?username=${event.target.value}`)
        .then((response) => {
          setOrders(response.data);
        });
    }
  };

  return (
    <div>
       <Breadscrumb link2="Manage orders"/>
      <div className="d-flex flex-row-reverse">
        <Button variant="info" className="add-new-products mb-2">
          <Link to="/admin/manage-orders/add-new-order">Add new</Link>
        </Button>
        <Search>
          <SearchIconWrapper>
            <SearchIcon />
          </SearchIconWrapper>
          <StyledInputBase
            placeholder="Search…"
            inputProps={{ "aria-label": "search" }}
            onKeyDown={handleKeyDown}
          />
        </Search>
      </div>

      <Table striped bordered hover size="sm" className="text-center">
        <thead>
          <tr>
            <th>#</th>
            <th>Order id</th>
            <th>Customer id</th>
            <th>Place order date</th>
            <th>Status</th>
            <th></th>
          </tr>
        </thead>
        <tbody className="view-products_table-body ">
          {orders && orders.map((p, i) => (
            <tr key={i} className="text-">
              <td>{(page-1) * 10 + i + 1}</td>
              <td>{p.order_id}</td>
              <td>{p.kh_id}</td>
              <td>{p.ordered_date.slice(0,10)}</td>
              <td>{p.status}</td>
              <td className="d-flex justify-content-center">
                <div className="d-flex">
                <Link className="action-edit-btn"
                    to={`/admin/manage-orders/edit-orders/${p.order_id}`}
                  >
                    <FontAwesomeIcon
                      className="edit-product_btn mx-2"
                      icon={faPen}
                    ></FontAwesomeIcon>
                  </Link>
                  <Link className="action-delete-btn">
                  <FontAwesomeIcon
                    className="delete-user_btn mx-2"
                    icon={faTrash}
                    onClick={() => handleDelete(p.order_id)}
                  ></FontAwesomeIcon>
                  </Link>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>

      {totalPages > 1 ? <Stack spacing={2} className="ml-auto">
        <Pagination count={totalPages} color="primary" onChange={handleChangePage}/>
      </Stack> : null}

      <Modal style={{ zIndex: "9999" }} show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Confirm delete form</Modal.Title>
        </Modal.Header>
        <Modal.Body>Are you sure to delete this order</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={confirmDelete}>
            Delete
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}

export default ManageOrders;

