import { React, useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import "../../../css-admin/edit-products-admin-form.css";
import "bootstrap/dist/css/bootstrap.min.css";
import axios from "axios";
import { withRouter } from "react-router-dom";
import { Button } from "react-bootstrap";
import { toast } from "react-toastify";
import Breadscrumb from "../Breadscrumb";

function AddNewVoucher({ history }) {
  const [voucherDetail, setVoucherDetail] = useState([]);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  //handle submit to send updated data
  const onSubmit = (data) => {
    axios
      .post(`http://localhost:3001/admin/manage-voucher/add-new-voucher`, {
        discount_id: data.DiscountID,
        points: data.Points,
        time: data.Time,
      })
      .then((res) => {
        history.push("/admin/manage-voucher");
        toast("Thêm voucher thành công");
      })
      .catch((err) => {
        toast("something went wrong");
      });
  };

  //When user click cancel button then redirect to manage products page
  const handleCancelEditProducts = () => {
    history.push("/admin/manage-voucher");
  };

  return (
    <div>
      <>
        <Breadscrumb link2="Manage voucher" link3="Add new voucher" />
        <form id="form-edit-orders" onSubmit={handleSubmit(onSubmit)}>
          <div className="d-flex edit-products">
            <label htmlFor="productId">
              Discount Id <span style={{ color: "red" }}>*</span>
            </label>
            <input
              type="text"
              placeholder="Discount id"
              {...register("DiscountID", { required: true, maxLength: 80 })}
            />
          </div>
          {errors.DiscountID && (
            <p className="error">Discount id is required</p>
          )}

          <div className="d-flex edit-products">
            <label htmlFor="productName">
              Point <span style={{ color: "red" }}>*</span>
            </label>
            <input
              type="text"
              placeholder="points"
              {...register("Points", { required: true, maxLength: 100 })}
            />
          </div>
          {errors.Points && <p className="error">Points is required</p>}

          <div className="d-flex edit-products">
            <label htmlFor="productName">
              Time <span style={{ color: "red" }}>*</span>
            </label>
            <input
              type="text"
              placeholder="time"
              {...register("Time", { required: true, maxLength: 100 })}
            />
          </div>
          {errors.Time && <p className="error">Time is required</p>}

          <div className="d-flex justify-content-center">
            <Button
              onClick={handleCancelEditProducts}
              id="cancel-edit-products-btn"
              variant="secondary"
            >
              Cancel
            </Button>
            <Button
              id="submit-edit-products-btn"
              type="submit"
              variant="primary"
            >
              Submit
            </Button>
          </div>
        </form>
      </>
    </div>
  );
}

export default withRouter(AddNewVoucher);
