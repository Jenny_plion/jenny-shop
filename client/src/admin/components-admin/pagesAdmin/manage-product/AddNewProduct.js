import { React, useState, useEffect } from "react";
import { useForm, Controller } from "react-hook-form";
import "../../../css-admin/edit-products-admin-form.css";
import { Button } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import axios from "axios";
import { withRouter } from "react-router-dom";
import { toast } from "react-toastify";
import Select from "react-select";
import Breadscrumb from "../Breadscrumb";

function AddNewProduct({ history }) {
  const [image_file, setImageFile] = useState(null);
  const [image_preview, setImagePreview] = useState("");
  const [listDiscount, setListDiscount] = useState([]);
  const [listCategory, setListCategory] = useState([]);
  const {
    register,
    handleSubmit,
    formState: { errors },
    control,
  } = useForm();

  useEffect(() => {
    axios
      .get("http://localhost:3001/admin/manage-voucher/get-list-voucher")
      .then((res) => {
        if (res.data.err) {
          toast(res.data);
        } else setListDiscount(JSON.parse(res.data[0].json));
      });

      axios
      .get("http://localhost:3001/admin/manage-category/get-list-category")
      .then((res) => {
        if (res.data.err) {
          toast(res.data);
        } else setListCategory(JSON.parse(res.data[0].json));
      });
  }, []);

  const onSubmit = (data) => {
    if (image_file !== null) {
      let formData = new FormData();
      formData.append("myImage", image_file);
      formData.append("product_id", data.ProductID);
      formData.append("product_name", data.ProductName);
      formData.append("price", data.Price);
      formData.append("product_information", data.ProductInformation);
      formData.append("product_status", data.ProductStatus);
      formData.append("total_sold", data.TotalSold);
      formData.append("product_type_id", data.ProductType);
      formData.append("discount_id", data.DiscountId);
      formData.append("category_id", data.categoryId);

      // the image field name should be similar to your api endpoint field name
      // in my case here the field name is customFile
      axios
        .post(
          "http://localhost:3001/admin/manage-products/add-new-products",
          formData,
          {
            headers: {
              Authorization:
                "YOUR_API_AUTHORIZATION_KEY_SHOULD_GOES_HERE_IF_HAVE",
              "Content-type": "multipart/form-data",
            },
          }
        )
        .then((res) => {
          if (res.data.message) {
            toast(res.data.message);
          } else {
            history.push("/admin/manage-products");
            toast(res.data);
          }
        })
        .catch((err) => {
          toast(err);
        });
    }
  };
  //Cancel edit product
  const handleCancelAddNewProducts = () => {
    history.push("/admin/manage-products");
  };

  // Image Preview Handler
  const handleImagePreview = (e) => {
    let image_as_base64 = URL.createObjectURL(e.target.files[0]);
    let image_as_files = e.target.files[0];
    setImageFile(image_as_files);
    setImagePreview(image_as_base64);
  };
  return (
    <div>
       <Breadscrumb link2="Manage products" link3="Add new product"/>
      <form id="form-edit-products" className="edit-product" onSubmit={handleSubmit(onSubmit)}>
        <div className="d-flex edit-products">
          <label htmlFor="productId">Product Id *</label>
          <input
            type="text"
            placeholder="Product id"
            {...register("ProductID", { required: true, maxLength: 80 })}
          />
        </div>
        {errors.ProductID && <p className="error">Product id is required</p>}

        <div className="d-flex edit-products">
          <label htmlFor="productName">Product name *</label>
          <input
            type="text"
            placeholder="Product name"
            {...register("ProductName", { required: true, maxLength: 100 })}
          />
        </div>
        {errors.ProductName && (
          <p className="error">Product name is required</p>
        )}

        <div className="d-flex edit-products">
          <label htmlFor="productInformation">Product Information *</label>
          <textarea {...register("ProductInformation")} rows="5"></textarea>
        </div>

        <div className="d-flex edit-products">
          <label htmlFor="productPrice">Product price *</label>
          <input
            type="text"
            placeholder="Price"
            {...register("Price", { required: true })}
          />
        </div>
        {errors.Price && <p className="error">Price is required</p>}

        <div className="d-flex edit-products">
          <label htmlFor="productStatus">Product status *</label>
          <select {...register("ProductStatus", { required: true })}>
            <option value=" ">Select...</option>
            <option value="old">Old</option>
            <option value="new">New</option>
          </select>
        </div>
        {errors.ProductStatus && (
          <p className="error">Product status is required</p>
        )}

        <div className="d-flex edit-products">
          <label htmlFor="totalSold">Total sold *</label>
          <input
            type="text"
            placeholder="Total sold"
            {...register("TotalSold", { required: true })}
          />
        </div>
        {errors.TotalSold && <p className="error">Total sold is required</p>}

        <div className="d-flex edit-products">
          <label htmlFor="productType">Product type *</label>
          <select {...register("ProductType", { required: true })}>
            <option value=" ">Select...</option>
            <option value="js-01">Kitchenware and tableware</option>
            <option value="js-02">Livingroom</option>
            <option value="js-03">Bedroom</option>
            <option value="js-04">Bathroom</option>
            <option value="js-05">Decoration</option>
          </select>
        </div>
        {errors.ProductType && (
          <p className="error">Product type is required</p>
        )}

<div className="d-flex edit-products">
          <label htmlFor="categoryname">
            Category name <span style={{ color: "red" }}>*</span>
          </label>
          <Controller
            control={control}
            name="categoryId"
            render={({ field: { onChange, value, ref } }) => (
              <>
                <Select
                  inputRef={ref}
                  value={listCategory.find((c) => c.value === value)}
                  options={listCategory}
                  onChange={(val) => onChange(val.value)}
                />
              </>
            )}
          />
        </div>

        <div className="d-flex edit-products">
          <label htmlFor="productId">
            Discount id <span style={{ color: "red" }}>*</span>
          </label>
          <Controller
            control={control}
            name="DiscountId"
            render={({ field: { onChange, value, ref } }) => (
              <>
                <Select
                  inputRef={ref}
                  value={listDiscount.find((c) => c.value === value)}
                  options={listDiscount}
                  onChange={(val) => onChange(val.value)}
                />
              </>
            )}
          />
        </div>

        <div className="d-flex flex-column edit-products">
          <label htmlFor="productType">Product image *</label>
          <input
            id="choose-image-product"
            type="file"
            onChange={handleImagePreview}
            {...register("ProductImage", {
              required: true,
              onChange: handleImagePreview,
            })}
            name="ProductImage"
            accept="image/*"
          />
          <img
            style={{ width: "250px", height: "100%" }}
            src={image_preview}
            alt="preview"
            className="product-image-preview"
          />
        </div>
        {errors.ProductImage && (
          <p className="error">Product image is required</p>
        )}

        <div className="d-flex justify-content-center">
          <Button
            onClick={handleCancelAddNewProducts}
            id="cancel-edit-products-btn"
            variant="secondary"
          >
            Cancel
          </Button>
          <Button id="submit-edit-products-btn" type="submit" variant="primary">
            Submit
          </Button>
        </div>
      </form>
    </div>
  );
}

export default withRouter(AddNewProduct);
