import { React, useState, useEffect } from "react";
import { Table, Image, Button, Modal } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import axios from "axios";
import "../../../css-admin/admin.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPen, faTrash } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";
import SearchIcon from "@mui/icons-material/Search";
import { styled, alpha } from "@mui/material/styles";
import InputBase from "@mui/material/InputBase";
import Pagination from "@mui/material/Pagination";
import Stack from "@mui/material/Stack";
import Breadscrumb from "../Breadscrumb";
import {toast} from "react-toastify";

function ManageProducts() {
  const Search = styled("div")(({ theme }) => ({
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginBottom: theme.spacing(1),
    marginRight: theme.spacing(2),
    border: "1px solid #dee2e6",
    width: "20%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(1),
      width: "auto",
    },
  }));

  const SearchIconWrapper = styled("div")(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  }));

  const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: "inherit",
    "& .MuiInputBase-input": {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)})`,
      transition: theme.transitions.create("width"),
      width: "100%",
      [theme.breakpoints.up("sm")]: {
        width: "12ch",
        "&:focus": {
          width: "20ch",
        },
      },
    },
  }));
  const [products, setProducts] = useState([]);
  const [totalPages, setTotalPages] = useState(1);
  useEffect(() => {
    //get list of product type
    axios
      .get(`http://localhost:3001/admin/manage-products/page/1`)
      .then((response) => {
        setProducts(response.data.listProduct);
        setTotalPages(response.data.totalPages);
      });
  }, []);

  const [show, setShow] = useState(false);
  //Handle show or close modal confirm delete products
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [isDeleteProduct, setIsDeleteProduct] = useState({
    confirmDelete: false,
    id: null,
  });
  const [page, setPage] = useState(1);

  const handleDelete = (i) => {
    handleShow();
    setIsDeleteProduct({ confirmDelete: true, id: i });
  };

  const confirmDelete = () => {
    if (isDeleteProduct.confirmDelete) {
      axios
        .delete(
          `http://localhost:3001/admin/manage-products/delete/${isDeleteProduct.id}`
        )
        .then((response) => {
          if (response.data.err) {
            toast(response.data.err);
          } else {
            handleClose();
            setProducts(
              products.filter((item) => item.product_id !== isDeleteProduct.id)
            );
            toast(response.data);
          }
        });
    }
  };

  //Handle search by name of products
  const handleSearchByName = (event) => {
    if (event.key === "Enter") {
      axios
        .get(
          `http://localhost:3001/admin/manage-products/search?productName=${event.target.value}`
        )
        .then((response) => {
          if (!response.data.err) setProducts(response.data);
        });
    }
  };

  //Handle change page
  const handleChangePage = (event, value) => {
    axios
      .get(`http://localhost:3001/admin/manage-products/page/${value}`)
      .then((response) => {
        setProducts(response.data.listProduct);
        setTotalPages(response.data.totalPages);
        setPage(value);
      });
  }

  return (
    <div>
       <Breadscrumb link2="Manage products"/>
      <div className="d-flex flex-row-reverse">
        <Button variant="info" className="add-new-products mb-2">
          <Link to="/admin/manage-products/add-new-products">Add new</Link>
        </Button>
        <Search>
          <SearchIconWrapper>
            <SearchIcon />
          </SearchIconWrapper>
          <StyledInputBase
            placeholder="Search…"
            inputProps={{ "aria-label": "search" }}
            onKeyDown={handleSearchByName}
          />
        </Search>
      </div>

      <Table striped bordered hover size="sm">
        <thead>
          <tr>
            <th>#</th>
            <th>Product image</th>
            <th>ProductID</th>
            <th>Product Name</th>
            <th>Product Infomation</th>
            <th>Price</th>
            <th>Product status</th>
            <th>Total sold</th>
            <th>Product type</th>
            <th>Discount id</th>
            <th></th>
          </tr>
        </thead>
        <tbody className="view-products_table-body">
          {products.map((p, i) => (
            <tr key={i} className="text-">
              <td>{(page-1) * 10 + i + 1}</td>
              <td>
                <Image
                  className="image-manage-product"
                  src={"/images/" + p.product_image}
                />
              </td>
              <td>{p.product_id}</td>
              <td>{p.product_name}</td>
              <td>{p.product_infomation}</td>
              <td>{p.price}</td>
              <td>{p.product_status}</td>
              <td>{p.total_sold}</td>
              <td>{p.product_type_id}</td>
              <td>{p.discount_id}</td>
              <td className="d-flex justify-content-center align-items-center">
                <div className="d-flex">
                  <Link className="action-edit-btn"
                    to={`/admin/manage-products/edit-products/${p.product_id}`}
                  >
                    <FontAwesomeIcon
                      className="edit-product_btn mx-2" 
                      icon={faPen}
                    ></FontAwesomeIcon>
                  </Link>
                  <Link className="action-delete-btn">
                  <FontAwesomeIcon
                    className="delete-product_btn mx-2"
                    icon={faTrash}
                    onClick={() => handleDelete(p.product_id)}
                  ></FontAwesomeIcon>
                  </Link>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>

      {totalPages > 1 ? <Stack spacing={2} className="ml-auto">
        <Pagination count={totalPages} color="primary" onChange={handleChangePage}/>
      </Stack> : null}

      <Modal style={{ zIndex: "9999" }} show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Confirm delete form</Modal.Title>
        </Modal.Header>
        <Modal.Body>Are you sure to delete this product</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={confirmDelete}>
            Delete
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}

export default ManageProducts;
