import { React, useEffect, useState } from "react";
import { useForm, Controller } from "react-hook-form";
import "../../../css-admin/edit-products-admin-form.css";
import { Button } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import axios from "axios";
import { withRouter } from "react-router-dom";
import Select from "react-select";
import { toast } from "react-toastify";
import Breadscrumb from "../Breadscrumb";

function EditProducts({ match, history }) {
  const [productDetail, setProductDetail] = useState([]);
  const [image_file, setImageFile] = useState(null);
  const [image_preview, setImagePreview] = useState("");
  const [listDiscount, setListDiscount] = useState([]);

  const {
    register,
    handleSubmit,
    formState: { errors },
    control,
  } = useForm();
  //get list discount id
  useEffect(() => {
    axios
      .get("http://localhost:3001/admin/manage-voucher/get-list-voucher")
      .then((res) => {
        if (res.data.err) {
          toast(res.data);
        } else setListDiscount(JSON.parse(res.data[0].json));
      });
  }, []);

  //handle submit to send updated data
  const onSubmit = (data) => {
      let formData = new FormData();
      formData.append("myImage", image_file);
      formData.append("product_id", data.ProductID);
      formData.append("product_name", data.ProductName);
      formData.append("price", data.Price);
      formData.append("product_information", data.ProductInformation);
      formData.append("product_status", data.ProductStatus);
      formData.append("total_sold", data.TotalSold);
      formData.append("product_type_id", data.ProductType);
      formData.append("discount_id", data.DiscountId);

      // the image field name should be similar to your api endpoint field name
      // in my case here the field name is customFile
      axios
        .put(
          `http://localhost:3001/admin/manage-products/edit-products/${match.params.id}`,
          formData,
          {
            headers: {
              Authorization:
                "YOUR_API_AUTHORIZATION_KEY_SHOULD_GOES_HERE_IF_HAVE",
              "Content-type": "multipart/form-data",
            },
          }
        )
        .then((res) => {
          toast("Edit product "+data.ProductID+ "successfully");
          history.push("/admin/manage-products");
        })
        .catch((err) => {
          toast(err);
        });
  };

  //get list of product type
  useEffect(() => {
    axios
      .get(
        `http://localhost:3001/admin/manage-products/edit-products/${match.params.id}`
      )
      .then((response) => {
        if (response.data.err) {
          alert(response.data.err);
        } else {
          setProductDetail(response.data);
          if(response.data[0].product_image)setImageFile(response.data[0].product_image);
        }
      });
  }, [match]);

  //When user click cancel button then redirect to manage products page
  const handleCancelEditProducts = () => {
    history.push("/admin/manage-products");
  };

  //Handle upload file

  // Image Preview Handler
  const handleImagePreview = (e) => {
    let image_as_base64 = URL.createObjectURL(e.target.files[0]);
    let image_as_files = e.target.files[0];
    setImageFile(image_as_files);
    setImagePreview(image_as_base64);
  };

  return (
    <div>
       <Breadscrumb link2="Manage products" link3="Edit products"/>
      {productDetail.length > 0 && (
        <form id="form-edit-products" className="edit-product" onSubmit={handleSubmit(onSubmit)}>
          <div className="d-flex edit-products">
            <label htmlFor="productId">Product Id *</label>
            <input
              defaultValue={productDetail[0].product_id}
              type="text"
              placeholder="Product id"
              {...register("ProductID", { required: true, maxLength: 80 })}
            />
          </div>
          {errors.ProductID && <p className="error">Product id is required</p>}

          <div className="d-flex edit-products">
            <label htmlFor="productName">Product name *</label>
            <input
              type="text"
              defaultValue={productDetail[0].product_name}
              placeholder="Product name"
              {...register("ProductName", { required: true, maxLength: 100 })}
            />
          </div>
          {errors.ProductName && (
            <p className="error">Product name is required</p>
          )}

          <div className="d-flex edit-products">
            <label htmlFor="productInformation">Product Information *</label>
            <textarea
              {...register("ProductInformation", { required: true })}
              defaultValue={productDetail[0].product_infomation}
              rows="5"
            ></textarea>
          </div>
          {errors.ProductInformation && (
            <p className="error">Product Information is required</p>
          )}

          <div className="d-flex edit-products">
            <label htmlFor="productPrice">Product price *</label>
            <input
              type="text"
              defaultValue={productDetail[0].price}
              placeholder="Price"
              {...register("Price", { required: true })}
            />
          </div>
          {errors.Price && <p className="error">Price is required</p>}

          <div className="d-flex edit-products">
            <label htmlFor="productStatus">Product status *</label>
            <select
              defaultValue={productDetail[0].product_status}
              {...register("ProductStatus", { required: true })}
            >
              <option value=" ">Select...</option>
              <option value="old">Old</option>
              <option value="new">New</option>
            </select>
          </div>
          {errors.ProductStatus && (
            <p className="error">Product status is required</p>
          )}

          <div className="d-flex edit-products">
            <label htmlFor="totalSold">Total sold *</label>
            <input
              type="text"
              defaultValue={productDetail[0].total_sold}
              placeholder="Total sold"
              {...register("TotalSold", { required: true })}
            />
          </div>
          {errors.TotalSold && <p className="error">Total sold is required</p>}

          <div className="d-flex edit-products">
            <label htmlFor="productType">Product type *</label>
            <select
              defaultValue={productDetail[0].product_type_id}
              {...register("ProductType", { required: true })}
            >
              <option value=" ">Select...</option>
              <option value="js-01">Kitchenware and tableware</option>
              <option value="js-02">Livingroom</option>
              <option value="js-03">Bedroom</option>
              <option value="js-04">Bathroom</option>
              <option value="js-05">Decoration</option>
            </select>
          </div>
          {errors.ProductType && (
            <p className="error">Product type is required</p>
          )}

          <div className="d-flex edit-products">
            <label htmlFor="productId">
              Discount id <span style={{ color: "red" }}>*</span>
            </label>
            <Controller
              control={control}
              name="DiscountId"
              defaultValue={productDetail[0].discount_id}
              render={({ field: { onChange, value, ref } }) => (
                <>
                  <Select
                    inputRef={ref}
                    value={listDiscount.find((c) => c.value === value)}
                    options={listDiscount}
                    onChange={(val) => onChange(val.value)}
                  />
                </>
              )}
            />
          </div>

          <div className="d-flex flex-column edit-products">
            <label htmlFor="productType">Product image *</label>
            {productDetail[0].product_image !== "" ? (
              <>
                <img
                  className="product-image"
                  style={{ width: "250px", height: "100%" }}
                  src={"/images/" + productDetail[0].product_image}
                  alt="preview"
                />

                <input
                  id="choose-image-product"
                  type="file"
                  onChange={handleImagePreview}
                  {...register("ProductImage", {
                    required:
                      productDetail[0].product_image !== "" ? false : true,
                    onChange: handleImagePreview,
                  })}
                  name="ProductImage"
                  accept="image/*"
                />
                {image_preview && (
                  <img
                    style={{ width: "250px", height: "100%" }}
                    className="product-image-preview"
                    src={image_preview}
                    alt=" preview"
                  />
                )}
              </>
            ) : (
              <>
                <input
                  id="choose-image-product"
                  type="file"
                  {...register("ProductImage", {
                    required: true,
                    onChange: handleImagePreview,
                  })}
                  name="ProductImage"
                  accept="image/*"
                />
                <img
                  className="product-image-preview"
                  style={{ width: "250px", height: "100%" }}
                  src={image_preview}
                  alt="preview"
                />
              </>
            )}
          </div>
          {errors.ProductImage && (
            <p className="error">Product image is required</p>
          )}

          <div className="d-flex justify-content-center">
            <Button
              onClick={handleCancelEditProducts}
              id="cancel-edit-products-btn"
              variant="secondary"
            >
              Cancel
            </Button>
            <Button
              id="submit-edit-products-btn"
              type="submit"
              variant="primary"
            >
              Submit
            </Button>
          </div>
        </form>
      )}
    </div>
  );
}

export default withRouter(EditProducts);
