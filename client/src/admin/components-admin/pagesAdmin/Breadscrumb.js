import * as React from 'react';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';
import Stack from '@mui/material/Stack';
import NavigateNextIcon from '@mui/icons-material/NavigateNext';

function handleClick(event) {
  event.preventDefault();
}

export default function Breadcrumb(props) {
  const breadcrumbs = [
    <Link underline="hover" key="1" color="inherit" href="/" onClick={handleClick}>
      Dashboard
    </Link>,
    <Link
      underline="hover"
      key="2"
      color= {props.link3 ? "inherit" : "text.primary"} 
      href="http://localhost:3000/admin/manage-users"
      onClick={handleClick}
    >
      {props.link2}
    </Link>,
    props.link3 ? (
    <Typography key="3" color="text.primary">
      {props.link3}
    </Typography>) : ""
  ];

  return (
    <Stack spacing={2}>
      <Breadcrumbs
        separator={<NavigateNextIcon fontSize="small" />}
        aria-label="breadcrumb"
      >
        {breadcrumbs}
      </Breadcrumbs>
    </Stack>
  );
}