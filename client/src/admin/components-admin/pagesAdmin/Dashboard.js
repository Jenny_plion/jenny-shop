import { React, useEffect, useState } from "react";
import { Container, Row, Button, ButtonToolbar, ButtonGroup } from "react-bootstrap";
import axios from "axios";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import TextField from "@mui/material/TextField";
import DatePicker from "@mui/lab/DatePicker";
import "../../css-admin/admin.css";
import { Bar} from "react-chartjs-2";

function Dashboard() {
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const d = new Date();
  const [month, setMonth] = useState(d.getMonth());
  const [year, setYear] = useState(d.getFullYear());
  const [chartData, setChartData] = useState(null);
  const [totalOrderedProducts, setTotalOrderedProducts] = useState(0);
  const backgroundColor = [
    "rgba(255, 99, 132, 0.2)",
    "rgba(54, 162, 235, 0.2)",
    "rgba(255, 206, 86, 0.2)",
    "rgba(75, 192, 192, 0.2)",
    "rgba(153, 102, 255, 0.2)",
    "rgba(153, 102, 255, 0.2)",
    "rgba(255, 159, 64, 0.2)",
    "rgba(153, 100, 275, 0.2)",
    "rgba(153, 150, 255, 0.2)",
    "rgba(255, 105, 64, 0.2)",
  ];

  const borderColor = [
    "rgba(255, 99, 132, 1)",
    "rgba(54, 162, 235, 1)",
    "rgba(255, 206, 86, 1)",
    "rgba(75, 192, 192, 1)",
    "rgba(153, 102, 255, 1)",
    "rgba(153, 102, 255, 0.2)",
    "rgba(255, 159, 64, 1)",
    "rgba(255, 99, 132, 1)",
    "rgba(54, 162, 235, 1)",
    "rgba(255, 206, 86, 1)",
  ]

  useEffect(() => {
    axios.get("http://localhost:3001/admin/dashboard").then((response) => {
      if (response.data.err) {
        alert("something went wrong");
      } else {
        response.data.length > 0 ? setTotalOrderedProducts(
          response.data.map((itm) => itm.quantity).reduce((a, v) => a + v),
          0
        ) : setTotalOrderedProducts(0);
        setChartData({
          labels: response.data.map((itm) => itm.id_product).slice(0, 9),
          datasets: [
            {
              label: "ordered quantity of this product",
              data: response.data.map((itm) => itm.quantity).slice(0, 9),
              backgroundColor: backgroundColor,
              borderColor: borderColor,
              borderWidth: 1,
            },
          ],
        });
      }
    });
  }, []);

  // console.log(chartData);
  const handleSubmitSearch = () => {
    axios.post("http://localhost:3001/admin/dashboard", {startDate: startDate, endDate: endDate}).then((response) => {
      if (response.data.err) {
        alert("something went wrong");
      } else {
        if(response.data.length > 0){
          setTotalOrderedProducts(
            response.data.map((itm) => itm.quantity).reduce((a, v) => a + v),
            0
          );
        }else setTotalOrderedProducts(0);
        
        setChartData({
          labels: response.data.map((itm) => itm.id_product).slice(0, 9),
          datasets: [
            {
              label: "ordered quantity of this product",
              data: response.data.map((itm) => itm.quantity).slice(0, 9),
              backgroundColor: backgroundColor,
              borderColor: borderColor,
              borderWidth: 1,
            },
          ],
        });
      }
    });
  }

  return (
    <div>
      <h5>
        Statitics order in {month}/{year}
      </h5>
      <Container>
        <Row className="dashboard-toolbar">
          <ButtonToolbar aria-label="Toolbar with button groups">
            <ButtonGroup className="me-2" aria-label="First group">
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <DatePicker
                label="Date start"
                value={startDate}
                onChange={(newValue) => {
                  setStartDate(newValue);
                }}
                renderInput={(params) => <TextField {...params} />}
              />
            </LocalizationProvider>
            </ButtonGroup>
            <ButtonGroup className="me-2" aria-label="Second group">
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <DatePicker
                label="Date end"
                value={endDate}
                onChange={(newValue) => {
                  setEndDate(newValue);
                }}
                renderInput={(params) => <TextField {...params} />}
                minDate={startDate} 
              />
            </LocalizationProvider>
            </ButtonGroup>
            <ButtonGroup aria-label="Third group">
            <Button type="submit" variant="primary" onClick={handleSubmitSearch}>
              Search
            </Button>
            </ButtonGroup>
          </ButtonToolbar>
         
          
        </Row>
        <Row>
          {chartData !== null && (
            <Bar className="chart"
              data={chartData}
              options={{
                legend: {
                  display: true,
                  position: "right",
                },
              }}
            />
          )}
          <p>Total ordered products are {totalOrderedProducts} (products)</p>
        </Row>
      </Container>
    </div>
  );
}

export default Dashboard;
