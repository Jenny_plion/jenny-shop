import { React, useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import "../../../css-admin/edit-products-admin-form.css";
import "bootstrap/dist/css/bootstrap.min.css";
import axios from "axios";
import { withRouter } from "react-router-dom";
import {  Button } from "react-bootstrap";
import { toast } from "react-toastify";
import Breadscrumb from "../Breadscrumb";

function EditCatagory({ match, history }) {
  const [voucherDetail, setVoucherDetail] = useState([]);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  useEffect(() => {
    //get list of product type
    axios.get(`http://localhost:3001/admin/manage-catagory/edit-catagory/${match.params.id}`).then((response) => {
        if(response.data.err){
            toast("Something went wrong")
        }else setVoucherDetail(response.data);
    });
  }, []);

  //handle submit to send updated data
  const onSubmit = (data) => {
    axios
      .put(
        `http://localhost:3001/admin/manage-catagory/edit-catagory/${match.params.id}`,
        {category_id: match.params.id, category_name: data.CategoryName, category_type: data.CategoryType }
      )
      .then((res) => {
        history.push("/admin/manage-catagory");
        toast("Edit catagory successfully")
      })
      .catch((err) => {
        toast("something went wrong")
      });
  };

  

  //When user click cancel button then redirect to manage products page
  const handleCancelEditProducts = () => {
    history.push("/admin/manage-catagory");
  };

  return (
    <div>
      <Breadscrumb link2="Manage category" link3="Edit category"/>
      {voucherDetail.length > 0 && (
        <>
          <form id="form-edit-orders" onSubmit={handleSubmit(onSubmit)}>
            <div className="d-flex edit-products">
              <label htmlFor="productId">Catagory Id *</label>
              <input
                readOnly
                defaultValue={voucherDetail[0].category_id}
                type="text"
                placeholder="Catagory id"
              />
            </div>

            <div className="d-flex edit-products">
              <label htmlFor="productName">Catagory name *</label>
              <input
                type="text"
                defaultValue={voucherDetail[0].category_name}
                placeholder="Catagory name"
                {...register("CategoryName", { required: true, maxLength: 100 })}
              />
            </div>
            {errors.CategoryName && <p className="error">Category name is required</p>}

            <div className="d-flex edit-products">
              <label htmlFor="productName">Catagory type *</label>
              <input
                type="text"
                defaultValue={voucherDetail[0].category_type}
                placeholder="Category type"
                {...register("CategoryType", { required: true, maxLength: 100 })}
              />
            </div>
            {errors.CategoryType && <p className="error">Category type is required</p>}

            <div className="d-flex justify-content-center">
              <Button
                onClick={handleCancelEditProducts}
                id="cancel-edit-products-btn"
                variant="secondary"
              >
                Cancel
              </Button>
              <Button
                id="submit-edit-products-btn"
                type="submit"
                variant="primary"
              >
                Submit
              </Button>
            </div>
          </form>
        </>
      )}
    </div>
  );
}

export default withRouter(EditCatagory);
