import { React, useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import "../../../css-admin/edit-products-admin-form.css";
import "bootstrap/dist/css/bootstrap.min.css";
import axios from "axios";
import { withRouter } from "react-router-dom";
import {  Button } from "react-bootstrap";
import { toast } from "react-toastify";
import Breadscrumb from "../Breadscrumb";

function AddNewCatagory({  history }) {
  const [voucherDetail, setVoucherDetail] = useState([]);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  //handle submit to send updated data
  const onSubmit = (data) => {
    axios
      .post(
        `http://localhost:3001/admin/manage-catagory/add-new-catagory`,
        { category_id: data.CategoryId, category_name: data.CategoryName, category_type: data.CategoryType}
      )
      .then((res) => {
        history.push("/admin/manage-catagory");
        toast("Thêm catagory thành công")
      })
      .catch((err) => {
        toast("something went wrong")
      });
  };

  

  //When user click cancel button then redirect to manage products page
  const handleCancelEditProducts = () => {
    history.push("/admin/manage-catagory");
  };

  return (
    <div>
        <>
          <Breadscrumb link2="Manage category" link3="Add new category"/>
          <form id="form-edit-orders" onSubmit={handleSubmit(onSubmit)}>
            <div className="d-flex edit-products">
              <label htmlFor="productId">Catagory Id <span style={{color: "red"}}>*</span></label>
              <input
                
                type="text"
                placeholder="Catagory id"
                {...register("CategoryId", { required: true, maxLength: 80 })}
              />
            </div>
            {errors.CategoryId && (
              <p className="error">Category id is required</p>
            )}

            <div className="d-flex edit-products">
              <label htmlFor="productName">Catagory name<span style={{color: "red"}}>*</span></label>
              <input
                type="text"
                
                placeholder="Catagory name"
                {...register("CategoryName", { required: true, maxLength: 100 })}
              />
            </div>
            {errors.CategoryName && <p className="error">Category name is required</p>}

            <div className="d-flex edit-products">
              <label htmlFor="productName">Category type<span style={{color: "red"}}>*</span></label>
              <input
                type="text"
                
                placeholder="Catagory type"
                {...register("CategoryType", { required: true, maxLength: 100 })}
              />
            </div>
            {errors.CategoryType && <p className="error">Category type is required</p>}

            <div className="d-flex justify-content-center">
              <Button
                onClick={handleCancelEditProducts}
                id="cancel-edit-products-btn"
                variant="secondary"
              >
                Cancel
              </Button>
              <Button
                id="submit-edit-products-btn"
                type="submit"
                variant="primary"
              >
                Submit
              </Button>
            </div>
          </form>
        </>
    
    </div>
  );
}

export default withRouter(AddNewCatagory);
