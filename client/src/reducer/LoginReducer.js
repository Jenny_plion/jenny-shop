const initialState = {
    responseData: {
        username: '',
        message: '', 
        role: ''
    }
}

function LoginReducer(state = initialState, action) {
    const { type, payload } = action;
    switch(type){
        case 'log_in_success':
            return { responseData: {username: payload.username, message: 'null', role: payload.role, id_account: payload.id} };
        case 'log_in_fail':
            return { responseData: {username: '', message: payload} };
        case 'log_out':
            return { responseData: {username: '', message: 'log out', role: '', id_account: ''} };
        default:
            return state;
    }
}

export default LoginReducer;