const initialState = {
  responseData: {
    firstname: "",
    lastname: "",
    address1: "",
    address2: "",
    city: "",
    state: "",
    zipCode: "",
    country: ""
  },
};

function CheckoutReducer(state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case "shipping_address":
      return {
        shippingAddressData: {
          firstname: payload.firstname,
          lastname: payload.lastname,
          address1: payload.address1,
          address2: payload.address2,
          city: payload.city,
          state: payload.state,
          zipCode: payload.zipcode,
          country: payload.country,
        },
      };
    default:
      return state;
  }
}

export default CheckoutReducer;
