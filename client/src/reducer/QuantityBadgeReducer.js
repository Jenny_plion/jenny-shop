const initialState = {
    count: 0
}

function QuantityBadgeReducer(state = initialState, action) {
    const { type, payload } = action;
    switch(type){
        case 'add-to-cart':
            return {
                count: state.count + 1
            } 
        case 'delete-cart':
            return {
                count: state.count > 0 ? (parseInt(state.count)  -  parseInt(payload.count)) : 0
            }
        case 'current-login':
            return {count: payload.count}
        case 'add-to-cart-quantity':
            return {
                count: (parseInt(state.count)  + parseInt(payload.count))
            }
        default:
            return state;
    }
}

export default QuantityBadgeReducer;