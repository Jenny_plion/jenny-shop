import { combineReducers } from 'redux'
import LoginReducer from './LoginReducer'
import CountReducer from './CountReducer'
import AddProductsReducer from '../admin/components-admin/reducer-admin/AddProductsReducer'
import CheckoutReducer from './CheckoutReducer'
import PaymentReducer from './PaymentReducer'
import QuantityBadgeReducer from './QuantityBadgeReducer'
import OrderItemsReducer from './OrderItemsReducer'

export default combineReducers({
  LoginReducer,
  CountReducer,
  AddProductsReducer,
  CheckoutReducer,
  PaymentReducer,
  QuantityBadgeReducer, 
  OrderItemsReducer
})