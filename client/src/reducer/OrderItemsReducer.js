const initialState = {
    responseData: []
};
  
  function OrderItemsReducer(state = initialState, action) {
    const { type, payload } = action;
    switch (type) {
      case "check-out":
        return {
            responseData: payload
        };
      default:
        return state;
    }
  }
  
  export default OrderItemsReducer;
  