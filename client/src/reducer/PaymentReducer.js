const initialState = {
    responseData: {
        nameOnCard: "",
        cardNumber: "",
        expireDate: "",
        cvv: "",
      },
  };
  
  function PaymentReducer(state = initialState, action) {
    const { type, payload } = action;
    switch (type) {
      case "payment":
        return {
          paymentData: {
            nameOnCard: payload.nameOnCard,
            cardNumber: payload.cardNumber,
            expireDate: payload.expireDate,
            cvv: payload.cvv,
          },
        };
      default:
        return state;
    }
  }
  
  export default PaymentReducer;
  