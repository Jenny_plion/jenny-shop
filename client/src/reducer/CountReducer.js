const initialState = {
    count: 0
}

function LoginReducer(state = initialState, action) {
    const { type } = action;
    switch(type){
        case 'increase':
            return {
                count: state.count + 1
            } 
        case 'decrease':
            return {
                count: state.count > 0 ? state.count - 1 : 0
            }
        default:
            return state;
    }
}

export default LoginReducer;