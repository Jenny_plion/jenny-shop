import "./App.css";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Home from "./components/pages/Home";
import Aboutus from "./components/pages/Aboutus";
import Products from "./components/pages/Products";
import Signup from "./components/pages/Signup";
import Navbarmenu from "./components/navbar/Navbar";
import Footer from "./components/navbar/Footer";
import ScrollToTopBtn from "./components/ScrollToTopBtn";
import SignInSide from "./components/form/SignInSide";
import ShoppingCart from "./components/pages/ShoppingCart";
import ProductDetail from "./components/ProductDetail";
import Admin from "./admin/Admin";
import {useSelector} from "react-redux";
import ProductLists from "./components/ProductLists";
import Checkout from "./components/checkout/Checkout";
import MyOrder from "./components/pages/MyOrder";
import MyProfile from "./components/pages/MyProfile";
import ChangePassword from "./components/pages/ChangePassword";
import SearchProduct from "./components/pages/SearchProduct";

function App() {
  const state = useSelector((state) => state);
  return (
    <div className="App">
      {state.LoginReducer.responseData.role !== "admin" ? (<Router>
        <Navbarmenu />
        <Route path="/" exact component={Home}></Route>
        <Route path="/aboutus" component={Aboutus}></Route>
        <Route path="/products" exact component={Products}></Route>
        <Route path="/signup" component={Signup}></Route>
        <Route path="/login" component={SignInSide}></Route>
        <Route path="/cart/:id" component={ShoppingCart}></Route>
        <Route path="/products/:id" exact component={ProductDetail}></Route>
        <Route path="/:product" exact component={ProductLists}></Route>
        <Route path="/checkout" exact component={Checkout}></Route>
        <Route path="/my-order" exact component={MyOrder}></Route>
        <Route path="/my-profile/:id" exact component={MyProfile}></Route>
        <Route path="/change-password/:id" exact component={ChangePassword}></Route>
        <Route path="/search" exact component={SearchProduct}></Route>
        <Footer />
        <ScrollToTopBtn />
      </Router>
      )  :  (
        <Router>
          <Admin />
        </Router>
       )}
    </div>
  );
}

export default App;
